function options = setDefaultParameters(options, varargin)
% usage: setDefaultParameters(options, 'param1', value1, ...)
% set default values for parameters IF THEY WERE NOT SET
% if (mod(nargin-1, 2) ~= 0)
%     error 'usage: setDefaultParameters(options, ''param1'', value1, ...)'
% end

if iscell(options)
    if length(options) == 1
        options = options{1};
    elseif isempty(options) 
        options = struct();
    else
        temp = [];
        for i=1:2:length(options)
            temp.(options{i}) = options{i+1};
        end
        options = temp;
    end
end

if length(varargin) == 1
    temp = varargin{1};
    varargin = {};
    f = fieldnames(temp);
    for i=1:length(f)
        varargin{2 * (i-1) + 1} = f{i};
        varargin{2 * (i-1) + 2} = temp.(f{i});
    end
end

param_report = true;
if (isfield(options, 'param_report'))
    param_report = options.param_report;
end
optionsmem = options;
for i=1:2:length(varargin)
    if ~isfield(options, varargin{i})
        if isnumeric(varargin{i + 1})
            s = num2str(varargin{i + 1});
        elseif islogical(varargin{i + 1})
            s = logical2str(varargin{i + 1});
        else
            s = ['''' (varargin{i + 1}) ''''];
        end
        if (param_report)
            fprintf('# setting %-25s = %-15s ( <a href=" ">default</a>  )\n', varargin{i}, mat2str(s));
        end
        eval(['options.' varargin{i} ' = [' s '];']);
    else
        optionsmem = rmfield(optionsmem, varargin{i});
        if (param_report)
            fprintf('# using   %-25s = %-15s ( <a href=" ">assigned</a> )\n', varargin{i}, mat2str(eval(['options.' varargin{i}])));
        end
    end
end
if (param_report)
    if (isstruct(optionsmem))
        fn_ = fieldnames(optionsmem);
        for fn = 1:length(fn_)
            try
                fprintf('# using   %-25s = %-15s (<a href=" ">not tested</a>)\n', fn_{fn}, mat2str(options.(fn_{fn})));
            catch
                fprintf('# using   %-25s                   (<a href=" ">not tested</a>)\n', fn_{fn});
            end
        end
    end
end

function s=logical2str(e)
if e
    s = 'true';
else
    s = 'false';
end