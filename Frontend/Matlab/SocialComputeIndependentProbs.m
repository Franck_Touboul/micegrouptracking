function [obj, probs] = SocialComputeIndependentProbs(obj)
nZones = length(obj.ROI.ZoneNames);
probs = zeros(obj.nSubjects, nZones);
for i=1:nZones
    probs(:, i) = sum(obj.zones(:, obj.valid) == i, 2);
end
probs = probs / sum(obj.valid);

