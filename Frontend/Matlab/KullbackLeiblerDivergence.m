function d = KullbackLeiblerDivergence(P, Q)
d = P * (flog2(P) - flog2(Q))';
