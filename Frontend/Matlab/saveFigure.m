function saveFigure(basename)
path = [pwd '/'];
if (nargin < 1)
    [name, path] = uiputfile({'*.fig;*.png;*.eps'});
    [pathstr, basename] = fileparts(name);
end

name = [path, basename];
fprintf('# in <a href="%s">''%s''</a>\n', path, path);
fprintf('# - creating ''<a href="%s">%s.fig</a>''\n', [name '.fig'], basename);
saveas(gcf, [name '.fig']);
if ~isunix || ismac
    fprintf('# - creating ''<a href="%s">%s.png</a>''\n', [name '.png'], basename);
    saveas(gcf, [name '.png']);
    fprintf('# - creating ''<a href="%s">%s.eps</a>''\n', [name '.eps'], basename);
%    saveas(gcf, [name '.eps'], 'epsc2');
    print(gcf, [name '.eps'], '-dpsc2');
else
    fprintf('# - creating ''<a href="%s">%s.eps</a>''\n', [name '.eps'], basename);
    print(gcf, [name '.eps'], '-dpsc2');
    fprintf('# - creating ''<a href="%s">%s.png</a>''\n', name, basename);
    try
        system(['setenv TMPDIR "/tmp"; pstopnm -stdout -dpi=300 -portrait ' name '.eps | pnmtopng > ' name '.png']);
    catch
        print(gcf, [name '.png'], '-dpng');
    end
    
end
