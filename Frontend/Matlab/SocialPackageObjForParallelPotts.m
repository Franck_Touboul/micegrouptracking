function obj = SocialPackageObjForParallelPotts(obj)
temp = struct();
fields = {'count', 'initialPottsWeights', 'Options', 'data', 'n', 'nSubjects', 'ROI', 'valid', 'nFrames', 'FilePrefix', 'OutputPath', 'Output', 'OutputToFile', 'OutputInOldFormat'};
for i=1:length(fields)
    f = fields{i};
    temp.(f) = obj.(f);
end
obj = temp;