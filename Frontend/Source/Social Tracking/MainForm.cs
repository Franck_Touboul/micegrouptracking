﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.Text.RegularExpressions;
//using System.Windows.s;
using System.Collections;

using DirectShowLib;

using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;

using Microsoft.DirectX;
using Microsoft.DirectX.Direct3D;
using Microsoft.Win32;

using csmatio.types;
using csmatio.io;
using SocialMatlab;

namespace Social_Tracking
{
    public partial class MainForm : Form
    {
        const int WM_GRAPHNOTIFY = 0x00008001;

        /// <summary>
        /// Location Information
        /// </summary>
        private string currentTrackFilename = "";
        private MLStructure social = null;
        private MLDouble x = null;
        private MLDouble y = null;
        private MLDouble time = null;
        private int timeIndex = 0;
        private MLDouble colors = null;
        private MLDouble zones = null;
        private ArrayList labels = null;

        //private Polygon areaPolygon;
        SplashScreen splash;

        /// <summary>
        /// DirectShow Filter Graph
        /// </summary>
        private IFilterGraph2 graphBuilder = null;
        private IMediaControl mediaControl = null;
        private IBaseFilter vmr9 = null;
        private IVMRMixerBitmap9 mixerBitmap = null;
        private IVMRWindowlessControl9 windowlessCtrl = null;
        private IMediaPosition mediaPosition;
        private IMediaEventEx mediaEvt = null;
        private Boolean handlersAdded;

        private Color colorKey = Color.Violet; // The color use as ColorKey for GDI operations
        private Bitmap colorKeyBitmap; // A RGB bitmap used for GDI operations.

        private Boolean isTimer;
        private System.Threading.Timer timer;

        private double m_framePerSec;
        private int m_videoWidth = 0;
        private int m_videoHeight = 0;
        private double lastDelay = 0;
        
        private GlobalMouseHandler globalClick = new GlobalMouseHandler();

        private double prevPosition = -100;

        private string[] commandLineArgs;

        private ArrayList subjectDataArray = new ArrayList();
        private ArrayList subjectAllDataArray = new ArrayList();

        private ArrayList allEvents = new ArrayList();
        private int currEventGroupIndex = 0;
        private int currEventIndex = 0;
        private int prevEventIndex = 0;

        private static readonly object eventSetLocker = new object();
        private static bool eventSetRunning = false;
        
//        private MyTrackBar myTrackBar;

        public MainForm(string[] args)
        {
            InitializeComponent();

            commandLineArgs = args;

            // We paint the windows ourself
            this.SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);

            // Get the colorkeyed bitmap without antialiasing
            //colorKeyBitmap = BitmapGenerator.GenerateColorKeyBitmap(colorKey, false);
            // Get the bitmap with alpha transparency
            //alphaBitmap = BitmapGenerator.GenerateAlphaBitmap();
            // Start Times
            TimerCallback tcb = updateOverlay;
            AutoResetEvent autoEvent = new AutoResetEvent(false);

            numOfSubjTB.Value = decimal.Parse(Properties.Settings.Default.numberOfSubjects);

            timer = new System.Threading.Timer(tcb, autoEvent, 0, Timeout.Infinite);

            videoFileTB.Text = Properties.Settings.Default.videoFile;
            trackingFileTB.Text = Properties.Settings.Default.trackFile;
            overlayAuto.Checked = true;

            // Areas
            foreach (string s in Properties.Settings.Default.areas)
            {
                areasLB.Items.Add(s);
            }

            //
            Application.AddMessageFilter(globalClick);


            subjectTable.AutoScroll = true;
            subjectTable.VerticalScroll.Visible = true;
            subjectTable.HorizontalScroll.Visible = false;

            Jump1.Text = Properties.Settings.Default.Jump1.ToString();
            Jump2.Text = Properties.Settings.Default.Jump2.ToString();
            Jump3.Text = Properties.Settings.Default.Jump3.ToString();
            Jump4.Text = Properties.Settings.Default.Jump4.ToString();
            Jump5.Text = Properties.Settings.Default.Jump5.ToString();

            //
            /*Graphics dc = sliderBackImage.CreateGraphics();
            Image image = new Bitmap(;
            TextureBrush tBrush = new TextureBrush(image);
            Pen blackPen = new Pen(Color.Black);
            dc.FillRectangle(tBrush, new Rectangle(0, 0, 200, 200));
            dc.DrawRectangle(blackPen, new Rectangle(0, 0, 200, 200));*/
            /*myTrackBar = new MyTrackBar();
            myTrackBar.ValueChanged += new MyTrackBar.ValueChangedHandler(myTrackBar_ValueChanged);
            VideoControl.Controls.Add(myTrackBar);
            myTrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            myTrackBar.Enabled = false;*/


            while (tabControl1.TabCount > 3)
                tabControl1.TabPages.RemoveAt(tabControl1.TabCount - 1);

            //sampleUserControl.TextboxValidated += new EventHandler(this.CustomEvent_Handler);
            mediaControlBox.TimeChanged += new EventHandler(this.mediaControlBox_ValueChanged);
            mediaControlBox.MediaControlButtonPressed += new EventHandler(this.playBtn_Click);
            mediaControlBox.KeyDownInControl += new System.Windows.Forms.KeyEventHandler(MainForm_KeyDown);
        }

        private void CloseInterfaces()
        {
            if (mediaControl != null)
            {
                mediaControl.Stop();
                mediaEvt.SetNotifyWindow((IntPtr)0, WM_GRAPHNOTIFY, (IntPtr)0);
            }

            if (handlersAdded)
                RemoveHandlers();

            if (vmr9 != null)
            {
                Marshal.ReleaseComObject(vmr9);
                vmr9 = null;
                windowlessCtrl = null;
                mixerBitmap = null;
            }

            if (graphBuilder != null)
            {
                Marshal.ReleaseComObject(graphBuilder);
                graphBuilder = null;
                mediaControl = null;
                mediaPosition = null;
                mediaEvt = null;
            }

        }

        public void ShowSplashScreen()
        {
            splash = new SplashScreen();
            splash.Show();
            splash.SetBounds(this.Size.Width / 2 - splash.Size.Width / 2 + this.Location.X,
                this.Size.Height / 2 - splash.Size.Height / 2 + this.Location.Y,
                splash.Size.Width, splash.Size.Height);
            splash.Update();
        }

        public void LoadFile(string filename, string trackFilename)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ShowSplashScreen();

                ClearTrack();
                CloseInterfaces();

                statusBox.Text += System.Environment.NewLine + "loading file '" + filename + "'...";

                if (trackFilename != null && trackFilename.Trim() != "" && !overlayIgnore.Checked)
                {
                    statusBox.Text += System.Environment.NewLine + "loading tracking data '" + trackFilename + "'...";
                    currentTrackFilename = trackFilename;
                    LoadTrack(currentTrackFilename);
                    //loadTrackingWorker.RunWorkerAsync();
                }
                else
                {
                    currentTrackFilename = "";
                }

                BuildGraph(filename);
                while (loadTrackingWorker.IsBusy)
                    System.Threading.Thread.Sleep(100);
                RunGraph();
                splash.Hide();
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public void LoadFile(string filename)
        {
            ShowSplashScreen();

            ClearTrack();
            CloseInterfaces();
            BuildGraph(filename);
            RunGraph();

            splash.Hide();
        }

        private void ClearTrack()
        {
            social = null;
            x = null;
            y = null;
            colors = null;
            time = null;
            zones = null;
            labels = null;
        }

        private void LoadTrack(string filename)
        {
            // load MAT file
            //MatFileReader mfr = new MatFileReader("D:\\Oren\\Projects\\SocialMice\\Res\\Trial.Social.v6.mat");
            timeIndex = 0;
            try
            {
                MatFileReader mfr = new MatFileReader(filename);
                social = null;
                labels = new ArrayList();
                foreach (MLArray mla in mfr.Data)
                {
                    if (mla.Name == "social")
                        social = (MLStructure)mla;
                }
                if (social != null)
                {
                    x = (MLDouble)social["x"];
                    y = (MLDouble)social["y"];
                    colors = (MLDouble)((MLStructure)((MLStructure)social["meta"])["subject"])["centerColors"]; // social.meta.subject.centerColors
                    time = (MLDouble)social["time"];
                    try
                    {
                        zones = (MLDouble)social["zones"];
                        MLCell labelsCell = (MLCell)social["labels"];
                        foreach (MLChar str in labelsCell.Cells) {
                            statusBox.Text += new String(str.ExportChar()) + System.Environment.NewLine;
                            labels.Add(new String(str.ExportChar()));
                        }
                    }
                    catch (Exception)
                    {
                    }

                    try
                    {
                        //social["Events"]
                        eventTabs.TabPages.Clear();
                        MLStructure EventGroup = ((MLStructure)social["Events"]);
                        allEvents.Clear();
                        currEventGroupIndex = 0;
                        for (int e = 0; e < EventGroup.Dimensions[1]; ++e)
                        {
                            string Title = new String(((MLChar)EventGroup["Title", e]).ExportChar());
                            TabPage currPage = new TabPage(Title);
                            eventTabs.TabPages.Add(currPage);
                            if (e == 0)
                                tabControl1.SelectedTab = eventsPage;
                            DataGridView eventsGrid = new DataGridView();
                            eventsGrid.BorderStyle = BorderStyle.None;
                            eventsGrid.EnableHeadersVisualStyles = false;
                            eventsGrid.Location = new Point(0, 0);
                            eventsGrid.Dock = DockStyle.Fill;
                            eventsGrid.Height = currPage.Height;
                            eventsGrid.Width = currPage.Width;
                            eventsGrid.CellEnter += new DataGridViewCellEventHandler(eventsGridView_CellEnter);
                            currPage.Controls.Add(eventsGrid);
                            eventsGrid.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
                            eventsGrid.RowHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight;
                            eventsGrid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

                            eventsGrid.Columns.Add("Start", "Start");
                            eventsGrid.Columns.Add("End", "End");
                            //eventsGrid.Columns.Add("Desc", "Desc");
                            eventsGrid.Columns.Add("Comments", "Comments");

                            MLDouble Instance = ((MLDouble)EventGroup["data", e]);
                            int nvals = Math.Min(Instance.Dimensions[1], 500);
                            eventsGrid.Rows.Add(nvals);
                            List<Event> eventGroup = new List<Event>(Instance.Dimensions[1]);
                            allEvents.Add(eventGroup);
                            eventGroup.Capacity = nvals;
                            for (int i = 0; i < nvals; ++i)
                            {
                                double StartTimeSec = Instance.Get(0, i);
                                double EndTimeSec = Instance.Get(1, i);
                                //string Desc = new String(((MLChar)Instance["Desc", i]).ExportChar());
                                string Desc = "";
                                //MLUInt8 Members = (MLUInt8)Instance["Members", i];
                                string Members = ((int)(Instance.Get(2, i))).ToString();
                                //
                                eventsGrid.Rows[i].HeaderCell.Value = i.ToString();
                                eventsGrid.Rows[i].Cells[0].Value = SecondsToTime(StartTimeSec); // @@@
                                eventsGrid.Rows[i].Cells[1].Value = SecondsToTime(EndTimeSec);// @@@
                                //eventsGrid.Rows[i].Cells[2].Value = Desc;
                                eventsGrid.Rows[i].Cells[2].Value = "";
                                //
                                Event currEvent = new Event();
                                currEvent.StartTimeSec = StartTimeSec;
                                currEvent.EndTimeSec = EndTimeSec;
                                currEvent.Desc = Desc;
                                eventGroup.Add(currEvent);

                                currEvent.Members = new int[Members.Length];
                                for (int j = 0; j < Members.Length; ++j)
                                    currEvent.Members[j] = int.Parse(Members[j].ToString());
                            }
                            if (eventsGrid.Rows.Count > 0)
                                eventsGrid.CurrentCell = eventsGrid.Rows[0].Cells[0];
                        }

                    }
                    catch (Exception)
                    {
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("failed to read track file: " + ex.Message);
            }

        }

        private void LoadTextualTrack(string filename)
        {
            StreamReader SR = new StreamReader(filename);

            int[] dims = new int[2];
            dims[0] = 1;
            dims[1] = 1;
            time = new MLDouble("time", dims);

            string line;
            int index = 0;
            while (true)
            {
                line = SR.ReadLine();
                if (line == null)
                    break;
                string[] fields = line.Split(' ');
                time.Set(double.Parse(fields[0]), index);
                ++index;
            }
        }


        private void BuildGraph(string fileName)
        {
            int hr = 0;

            try
            {
                graphBuilder = (IFilterGraph2)new FilterGraph();
                mediaControl = (IMediaControl)graphBuilder;
                mediaEvt = (IMediaEventEx)graphBuilder;
                mediaPosition = (IMediaPosition)graphBuilder;

                vmr9 = (IBaseFilter)new VideoMixingRenderer9();

                ConfigureVMR9InWindowlessMode();

                hr = graphBuilder.AddFilter(vmr9, "Video Mixing Renderer 9");
                DsError.ThrowExceptionForHR(hr);

                hr = graphBuilder.RenderFile(fileName, null);
                DsError.ThrowExceptionForHR(hr);

                mixerBitmap = (IVMRMixerBitmap9)vmr9;

                SetMixerSettings();

                IPin iPinSource = DsFindPin.ByDirection(vmr9, PinDirection.Input, 0);
                AMMediaType ammtype = new AMMediaType();
                iPinSource.ConnectionMediaType(ammtype);
                m_framePerSec = 25; //@@@@@@@
                timeIndex = 0;
            }
            catch (Exception e)
            {
                CloseInterfaces();
                MessageBox.Show("An error occured during the graph building : \r\n\r\n" + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ConfigureVMR9InWindowlessMode()
        {
            int hr = 0;

            IVMRFilterConfig9 filterConfig = (IVMRFilterConfig9)vmr9;

            // Not really needed for VMR9 but don't forget calling it with VMR7
            hr = filterConfig.SetNumberOfStreams(1);
            DsError.ThrowExceptionForHR(hr);

            // Change VMR9 mode to Windowless
            hr = filterConfig.SetRenderingMode(VMR9Mode.Windowless);
            DsError.ThrowExceptionForHR(hr);

            windowlessCtrl = (IVMRWindowlessControl9)vmr9;

            // Set "Parent" window
            hr = windowlessCtrl.SetVideoClippingWindow(videoPanel.Handle);
            DsError.ThrowExceptionForHR(hr);

            // Set Aspect-Ratio
            hr = windowlessCtrl.SetAspectRatioMode(VMR9AspectRatioMode.LetterBox);
            DsError.ThrowExceptionForHR(hr);

            // Add delegates for Windowless operations
            AddHandlers();

            // Call the resize handler to configure the output size
            MainForm_ResizeMove(null, null);
        }

        private void AddHandlers()
        {
            // Add handlers for VMR purpose
            this.Paint += new PaintEventHandler(MainForm_Paint); // for WM_PAINT
            this.Resize += new EventHandler(MainForm_ResizeMove); // for WM_SIZE
            this.Move += new EventHandler(MainForm_ResizeMove); // for WM_MOVE
            SystemEvents.DisplaySettingsChanged += new EventHandler(SystemEvents_DisplaySettingsChanged); // for WM_DISPLAYCHANGE
            handlersAdded = true;
        }

        private void RemoveHandlers()
        {
            // remove handlers when they are no more needed
            handlersAdded = false;
            this.Paint -= new PaintEventHandler(MainForm_Paint);
            this.Resize -= new EventHandler(MainForm_ResizeMove);
            this.Move -= new EventHandler(MainForm_ResizeMove);
            SystemEvents.DisplaySettingsChanged -= new EventHandler(SystemEvents_DisplaySettingsChanged);
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            if (windowlessCtrl != null)
            {
                IntPtr hdc = e.Graphics.GetHdc();
                int hr = windowlessCtrl.RepaintVideo(this.Handle, hdc);
                e.Graphics.ReleaseHdc(hdc);
            }
        }

        private void MainForm_ResizeMove(object sender, EventArgs e)
        {
            if (windowlessCtrl != null)
            {
                int hr = windowlessCtrl.SetVideoPosition(null, DsRect.FromRectangle(videoPanel.ClientRectangle));
            }
        }

        private void SystemEvents_DisplaySettingsChanged(object sender, EventArgs e)
        {
            if (windowlessCtrl != null)
            {
                int hr = windowlessCtrl.DisplayModeChanged();
            }
        }


        private void SetMixerSettings()
        {
            int hr = 0;
            VMR9AlphaBitmap alphaBmp;

            // Old school GDI stuff...
            if (colorKeyBitmap == null || colorKeyBitmap.Height != m_videoHeight || colorKeyBitmap.Width != m_videoWidth)
                colorKeyBitmap = new Bitmap(100, 100, PixelFormat.Format32bppArgb);

            Graphics g = Graphics.FromImage(colorKeyBitmap);
            //g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            //g.SmoothingMode = SmoothingMode.HighQuality;
            IntPtr hdc = g.GetHdc();
            IntPtr memDC = NativeMethodes.CreateCompatibleDC(hdc);
            IntPtr hBitmap = colorKeyBitmap.GetHbitmap();
            NativeMethodes.SelectObject(memDC, hBitmap);

            // Set Alpha Bitmap Parameters for using a GDI DC
            alphaBmp = new VMR9AlphaBitmap();
            alphaBmp.dwFlags = VMR9AlphaBitmapFlags.hDC | VMR9AlphaBitmapFlags.SrcColorKey | VMR9AlphaBitmapFlags.FilterMode;
            alphaBmp.hdc = memDC;
            alphaBmp.rSrc = new DsRect(0, 0, colorKeyBitmap.Size.Width, colorKeyBitmap.Size.Height);
            alphaBmp.rDest = GetDestRectangle();
            alphaBmp.clrSrcKey = ColorTranslator.ToWin32(colorKey);
            alphaBmp.dwFilterMode = VMRMixerPrefs.PointFiltering;
            alphaBmp.fAlpha = 0.0f;

            // Set Alpha Bitmap Parameters
            hr = mixerBitmap.SetAlphaBitmap(ref alphaBmp);
            DsError.ThrowExceptionForHR(hr);

            // Release GDI handles
            NativeMethodes.DeleteObject(hBitmap);
            NativeMethodes.DeleteDC(memDC);
            g.ReleaseHdc(hdc);
            g.Dispose();
        }


        private NormalizedRect GetDestRectangle()
        {
            int hr = 0;
            int width, height, arW, arH;
            NormalizedRect rect = new NormalizedRect();

            hr = windowlessCtrl.GetNativeVideoSize(out width, out height, out arW, out arH);
            DsError.ThrowExceptionForHR(hr);

            m_videoWidth = width;
            m_videoHeight = height;

            rect.top = 0.0f;
            rect.left = 0.0f;
            rect.bottom = 1.0f;
            rect.right = 1.0f;

            return rect;
        }

        public double GetTimeInSeconds()
        {
            if (mediaPosition == null)
                return 0;
            double origPosition;
            mediaPosition.get_CurrentPosition(out origPosition);
            return origPosition;
        }

        public void GoToTime(double seconds)
        {
            if (mediaPosition == null)
                return;
            if (subjectDataArray != null)
            {
                if (subjectDataArray.Count > 0)
                    clearOverlay();
                subjectDataArray.Clear();

            }
            mediaPosition.put_CurrentPosition(seconds);
            this.BeginInvoke(new InvokeDelegate(updateOverlayDelegate));
        }

        private int getCurrentTrackingIndex()
        {
            double position;
            mediaPosition.get_CurrentPosition(out position);
            return getCurrentTrackingIndex(position);
        }

        private int getCurrentTrackingIndex(double currentPosition)
        {
            if (time == null)
                return 0;
            int frame = -1;
            for (int i = timeIndex; i < time.Size; ++i)
            {
                if (time.GetReal(i) > currentPosition)
                {
                    if (i > 0 && time.GetReal(i - 1) > currentPosition)
                        break;
                    frame = i;
                    break;
                }
            }
            if (frame < 0)
            {
                frame = 0;
                for (int i = 0; i < timeIndex; ++i)
                {
                    if (time.GetReal(i) > currentPosition)
                    {
                        frame = i;
                        break;
                    }
                }
            }
            return frame;
        }

        private Bitmap getOverlayBitmap()
        {
            if (colorKeyBitmap == null || colorKeyBitmap.Height != m_videoHeight || colorKeyBitmap.Width != m_videoWidth)
                colorKeyBitmap = new Bitmap(m_videoWidth, m_videoHeight, PixelFormat.Format32bppArgb);
            return colorKeyBitmap;
        }

        public int getCurrentFrame()
        {
            double position;
            mediaPosition.get_CurrentPosition(out position);
            return (int)(position * m_framePerSec);
        }

        Event getCurrentEvent(double time)
        {
            lock (eventSetLocker)
            {
                if (!eventSetRunning)
                    eventSetRunning = true;
                else
                    return null;
            }
            try
            {
                List<Event> currEventGroup = (List<Event>)allEvents[currEventGroupIndex];
                if (currEventGroup.Count == 0)
                    return null;
                if (prevEventIndex >= currEventGroup.Count)
                    prevEventIndex = 0;
                int index = prevEventIndex;

                while (index < currEventGroup.Count - 1 && currEventGroup[index].EndTimeSec < time)
                    ++index;
                while (index > 0 && currEventGroup[index - 1].EndTimeSec > time)
                    --index;
                if (prevEventIndex != index)
                {
                    DataGridView eventGridView = (DataGridView)eventTabs.SelectedTab.Controls[0];
                    if (!eventGridView.Rows[index].Selected)
                    {
                        eventGridView.ClearSelection();
                        eventGridView.Rows[index].Selected = true;

                        if (eventGridView.FirstDisplayedScrollingRowIndex + eventGridView.DisplayedRowCount(false) <= index)
                        {
                            eventGridView.FirstDisplayedScrollingRowIndex =
                                index - eventGridView.DisplayedRowCount(false) + 1;
                        }
                        eventGridView.FirstDisplayedScrollingRowIndex = index;
                    }
                    prevEventIndex = index;
                }

                if (currEventGroup[index].StartTimeSec <= time && currEventGroup[index].EndTimeSec >= time)
                {
                    currEventIndex = index;
                    return currEventGroup[index];
                } //else
                //((DataGridView)eventTabs.SelectedTab.Controls[0]).ClearSelection();
                return null;
            }
            catch
            {
            }
            finally
            {
                eventSetRunning = false;
            }
            return null;
        }

        List<Event> getCurrentEvents(double time)
        {
            lock (eventSetLocker)
            {
                if (!eventSetRunning)
                    eventSetRunning = true;
                else
                    return null;
            }
            try
            {
                List<Event> currEventGroup = (List<Event>)allEvents[currEventGroupIndex];
                if (currEventGroup.Count == 0)
                    return null;
                List<Event> events = new List<Event>();
                if (prevEventIndex >= currEventGroup.Count)
                    prevEventIndex = 0;
                int start = prevEventIndex;

                while (start < currEventGroup.Count - 1 && currEventGroup[start].EndTimeSec < time)
                    ++start;
                while (start > 0 && currEventGroup[start - 1].EndTimeSec > time)
                    --start;
                if (prevEventIndex != start)
                {
                    DataGridView eventGridView = (DataGridView)eventTabs.SelectedTab.Controls[0];
                    if (!eventGridView.Rows[start].Selected)
                    {
                        eventGridView.ClearSelection();
                        eventGridView.Rows[start].Selected = true;
                        eventGridView.FirstDisplayedScrollingRowIndex = start;
                    }
                    prevEventIndex = start;
                }

                int index = start;
                while (currEventGroup[index].StartTimeSec <= time && currEventGroup[index].EndTimeSec >= time)
                {
                    currEventIndex = start;
                    ++index;
                } //else
                //((DataGridView)eventTabs.SelectedTab.Controls[0]).ClearSelection();
                return events;
            }
            finally
            {
                eventSetRunning = false;
            }
        }



        private void clearOverlay()
        {
            VMR9AlphaBitmap alphaBmp;
            alphaBmp = new VMR9AlphaBitmap();
            alphaBmp.rDest = GetDestRectangle();

            Bitmap v = getOverlayBitmap(); //new Bitmap(256, 256, PixelFormat.Format24bppRgb);
            Graphics g = Graphics.FromImage(v);
            g.Clear(colorKey);
            IntPtr hdc = g.GetHdc();
            IntPtr memDC = NativeMethodes.CreateCompatibleDC(hdc);
            IntPtr hBitmap = v.GetHbitmap();
            NativeMethodes.SelectObject(memDC, hBitmap);

            // Set Alpha Bitmap Parameters for using a GDI DC
            alphaBmp.dwFlags = VMR9AlphaBitmapFlags.hDC | VMR9AlphaBitmapFlags.SrcColorKey | VMR9AlphaBitmapFlags.FilterMode;
            alphaBmp.hdc = memDC;
            alphaBmp.rSrc = new DsRect(0, 0, v.Size.Width, v.Size.Height);
            alphaBmp.clrSrcKey = ColorTranslator.ToWin32(colorKey);
            alphaBmp.dwFilterMode = VMRMixerPrefs.PointFiltering;
            alphaBmp.fAlpha = 0.75f;

            // Set Alpha Bitmap Parameters
            int hr = mixerBitmap.SetAlphaBitmap(ref alphaBmp);
            DsError.ThrowExceptionForHR(hr);

            // Release GDI handles
            NativeMethodes.DeleteObject(hBitmap);
            NativeMethodes.DeleteDC(memDC);
            g.ReleaseHdc(hdc);
            g.Dispose();

            //double position;
//            mediaPosition.get_CurrentPosition(out position);
            //mediaPosition.put_CurrentPosition(position);
        }

        private void markOnVideo()
        {
            int dx = 10;
            int dy = 10;

            VMR9AlphaBitmap alphaBmp;
            alphaBmp = new VMR9AlphaBitmap();
            alphaBmp.rDest = GetDestRectangle();

            Bitmap v = getOverlayBitmap(); //new Bitmap(256, 256, PixelFormat.Format24bppRgb);
            Graphics g = Graphics.FromImage(v);
            g.Clear(colorKey);
            SolidBrush brush = new SolidBrush(Color.FromArgb(255, 255, 255));
            Font displayFont = new Font("Arial", dx, System.Drawing.FontStyle.Bold);

            foreach (SubjectData s in subjectDataArray)
            {
                Rectangle rect = new Rectangle(s.x - dx, s.y - dy, 2 * dx, 2 * dy);
                g.FillEllipse(brush, rect);
                g.DrawEllipse(Pens.White, rect);

                StringFormat sformst = new StringFormat();
                sformst.Alignment = StringAlignment.Center;
                sformst.LineAlignment = StringAlignment.Center;
                g.DrawString((s.id + 1).ToString(), displayFont, new SolidBrush(Color.FromArgb(50, 50, 50)), new PointF(s.x, s.y), sformst);
            }
            IntPtr hdc = g.GetHdc();
            IntPtr memDC = NativeMethodes.CreateCompatibleDC(hdc);
            IntPtr hBitmap = v.GetHbitmap();
            NativeMethodes.SelectObject(memDC, hBitmap);

            // Set Alpha Bitmap Parameters for using a GDI DC
            alphaBmp.dwFlags = VMR9AlphaBitmapFlags.hDC | VMR9AlphaBitmapFlags.SrcColorKey | VMR9AlphaBitmapFlags.FilterMode;
            alphaBmp.hdc = memDC;
            alphaBmp.rSrc = new DsRect(0, 0, v.Size.Width, v.Size.Height);
            alphaBmp.clrSrcKey = ColorTranslator.ToWin32(colorKey);
            alphaBmp.dwFilterMode = VMRMixerPrefs.PointFiltering;
            alphaBmp.fAlpha = 0.75f;

            // Set Alpha Bitmap Parameters
            int hr = mixerBitmap.SetAlphaBitmap(ref alphaBmp);
            DsError.ThrowExceptionForHR(hr);

            // Release GDI handles
            NativeMethodes.DeleteObject(hBitmap);
            NativeMethodes.DeleteDC(memDC);
            g.ReleaseHdc(hdc);
            g.Dispose();

            double position;
            mediaPosition.get_CurrentPosition(out position);
            mediaPosition.put_CurrentPosition(position);
        }


        public delegate void InvokeDelegate();
        private void updateOverlayDelegate()
        {
            if (mediaPosition == null)
                return;
            try
            {

                int hr = 0;

                isTimer = true;
                double origPosition;
                mediaPosition.get_CurrentPosition(out origPosition);

                mediaControlBox.Time = origPosition * m_framePerSec;
                isTimer = false;

                //goToTime.Text = minutes.ToString( "D2" ) 

                int hours = (int)origPosition / 3600;
                int minutes = ((int)origPosition - hours * 3600) / 60;
                int seconds = (int)origPosition % 60;
                int miliseconds = (int)((origPosition - Math.Floor(origPosition)) * 100);
                currTimeTB.Text = hours.ToString() + ":" + minutes.ToString("D2") + ":" + seconds.ToString("D2") + "." + miliseconds.ToString("D2");
                if (this.ActiveControl != frameBox)
                    frameBox.Text = ((int)mediaControlBox.Time).ToString();

                if (x == null || y == null || social == null || time == null)
                    return;
                if (m_videoHeight == 0 || m_videoWidth == 0)
                    return;
                VMR9AlphaBitmap alphaBmp;
                alphaBmp = new VMR9AlphaBitmap();
                alphaBmp.rDest = GetDestRectangle();

                double currentPosition = origPosition;
                // Old school GDI stuff...
                Bitmap v = getOverlayBitmap(); //new Bitmap(256, 256, PixelFormat.Format24bppRgb);
                Graphics g = Graphics.FromImage(v);
                
                g.Clear(colorKey);
                if (checkBox1.Checked)
                    g.FillRegion(Brushes.Black,  new Region(new Rectangle(0,0,m_videoWidth, m_videoHeight)));
                //g.FillRectangle(Brushes.Green, 0, 0, v.Width, v.Height);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                //Random randObj = new Random();
                //float x = (float)randObj.Next(256);
                //float y = (float)randObj.Next(256);
                //g.FillEllipse(Brushes.Green, x, y, 10f, 10f);

                //Font displayFont = new Font("Times New Roman", 40, System.Drawing.FontStyle.Bold);
                //g.DrawString(minutes.ToString() + ":" + seconds.ToString() + "." + miliseconds.ToString(), displayFont, Brushes.Yellow, new PointF(0, 0));

                int frame = getCurrentTrackingIndex(currentPosition);
                Event currEvent = getCurrentEvent(currentPosition);
                List<int> membersX = null;
                List<int> membersY = null;
                if (currEvent != null)
                {
                    membersX = new List<int>(currEvent.Members.Count());
                    membersY = new List<int>(currEvent.Members.Count());
                }


                timeIndex = frame;
                int dx = 10;
                int dy = 10;
                Pen whitePen = new Pen(Color.White, 2);
                Pen blackPen = new Pen(Color.Black, 2);
                Font displayFont = new Font("Arial", 12, System.Drawing.FontStyle.Bold);
                for (int i = 0; i < x.M; ++i)
                {
                    double currXd = x.GetReal(i, frame);
                    double currYd = y.GetReal(i, frame);
                    double label = 0; 
                    if (zones != null)
                        label = zones.GetReal(i, frame);

                    if (Double.IsNaN(currXd) || Double.IsInfinity(currXd) || Double.IsNaN(currYd) || Double.IsInfinity(currYd))
                        continue;

                    int currX = (int)(currXd);
                    int currY = (int)(currYd);
                    if (currEvent != null)
                    {
                        if (Array.Exists(currEvent.Members, element => element - 1 == i))
                        {
                            membersX.Add(currX);
                            membersY.Add(currY);
                        }

                    }

                    int R = (int)(colors.GetReal(i, 0) * 255);
                    int G = (int)(colors.GetReal(i, 1) * 255);
                    int B = (int)(colors.GetReal(i, 2) * 255);
                    SolidBrush brush = new SolidBrush(Color.FromArgb(R, G, B));
                    Rectangle rect = new Rectangle(currX - dx, (currY - dy), 2 * dx, 2 * dy);
                    Rectangle rect2 = new Rectangle(currX - dx - 2, (currY - dy - 2), 2 * dx + 4, 2 * dy + 4);
                    //g.FillEllipse(brush, rect);
                    g.FillPie(brush, rect, 360.0f / (float)x.M * i, 360.0f / (float)x.M);
                    g.DrawPie(whitePen, rect, 360.0f / (float)x.M * i, 360.0f / (float)x.M);
                    g.DrawEllipse(blackPen, rect2);
                    g.DrawEllipse(whitePen, rect);
                    if (label > 0)
                    {
//                        g.DrawString(label.ToString(), displayFont, Brushes.White, currX + dx, currY + dx);
                        g.DrawString(labels[(int)label-1].ToString(), displayFont, Brushes.White, currX + dx, currY + dx);
                    }
                }



                if (currEvent != null)
                {
                    Pen DashedPen = new Pen(Color.White, 2);
                    DashedPen.DashStyle = DashStyle.Dash;
                    for (int i = 1; i < currEvent.Members.Count(); ++i)
                        g.DrawLine(DashedPen, new Point(membersX[i - 1], membersY[i - 1]), new Point(membersX[i], membersY[i]));
                }

                //v.RotateFlip(RotateFlipType.RotateNoneFlipY);

                IntPtr hdc = g.GetHdc();
                IntPtr memDC = NativeMethodes.CreateCompatibleDC(hdc);
                IntPtr hBitmap = v.GetHbitmap();
                NativeMethodes.SelectObject(memDC, hBitmap);

                // Set Alpha Bitmap Parameters for using a GDI DC
                alphaBmp.dwFlags = VMR9AlphaBitmapFlags.hDC | VMR9AlphaBitmapFlags.SrcColorKey | VMR9AlphaBitmapFlags.FilterMode;
                alphaBmp.hdc = memDC;
                alphaBmp.rSrc = new DsRect(0, 0, v.Size.Width, v.Size.Height);
                alphaBmp.clrSrcKey = ColorTranslator.ToWin32(colorKey);
                alphaBmp.dwFilterMode = VMRMixerPrefs.PointFiltering;
                if (checkBox1.Checked)
                    alphaBmp.fAlpha = 1.0f;
                else
                    alphaBmp.fAlpha = 0.75f;


                double newPosition;
                mediaPosition.get_CurrentPosition(out newPosition);
                lastDelay = newPosition - origPosition;

                // Set Alpha Bitmap Parameters
                hr = mixerBitmap.SetAlphaBitmap(ref alphaBmp);
                DsError.ThrowExceptionForHR(hr);

                // Release GDI handles
                NativeMethodes.DeleteObject(hBitmap);
                NativeMethodes.DeleteDC(memDC);
                g.ReleaseHdc(hdc);
                g.Dispose();
            }
            catch (Exception)
            {

            }
        }

        private void updateOverlay(object source)
        {
            try
            {
                if (mediaPosition != null && windowlessCtrl is VideoMixingRenderer9)
                {
                    double position;
                    mediaPosition.get_CurrentPosition(out position);
                    if (Math.Abs(position - prevPosition) > 0.2)
                    {
                        prevPosition = position;
                        this.BeginInvoke(new InvokeDelegate(updateOverlayDelegate));
                    }
                }

            }
            finally
            {
                timer.Change(40, Timeout.Infinite);
            }

            return;
            /*isTimer = true;
            double currentPosition;
            mediaPosition.get_CurrentPosition(out currentPosition);
            trackBar1.Value = (int)(currentPosition * 100.0);
            isTimer = false;
            minutes = (int)currentPosition / 60;
            seconds = (int)currentPosition % 60;
            label1.Text = "Position: " + minutes.ToString("D2")
                + ":m" + seconds.ToString("D2") + ":s";*/



            // Old school GDI stuff...
        }

        static string SecondsToTime(double secs)
        {
            DateTime time = new DateTime((long)(secs * 10000000));
            return time.ToString("HH:mm:ss");
        }

        private void RunGraph()
        {
            if (mediaControl != null)
            {
                OABool bCsf, bCsb;
                mediaPosition.CanSeekBackward(out bCsb);
                mediaPosition.CanSeekForward(out bCsf);

                Boolean isSeeking = (bCsb == OABool.True) && (bCsf == OABool.True);
                if (isSeeking)
                {
                    mediaControlBox.IsEnabled = true;
                }
                else
                {
                    mediaControlBox.IsEnabled = false;
                }

                double duration;
                mediaPosition.get_Duration(out duration);

                mediaControlBox.TotalDuration = duration * m_framePerSec;
                mediaControlBox.Time = 0;

                totalTimeLabel.Text = SecondsToTime(duration);

                mediaEvt.SetNotifyWindow((IntPtr)this.Handle, WM_GRAPHNOTIFY, (IntPtr)0);

                Play();
            }
        }

        //
        //override to process custom graph notify messages
        //
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_GRAPHNOTIFY)
            {
                if (mediaEvt != null)
                    OnGraphNotify();
                return;
            }
            base.WndProc(ref m);
        }

        //
        //call to process WM_GRAPHNOTIFY message
        //get out of the loop when GetEvent returns not 0
        //
        void OnGraphNotify()
        {
            IntPtr p1, p2;
            EventCode code;

            if (mediaEvt == null)
                return;
            while (mediaEvt.GetEvent(out code, out p1, out p2, 0) == 0)
            {
                mediaEvt.FreeEventParams(code, p1, p2);
                if (code == EventCode.Complete)
                    OnClipCompleted();
            }
        }

        void OnClipCompleted()
        {
            Stop();
            mediaPosition.put_CurrentPosition(0.0);
            Play();
        }

        private void StopGraph()
        {
            if (mediaControl != null)
            {
                int hr = mediaControl.Stop();
                DsError.ThrowExceptionForHR(hr);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (commandLineArgs != null && commandLineArgs.Length > 0)
                LoadFile(commandLineArgs[0]);

        }

        private void Stop()
        {
            if (mediaControl == null)
                return;
            mediaControl.Stop();
            playBtn.Text = "Play";
            mediaControlBox.IsPlaying = false;
        }

        private void Play()
        {
            if (mediaControl == null)
                return;
            mediaControl.Run();
            playBtn.Text = "Pause";
            mediaControlBox.IsPlaying = true;
        }

        private void Pause()
        {
            if (mediaControl == null)
                return;
            mediaControl.Pause();
            playBtn.Text = "Play";
            mediaControlBox.IsPlaying = false;
        }

        private void playBtn_Click(object sender, EventArgs e)
        {
            if (playBtn.Text == "Play")
                Play();
            else
                Pause();
        }

        private void stopBtn_Click(object sender, EventArgs e)
        {
            Stop();
        }

/*        private void timeTrack_MouseDown(object sender, MouseEventArgs e)
        {
            float loc = (float)(e.Location.X - 10);
            float width = (float)(timeTrack.Width - 20);
            int pos = (int)(loc / width * (float)timeTrack.Maximum);
            if (pos < 0)
                pos = 0;
            if (pos > timeTrack.Maximum)
                pos = timeTrack.Maximum;
            timeTrack.Value = pos;
            //            frameBox.Text = timeTrack.Value.ToString();
            updateOverlayDelegate();
        }*/


        private void mediaControlBox_ValueChanged(object sender, EventArgs e)
        {
            if (mediaControl == null)
                return;
            //            frameBox.Text = timeTrack.Value.ToString();
            if (!isTimer)
            {
                Pause();
                GoToTime((float)mediaControlBox.Time / (float)m_framePerSec);
                updateOverlayDelegate();
                //Play();
            }
        }

        private void goToTime_Click(object sender, EventArgs e)
        {
            GoToTime timeForm = new GoToTime(this);
            timeForm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (overlayAuto.Enabled && overlayAuto.Checked)
            {
                trackingFileTB.Text = "";
                string overlayFilename = Regex.Replace(videoFileTB.Text, "\\.[^\\.]*$", ".mat");
                trackingFileTB.Text = overlayFilename;
            }
            LoadFile(videoFileTB.Text, trackingFileTB.Text);
        }

        private void videoFileTB_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.videoFile = ((TextBox)sender).Text;
            Properties.Settings.Default.Save();
        }

        private void trackingFileTB_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.trackFile = ((TextBox)sender).Text;
            Properties.Settings.Default.Save();
        }

        private void numOfSubjTB_ValueChanged(object sender, EventArgs e)
        {
            int newNumberOfSubjects = decimal.ToInt32(((NumericUpDown)sender).Value);
            while (newNumberOfSubjects > subjectsLB.Items.Count)
            {
                string name = "Subject " + (subjectsLB.Items.Count + 1).ToString();
                subjectsLB.Items.Add(name);

                Label nameCell = new Label();
                nameCell.Text = name;
                subjectTable.Controls.Add(nameCell);

                Label countCell = new Label();
                countCell.Text = "0";
                subjectTable.Controls.Add(countCell);
            }
            while (newNumberOfSubjects < subjectsLB.Items.Count)
            {
                subjectsLB.Items.RemoveAt(subjectsLB.Items.Count - 1);
                subjectTable.Controls.RemoveAt(subjectTable.Controls.Count - 1);
                subjectTable.Controls.RemoveAt(subjectTable.Controls.Count - 1);
            }
        }

        private void subjectsLB_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void videoPanel_Click(object sender, EventArgs e)
        {
        }

        public static Control FindFocusedControl(Control control)
        {
            var container = control as ContainerControl;
            while (container != null)
            {
                control = container.ActiveControl;
                container = control as ContainerControl;
            }
            return control;
        }

        private void MatlabTest() {
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (FindFocusedControl(this) is TextBox)
                return;
            e.Handled = true;

            // Gets the key code 
            //statusBox.Text = "KeyCode: " + e.KeyCode.ToString();   
            if (e.KeyCode == Keys.Up)
            {
                GoToTime(GetTimeInSeconds() + int.Parse(Jump1.Text));
                statusBox.Text = "KeyCode: " + e.KeyCode.ToString();
                // perform copy
                //e.Handled = true;
            }
            else
            if (e.KeyCode == Keys.Down)
            {
                GoToTime(GetTimeInSeconds() - int.Parse(Jump1.Text));
                statusBox.Text = "KeyCode: " + e.KeyCode.ToString();
                // perform copy
                //e.Handled = true;
            }
            else if (e.KeyCode == Keys.Left && Control.ModifierKeys == Keys.Control)
            {
                GoToTime(GetTimeInSeconds() - int.Parse(Jump2.Text));
                statusBox.Text = "KeyCode: " + e.KeyCode.ToString();
                // perform copy
                //e.Handled = true;
            }
            else if (e.KeyCode == Keys.Right && Control.ModifierKeys == Keys.Control)
            {
                GoToTime(GetTimeInSeconds() + int.Parse(Jump2.Text));
                statusBox.Text = "KeyCode: " + e.KeyCode.ToString();
                // perform copy
                //e.Handled = true;
            }
            else if (e.KeyCode == Keys.Up && Control.ModifierKeys == Keys.Control)
            {
                GoToTime(GetTimeInSeconds() + int.Parse(Jump3.Text));
                statusBox.Text = "KeyCode: " + e.KeyCode.ToString();
                // perform copy
                //e.Handled = true;
            }
            else if (e.KeyCode == Keys.Down && Control.ModifierKeys == Keys.Control)
            {
                GoToTime(GetTimeInSeconds() - int.Parse(Jump3.Text));
                statusBox.Text = "KeyCode: " + e.KeyCode.ToString();
                // perform copy
                //e.Handled = true;
            }
            else if (e.KeyCode == Keys.Left && Control.ModifierKeys == Keys.Control && Control.ModifierKeys == Keys.Shift)
            {
                GoToTime(GetTimeInSeconds() - int.Parse(Jump4.Text));
                statusBox.Text = "KeyCode: " + e.KeyCode.ToString();
                // perform copy
                //e.Handled = true;
            }
            else if (e.KeyCode == Keys.Right && Control.ModifierKeys == Keys.Control && Control.ModifierKeys == Keys.Shift)
            {
                GoToTime(GetTimeInSeconds() + int.Parse(Jump4.Text));
                statusBox.Text = "KeyCode: " + e.KeyCode.ToString();
                // perform copy
                //e.Handled = true;
            }
            else if (e.KeyCode == Keys.Down && Control.ModifierKeys == Keys.Control && Control.ModifierKeys == Keys.Shift)
            {
                GoToTime(GetTimeInSeconds() - int.Parse(Jump5.Text));
                statusBox.Text = "KeyCode: " + e.KeyCode.ToString();
                // perform copy
                //e.Handled = true;
            }
            else if (e.KeyCode == Keys.Up && Control.ModifierKeys == Keys.Control && Control.ModifierKeys == Keys.Shift)
            {
                GoToTime(GetTimeInSeconds() + int.Parse(Jump5.Text));
                statusBox.Text = "KeyCode: " + e.KeyCode.ToString();
                // perform copy
                //e.Handled = true;
            }
            else if (e.KeyCode == Keys.Space)
            {
                playBtn_Click(null, null);
                // perform copy
                //e.Handled = true;
            }
            else if (e.KeyCode == Keys.Right)
            {
                GoToTime(GetTimeInSeconds() + 1.0f/m_framePerSec);
            }
            else if (e.KeyCode == Keys.Left)
            {
                GoToTime(GetTimeInSeconds() - 1.0f / m_framePerSec);
            }
            else if (e.KeyCode == Keys.PageDown)
            {
                DataGridView currGrid = (DataGridView)eventTabs.SelectedTab.Controls[0];
                int prevRow = currGrid.CurrentCell.RowIndex;
                if (prevRow + 1 < currGrid.Rows.Count)
                {
  //                  currGrid.ClearSelection();
   //                 currGrid.Rows[prevRow + 1].Selected = true;
//                    currGrid.c = prevRow + 1;
                    currGrid.CurrentCell = currGrid.Rows[prevRow + 1].Cells[0];
                }
            }
            else if (e.KeyCode == Keys.PageUp)
            {
                DataGridView currGrid = (DataGridView)eventTabs.SelectedTab.Controls[0];
                int prevRow = currGrid.CurrentCell.RowIndex;
                if (prevRow - 1 > 0)
                {
//                    currGrid.ClearSelection();
//                    currGrid.Rows[prevRow - 1].Selected = true;
                    currGrid.CurrentCell = currGrid.Rows[prevRow - 1].Cells[0];


                }
            }

            statusBox.Text = e.Handled.ToString();
        }



        private void MainForm_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
       {
//
       }

        private void addAreaBtn_Click(object sender, EventArgs e)
        {
            areasLB.Items.Add(addAreaTB.Text);
            Properties.Settings.Default.areas.Add(addAreaTB.Text);
        }

        private void removeAreaBtn_Click(object sender, EventArgs e)
        {
            areasLB.Items.RemoveAt(areasLB.SelectedIndex);
            Properties.Settings.Default.areas.Add(addAreaTB.Text);
        }

        private void frameBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void frameBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                GoToTime((float)int.Parse(frameBox.Text) / m_framePerSec);
                e.Handled = true;
            }
        }

        private void frameBox_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void MainForm_MouseClick(object sender, MouseEventArgs e)
        {
            this.Select();
        }

        private void MainForm_Click(object sender, EventArgs e)
        {
            this.Select();
        }

        private void videoFileTB_MouseCaptureChanged(object sender, EventArgs e)
        {

        }

        private void trackingFileTB_Click(object sender, EventArgs e)
        {

        }

        public class GlobalMouseHandler : IMessageFilter
        {

            private const int WM_LBUTTONDOWN = 0x201;

            public bool PreFilterMessage(ref Message m)
            {
                if (m.Msg == WM_LBUTTONDOWN)
                {
                    //MessageBox.Show("beep");
                    // Do stuffs
                }
                return false;
            }
        }

        private void overlayAuto_CheckedChanged(object sender, EventArgs e)
        {
            if (overlayAuto.Checked)
            {
                trackingFileTB.Enabled = false;
                trackingFileBtn.Enabled = false;
            }
            else
            {
                trackingFileTB.Enabled = true;
                trackingFileBtn.Enabled = true;
            }
        }

        private void overlayIgnore_CheckedChanged(object sender, EventArgs e)
        {
            if (overlayIgnore.Checked)
            {
                overlayAuto.Enabled = false;
                trackingFileTB.Enabled = false;
                trackingFileBtn.Enabled = false;
            }
            else
            {
                overlayAuto.Enabled = true;
                overlayAuto_CheckedChanged(sender, e);
            }
        }


        private void MainForm_DragDrop(object sender, DragEventArgs e)
        {
            string[] s = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            if (s.Length > 0)
            {
                videoFileTB.Text = s[0];
                button1_Click(sender, e);
            }
        }

        private void MainForm_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;
        }

        private void MainForm_Paint_1(object sender, PaintEventArgs e)
        {
            try
            {
                ConfigureVMR9InWindowlessMode();
            }
            catch (Exception)
            {

            }

        }

        private void addSubjLocBtn_Click(object sender, EventArgs e)
        {
            foreach (SubjectData s in subjectDataArray)
            {
                Label curr = subjectTable.Controls[s.id * subjectTable.ColumnCount + 1] as Label;
                if (curr != null) 
                    curr.Text = (int.Parse(curr.Text) + 1).ToString();
                subjectAllDataArray.Add(s);
            }

            for (int i = 0; i < subjectTable.RowCount; ++i)
            {
                Label curr = subjectTable.Controls[i * subjectTable.ColumnCount + 1] as Label;
                if (curr != null)
                    curr.Text = "0";
            }

            foreach (SubjectData s in subjectAllDataArray)
            {
                Label curr = subjectTable.Controls[s.id * subjectTable.ColumnCount + 1] as Label;
                if (curr != null)
                    curr.Text = (int.Parse(curr.Text) + 1).ToString();
            }
            subjectDataArray.Clear();
        }

        private void subjectTable_Paint(object sender, PaintEventArgs e)
        {
        }

        private void subjRestartBtn_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < subjectTable.RowCount; ++i)
            {
                Label curr = subjectTable.Controls[i * subjectTable.ColumnCount + 1] as Label;
                if (curr != null)
                    curr.Text = "0";
            }
            subjectAllDataArray.Clear();
        }



        private void videoPanel_MouseClick(object sender, MouseEventArgs e)
        {
            if (subjectsLB.SelectedIndex >= 0)
            {
                float x = (float)(e.Location.X);
                float y = (float)(e.Location.Y);

                float cHeight = (float)videoPanel.ClientRectangle.Height;
                float cWidth = (float)videoPanel.ClientRectangle.Width;

                float vHeight = (float)m_videoHeight;
                float vWidth = (float)m_videoWidth;

                float clientRatio = cHeight / cWidth;
                float videoRatio = vHeight / vWidth;

                if (clientRatio > videoRatio)
                {
                    x = x / cWidth * vWidth;
                    float pHeight = cWidth * videoRatio;
                    y = (y - (cHeight - pHeight) / 2) / pHeight * vHeight;
                }
                else
                {
                    y = y / cHeight * vHeight;
                    float pWidth = cHeight / videoRatio;
                    x = (x - (cWidth - pWidth) / 2) / pWidth * vWidth;
                }

                if (tabControl1.SelectedTab == areasPage)
                {
                }
                else if (tabControl1.SelectedTab == subjectsPage)
                {
                    //                markOnVideo((int)x, (int)y, subjectsLB.SelectedIndex + 1);
                    foreach (SubjectData s in subjectDataArray)
                    {
                        if (s.id == subjectsLB.SelectedIndex)
                        {
                            subjectDataArray.Remove(s);
                            break;
                        }
                    }
                    subjectDataArray.Add(new SubjectData((int)x, (int)y, subjectsLB.SelectedIndex));
                    markOnVideo();
                    statusBox.Text += "x: " + ((int)x).ToString() + ", y: " + ((int)y).ToString() + System.Environment.NewLine;
                }
            }
        }

        private void videoPanel_MouseHover(object sender, EventArgs e)
        {
        }

        private void videoPanel_MouseMove(object sender, MouseEventArgs e)
        {
            /*if (ps.X > 0)
            {
                ControlPaint.DrawReversibleLine(this.PointToScreen(ps), this.PointToScreen(pe), Color.Black);
                pe = new Point(e.X, e.Y);
                ControlPaint.DrawReversibleLine(this.PointToScreen(ps), this.PointToScreen(pe), Color.Black);
                //locLabel.Location.X=1;
            }*/

        }

        Point ps = new Point(-1, -1);
        Point pe = new Point(-1, -1);
        Label locLabel = new Label();        
        private void videoPanel_MouseDown(object sender, MouseEventArgs e)
        {
            locLabel.Text = "X";
            this.Controls.Add(locLabel);
            ps = new Point(e.X, e.Y);
            pe = ps;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            AnalyzeMovies analyzeForm = new AnalyzeMovies();
            analyzeForm.Show();
        }

        private void tasksBox_DragDrop(object sender, DragEventArgs e)
        {
            string[] s = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            if (s.Length > 0)
            {
                //videoFileTB.Text = s[0];
                AnalyzeMovies analyzeForm = new AnalyzeMovies(s);
                DialogResult returnVal = analyzeForm.ShowDialog();
                if (returnVal == DialogResult.OK)
                {

                }
            }
        }

        private void loadTrackingWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            LoadTrack(currentTrackFilename);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void statusBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void eventsGridView_CellEnter(object sender,
            DataGridViewCellEventArgs e)
        {
            try
            {
                if (m_framePerSec > 0)
                {
                    DataGridView gridView = (DataGridView)sender;
                    gridView.ClearSelection();
                    gridView.Rows[e.RowIndex].Selected = true;
                    prevEventIndex = e.RowIndex;
                    GoToTime(((List<Event>)allEvents[currEventGroupIndex])[e.RowIndex].StartTimeSec);
                }
            }
            catch { }
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            this.Refresh();     // Combines Invalidate() and Update()
        }

        private void VideoMarkerImage_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {

        }

        private void eventsExportBTN_Click(object sender, EventArgs e)
        {
            DataGridView eventGridView = (DataGridView)eventTabs.SelectedTab.Controls[0];
            eventsExportFileDialog.FileName = Path.GetFileNameWithoutExtension(videoFileTB.Text) + ".txt";
            DialogResult returnVal = eventsExportFileDialog.ShowDialog();
            if (returnVal == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(eventsExportFileDialog.FileName))
                {
                    for (int i = 0; i < eventGridView.RowCount; ++i)
                    {
                        string line = "";
                        for (int j = 0; j < eventGridView.ColumnCount; ++j)
                        {
                            if (j > 0)
                                line += "\t";
                            line += eventGridView[j, i].Value;
                        }
                        sw.WriteLine(line);
                    }
                }

            }

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void elementHost1_ChildChanged(object sender, System.Windows.Forms.Integration.ChildChangedEventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void SettingsTab_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void Jump1_TextChanged(object sender, EventArgs e)
        {
            try
            { 
                Properties.Settings.Default.Jump1 = int.Parse(Jump1.Text);
                Properties.Settings.Default.Save();
            }
            catch
            {
                Jump1.Text = Properties.Settings.Default.Jump1.ToString();
            }
        }

        private void Jump2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default.Jump2 = int.Parse(Jump2.Text);
                Properties.Settings.Default.Save();
            }
            catch
            {
                Jump2.Text = Properties.Settings.Default.Jump1.ToString();
            }


        }

        private void Jump3_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default.Jump3 = int.Parse(Jump3.Text);
                Properties.Settings.Default.Save();
            }
            catch
            {
                Jump1.Text = Properties.Settings.Default.Jump3.ToString();
            }

        }

        private void Jump4_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default.Jump4 = int.Parse(Jump4.Text);
                Properties.Settings.Default.Save();
            }
            catch
            {
                Jump1.Text = Properties.Settings.Default.Jump4.ToString();
            }

        }

        private void Jump5_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Properties.Settings.Default.Jump5 = int.Parse(Jump5.Text);
                Properties.Settings.Default.Save();
            }
            catch
            {
                Jump1.Text = Properties.Settings.Default.Jump5.ToString();
            }

        }
    }


    public class Event
    {
        public string Desc;
        public double StartTimeSec;
        public double EndTimeSec;
        public int[] Members;
    }


    public class SubjectData
    {
        public SubjectData(int x_, int y_, int id_)
        {
            x = x_;
            y = y_;
            id = id_;
        }

        public int x, y;
        public int id;
    }


    public class TransparentControl : Control
    {
        private readonly System.Windows.Forms.Timer refresher;
        private Image _image;

        public TransparentControl()
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            BackColor = Color.Transparent;
            refresher = new System.Windows.Forms.Timer();
            refresher.Tick += TimerOnTick;
            refresher.Interval = 50;
            refresher.Enabled = true;
            refresher.Start();
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x20;
                return cp;
            }
        }

        protected override void OnMove(EventArgs e)
        {
            RecreateHandle();
        }


        protected override void OnPaint(PaintEventArgs e)
        {
            if (_image != null)
            {
                e.Graphics.DrawImage(_image, (Width / 2) - (_image.Width / 2), (Height / 2) - (_image.Height / 2));
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            //Do not paint background
        }

        //Hack
        public void Redraw()
        {
            RecreateHandle();
        }

        private void TimerOnTick(object source, EventArgs e)
        {
            RecreateHandle();
            refresher.Stop();
        }

        public Image Image
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
                RecreateHandle();
            }
        }
    }
    internal sealed class NativeMethodes
    {
        // Graphics.GetHdc() have several "bugs" detailed here : 
        // http://support.microsoft.com/default.aspx?scid=kb;en-us;311221
        // (see case 2) So we have to play with old school GDI...
        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateCompatibleDC(IntPtr hdc);

        [DllImport("gdi32.dll")]
        public static extern bool DeleteDC(IntPtr hdc);

        [DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);

        [DllImport("gdi32.dll")]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);

    }
}
