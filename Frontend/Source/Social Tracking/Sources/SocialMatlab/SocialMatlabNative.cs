/*
* MATLAB Compiler: 4.13 (R2010a)
* Date: Tue Dec 07 18:33:22 2010
* Arguments: "-B" "macro_default" "-W" "dotnet:SocialMatlab,SocialMatlab,0.0,private"
* "-T" "link:lib" "-d" "D:\Oren\Projects\SocialMice\SocialMatlab\src" "-w"
* "enable:specified_file_mismatch" "-w" "enable:repeated_file" "-w"
* "enable:switch_ignored" "-w" "enable:missing_lib_sentinel" "-w" "enable:demo_license"
* "-v" "class{SocialMatlab:D:\Oren\Projects\SocialMice\@SocialTracking\SocialTracking.m}" 
*/
using System;
using System.Reflection;
using System.IO;
using MathWorks.MATLAB.NET.Arrays;
using MathWorks.MATLAB.NET.Utility;
using MathWorks.MATLAB.NET.ComponentData;

#if SHARED
[assembly: System.Reflection.AssemblyKeyFile(@"")]
#endif

namespace SocialMatlabNative
{
  /// <summary>
  /// The SocialMatlab class provides a CLS compliant, Object (native) interface to the
  /// M-functions contained in the files:
  /// <newpara></newpara>
  /// D:\Oren\Projects\SocialMice\@SocialTracking\SocialTracking.m
  /// <newpara></newpara>
  /// deployprint.m
  /// <newpara></newpara>
  /// printdlg.m
  /// </summary>
  /// <remarks>
  /// @Version 0.0
  /// </remarks>
  public class SocialMatlab : IDisposable
  {
    #region Constructors

    /// <summary internal= "true">
    /// The static constructor instantiates and initializes the MATLAB Compiler Runtime
    /// instance.
    /// </summary>
    static SocialMatlab()
    {
      if (MWMCR.MCRAppInitialized)
      {
        Assembly assembly= Assembly.GetExecutingAssembly();

        string ctfFilePath= assembly.Location;

        int lastDelimiter= ctfFilePath.LastIndexOf(@"\");

        ctfFilePath= ctfFilePath.Remove(lastDelimiter, (ctfFilePath.Length - lastDelimiter));

        string ctfFileName = MCRComponentState.MCC_SocialMatlab_name_data + ".ctf";

        Stream embeddedCtfStream = null;

        String[] resourceStrings = assembly.GetManifestResourceNames();

        foreach (String name in resourceStrings)
        {
          if (name.Contains(ctfFileName))
          {
            embeddedCtfStream = assembly.GetManifestResourceStream(name);
            break;
          }
        }
        mcr= new MWMCR(MCRComponentState.MCC_SocialMatlab_name_data,
                       MCRComponentState.MCC_SocialMatlab_root_data,
                       MCRComponentState.MCC_SocialMatlab_public_data,
                       MCRComponentState.MCC_SocialMatlab_session_data,
                       MCRComponentState.MCC_SocialMatlab_matlabpath_data,
                       MCRComponentState.MCC_SocialMatlab_classpath_data,
                       MCRComponentState.MCC_SocialMatlab_libpath_data,
                       MCRComponentState.MCC_SocialMatlab_mcr_application_options,
                       MCRComponentState.MCC_SocialMatlab_mcr_runtime_options,
                       MCRComponentState.MCC_SocialMatlab_mcr_pref_dir,
                       MCRComponentState.MCC_SocialMatlab_set_warning_state,
                       ctfFilePath, embeddedCtfStream, true);
      }
      else
      {
        throw new ApplicationException("MWArray assembly could not be initialized");
      }
    }


    /// <summary>
    /// Constructs a new instance of the SocialMatlab class.
    /// </summary>
    public SocialMatlab()
    {
    }


    #endregion Constructors

    #region Finalize

    /// <summary internal= "true">
    /// Class destructor called by the CLR garbage collector.
    /// </summary>
    ~SocialMatlab()
    {
      Dispose(false);
    }


    /// <summary>
    /// Frees the native resources associated with this object
    /// </summary>
    public void Dispose()
    {
      Dispose(true);

      GC.SuppressFinalize(this);
    }


    /// <summary internal= "true">
    /// Internal dispose function
    /// </summary>
    protected virtual void Dispose(bool disposing)
    {
      if (!disposed)
      {
        disposed= true;

        if (disposing)
        {
          // Free managed resources;
        }

        // Free native resources
      }
    }


    #endregion Finalize

    #region Methods

    /// <summary>
    /// Provides a void output, 0-input Objectinterface to the SocialTracking M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// settings
    /// </remarks>
    ///
    public void SocialTracking()
    {
      mcr.EvaluateFunction(0, "SocialTracking", new Object[]{});
    }


    /// <summary>
    /// Provides a void output, 1-input Objectinterface to the SocialTracking M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// settings
    /// </remarks>
    /// <param name="varargin">Array of Objects representing the input arguments 1
    /// through varargin.length</param>
    ///
    public void SocialTracking(params Object[] varargin)
    {
      mcr.EvaluateFunction(0, "SocialTracking", varargin);
    }


    /// <summary>
    /// Provides the standard 0-input Object interface to the SocialTracking M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// settings
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] SocialTracking(int numArgsOut)
    {
      return mcr.EvaluateFunction(numArgsOut, "SocialTracking", new Object[]{});
    }


    /// <summary>
    /// Provides the standard 1-input Object interface to the SocialTracking M-function.
    /// </summary>
    /// <remarks>
    /// M-Documentation:
    /// settings
    /// </remarks>
    /// <param name="numArgsOut">The number of output arguments to return.</param>
    /// <param name="varargin">Array of Objects representing the input arguments 1
    /// through varargin.length</param>
    /// <returns>An Array of length "numArgsOut" containing the output
    /// arguments.</returns>
    ///
    public Object[] SocialTracking(int numArgsOut, params Object[] varargin)
    {
      return mcr.EvaluateFunction(numArgsOut, "SocialTracking", varargin);
    }


    /// <summary>
    /// This method will cause a MATLAB figure window to behave as a modal dialog box.
    /// The method will not return until all the figure windows associated with this
    /// component have been closed.
    /// </summary>
    /// <remarks>
    /// An application should only call this method when required to keep the
    /// MATLAB figure window from disappearing.  Other techniques, such as calling
    /// Console.ReadLine() from the application should be considered where
    /// possible.</remarks>
    ///
    public void WaitForFiguresToDie()
    {
      mcr.WaitForFiguresToDie();
    }



    #endregion Methods

    #region Class Members

    private static MWMCR mcr= null;

    private bool disposed= false;

    #endregion Class Members
  }
}
