﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Social_Tracking
{
    public partial class GoToTime : Form
    {
        private MainForm manager;

        public GoToTime(MainForm m)
        {
            InitializeComponent();

            manager = m;
            double origPosition = m.GetTimeInSeconds();
            int hours = (int)origPosition / 3600;
            int minutes = (int)origPosition / 60;
            int seconds = (int)origPosition % 60;
            hoursBox.Text = hours.ToString("D2");
            minsBox.Text = minutes.ToString("D2");
            secsBox.Text = seconds.ToString("D2");
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void goBtn_Click(object sender, EventArgs e)
        {
            manager.GoToTime(int.Parse(hoursBox.Text) * 3600 + int.Parse(minsBox.Text) * 60 + int.Parse(secsBox.Text));
        }

        private void hoursBox_TextChanged(object sender, EventArgs e)
        {
            if (hoursBox.Text.Length == 2)
                minsBox.Select();
        }

        private void minsBox_TextChanged(object sender, EventArgs e)
        {
            if (minsBox.Text.Length == 2)
                secsBox.Select();

        }

        private void secsBox_TextChanged(object sender, EventArgs e)
        {
            if (secsBox.Text.Length == 2)
                goBtn.Select();
        }

        private void hoursBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.V && (e.Modifiers & Keys.Control) != Keys.None)
            {
                TextBox tb = sender as TextBox;
                string textToPaste = Clipboard.GetText();
                string[] split1 = textToPaste.Split('.');
                if (split1.Length > 0)
                {
                    string[] split2 = textToPaste.Split(':');
                    if (split2.Length == 1)
                    {
                        tb.Text = split2[0];
                    }
                    else if (split2.Length == 2) 
                    {
                        minsBox.Text = split2[0];
                        secsBox.Text = split2[1];
                    }
                    else if (split2.Length == 3)
                    {
                        hoursBox.Text = split2[0];
                        minsBox.Text = split2[1];
                        secsBox.Text = split2[2];
                    }
                }
                //tb.Paste();
                e.Handled = true;
                e.SuppressKeyPress = true;
                //Console.WriteLine("Pasted: {0}", textToPaste);
            }
        }
    }
}
