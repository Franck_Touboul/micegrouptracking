﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DirectShowLib;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Threading;
//using QuartzTypeLib;

namespace CheeseOne
{
    /// <summary>
    /// Interaction logic for VideoPlayer.xaml
    /// </summary>
    public partial class VideoPlayer : UserControl, IMyVideoGrabber
    {
        private IFilterGraph2 graphBuilder = null;
        private IMediaControl mediaControl = null;
        private IBaseFilter vmr9 = null;
        private IVMRMixerBitmap9 mixerBitmap = null;
        private IVMRWindowlessControl9 windowlessCtrl = null;
        private ICaptureGraphBuilder2 captureGraph;
        private IBaseFilter sourceFilter;
        private IMediaPosition mediaPosition;
        private IMediaEventEx mediaEvt = null;
        private double FramePerSec = 25;
        private System.Windows.Forms.Panel VideoPanel;
        private String Filename = "";

        private IPin PreviewPin;
        private ISampleGrabber Grabber;
        private IBaseFilter GrabberFilter;
        private MyVideoGrabber BufferGrabber;
        public delegate void NewFrameHandler(object sender, NewFrameEventArgs e);
        public event NewFrameHandler OnNewFrame;
        public void HandleNewVideoFrame()
        {
            NewFrameEventArgs args = new NewFrameEventArgs(BufferGrabber);
            if (OnNewFrame != null)
                OnNewFrame(this, args);
        }

        private System.Threading.Timer OverlayTimer;
        private Boolean IsTimerEvent = false;
        private double PrevPosition = -100;

        const int WM_GRAPHNOTIFY = 0x00008001;

        private enum MediaStatus { Playing, Paused, None };
        private MediaStatus CurrentState = MediaStatus.None;

        public int NumberOfFrames
        {
            get {
                return (int)(Duration * FramePerSec);
            }
        }

        public double Duration
        {
            get
            {
                double duration;
                mediaPosition.get_Duration(out duration);
                return duration;
            }
        }

        public VideoPlayer()
        {
            InitializeComponent();
            VideoPanel = VideoHost.Child as System.Windows.Forms.Panel;

            MediaControlPanel.TimeChanged += new EventHandler(MediaControlPanel_ValueChanged);
            MediaControlPanel.MediaControlButtonPressed += new EventHandler(PlayButton);
            MediaControlPanel.KeyDownInControl += new System.Windows.Forms.KeyEventHandler(KeyDown);
            
            System.Threading.TimerCallback tcb = UpdateOverlay;
            System.Threading.AutoResetEvent autoEvent = new System.Threading.AutoResetEvent(false);
            OverlayTimer = new System.Threading.Timer(tcb, autoEvent, 0, System.Threading.Timeout.Infinite);

            AddHandler(Keyboard.KeyDownEvent, (KeyEventHandler)Grid_KeyDown);
        }

        private void UpdateOverlay(object source)
        {
            try
            {
                if (mediaPosition != null && windowlessCtrl is VideoMixingRenderer9)
                {
                    double position;
                    mediaPosition.get_CurrentPosition(out position);
                    if (Math.Abs(position - PrevPosition) > 0.2)
                    {
                        this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
                        {
                            UpdateOverlayDelegate();
                        }));
                    }
                }

            }
            finally
            {
                OverlayTimer.Change(40, System.Threading.Timeout.Infinite);
            }

            return;
        }


        private void MediaControlPanel_ValueChanged(object sender, EventArgs e)
        {
            if (mediaControl == null)
                return;
            if (!IsTimerEvent)
            {
                Pause();
                GoToTime((float)MediaControlPanel.Time / (float)FramePerSec);
                Play();
            }
        }

        public Bitmap GrabFrame()
        {
            SetMediaSampleGrabber();
            return BufferGrabber.GetFrame();
        }

        private double[] ByteToDouble(byte[] input)
        {
            var output = new double[input.Length];
            for (int i = 0; i < input.Length; ++i)
                output[i] = input[i];
            return output;
        }

        public BitmapSource GrabFrameToMatlab(MLApp.MLApp matlab, String name)
        {
            System.Drawing.Bitmap frame = GrabFrame();
            BitmapSource bitmapSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(frame.GetHbitmap(), IntPtr.Zero, System.Windows.Int32Rect.Empty, BitmapSizeOptions.FromWidthAndHeight(frame.Width, frame.Height));

            int nStride = (bitmapSource.PixelWidth * bitmapSource.Format.BitsPerPixel + 7) / 8;
            byte[] pixelByteArray = new byte[bitmapSource.PixelHeight * nStride];
            byte[] zeros = Enumerable.Repeat((byte)0x0, pixelByteArray.Length).ToArray();
            bitmapSource.CopyPixels(pixelByteArray, nStride, 0);

            CheeseOne.MainWindow.Matlab.PutFullMatrix(name, "base", ByteToDouble(pixelByteArray), ByteToDouble(zeros));
            CheeseOne.MainWindow.Matlab.Execute(name + "= permute(reshape(image, " + (bitmapSource.Format.BitsPerPixel / 8).ToString() + ", 704, 576), [3 2 1]); " + name + " = uint8(" + name + "(:, :, 1:3));");

            return bitmapSource;
        }

        public void SetBackgroundImage(ImageSource source)
        {
            Pause();
            BackgroundImage.Visibility = System.Windows.Visibility.Visible;
            BackgroundImage.Source = source;
        }

        public ImageSource BitmapToImageSource(System.Drawing.Bitmap bmp)
        {
            ImageSource source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(bmp.GetHbitmap(), IntPtr.Zero, System.Windows.Int32Rect.Empty, BitmapSizeOptions.FromWidthAndHeight(bmp.Width, bmp.Height));
            return source;
        }


        public void GoToTime(double seconds)
        {
            if (mediaPosition == null)
                return;
            BackgroundImage.Visibility = System.Windows.Visibility.Hidden;
            mediaPosition.put_CurrentPosition(seconds);

            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
            {
                UpdateOverlayDelegate();
            }));
        }

        public void Preload(String filename)
        {
            CloseInterfaces();
            Filename = filename;
        }

        public void Load(String filename)
        {
            CloseInterfaces();
            BuildGraph(filename);
            RunGraph();
        }

        public void Close()
        {
            CloseInterfaces();
        }

        private void CloseInterfaces()
        {
            if (mediaControl != null)
            {
                mediaControl.Stop();
                mediaEvt.SetNotifyWindow((IntPtr)0, WM_GRAPHNOTIFY, (IntPtr)0);
            }

            if (vmr9 != null)
            {
                //Marshal.ReleaseComObject(vmr9);
                vmr9 = null;
                windowlessCtrl = null;
                mixerBitmap = null;
            }

            if (graphBuilder != null)
            {
                //Marshal.ReleaseComObject(graphBuilder);
                graphBuilder = null;
            }
            mediaPosition = null;
            mediaEvt = null;
            mediaControl = null;
            windowlessCtrl = null;
            captureGraph = null;
            sourceFilter = null;

            Filename = "";
            CurrentState = MediaStatus.None;
            MediaControlPanel.IsPlaying = false;
            MediaControlPanel.TotalDuration = 0;
            MediaControlPanel.Time = 0;
        }

        private void BuildGraph(string fileName)
        {
            int hr = 0;

            try
            {
                graphBuilder = (IFilterGraph2)new FilterGraph();
                mediaControl = (IMediaControl)graphBuilder;
                mediaEvt = (IMediaEventEx)graphBuilder;
                mediaPosition = (IMediaPosition)graphBuilder;

                vmr9 = (IBaseFilter)new VideoMixingRenderer9();

                ConfigureVMR9InWindowlessMode();

                captureGraph = (ICaptureGraphBuilder2)new CaptureGraphBuilder2();
                captureGraph.SetFiltergraph(graphBuilder);

                hr = graphBuilder.AddSourceFilter(fileName, "source filter", out sourceFilter);
                DsError.ThrowExceptionForHR(hr);


                hr = graphBuilder.AddFilter(vmr9, "Video Mixing Renderer 9");
                DsError.ThrowExceptionForHR(hr);

                InitSampleGrabber();

                //hr = captureGraph.RenderStream(PinCategory.Preview, MediaType.Video, source, null, vmr9);
                hr = captureGraph.RenderStream(null, null, sourceFilter, GrabberFilter, vmr9);
                PreviewPin = DsFindPin.ByDirection(vmr9, PinDirection.Input, 0);
                //hr = graphBuilder.RenderFile(fileName, null);
                DsError.ThrowExceptionForHR(hr);
            }
            catch (Exception e)
            {
                CloseInterfaces();
                MessageBox.Show("An error occured during the graph building : \r\n\r\n" + e.Message, "Error");
            }
        }

        private void ConfigureVMR9InWindowlessMode()
        {
            int hr = 0;

            IVMRFilterConfig9 filterConfig = (IVMRFilterConfig9)vmr9;

            // Not really needed for VMR9 but don't forget calling it with VMR7
            hr = filterConfig.SetNumberOfStreams(1);
            DsError.ThrowExceptionForHR(hr);

            // Change VMR9 mode to Windowless
            hr = filterConfig.SetRenderingMode(VMR9Mode.Windowless);
            DsError.ThrowExceptionForHR(hr);

            windowlessCtrl = (IVMRWindowlessControl9)vmr9;

            // Set "Parent" window
            hr = windowlessCtrl.SetVideoClippingWindow(VideoPanel.Handle);
            DsError.ThrowExceptionForHR(hr);

            // Set Aspect-Ratio
            hr = windowlessCtrl.SetAspectRatioMode(VMR9AspectRatioMode.LetterBox);
            DsError.ThrowExceptionForHR(hr);

            hr = windowlessCtrl.SetVideoPosition(null, DsRect.FromRectangle(VideoPanel.ClientRectangle));
            // Call the resize handler to configure the output size
        }

        private bool InitSampleGrabber()
        {
            int hr = 0;

            // Get SampleGrabber
            Grabber = new SampleGrabber() as ISampleGrabber;

            if (Grabber == null)
                return false;

            GrabberFilter = Grabber as IBaseFilter;
            if (GrabberFilter == null)
            {
                Marshal.ReleaseComObject(Grabber);
                Grabber = null;
            }
            AMMediaType media = new AMMediaType();

            media.majorType = MediaType.Video;
            media.subType = MediaSubType.RGB24;
            media.formatPtr = IntPtr.Zero;
            hr = Grabber.SetMediaType(media);
            if (hr < 0)
                Marshal.ThrowExceptionForHR(hr);
            hr = graphBuilder.AddFilter(GrabberFilter, "SampleGrabber");

            if (hr < 0)
                Marshal.ThrowExceptionForHR(hr);
            hr = Grabber.SetBufferSamples(true);
            if (hr == 0)
                hr = Grabber.SetOneShot(false);
            if (hr == 0)
                hr = Grabber.SetCallback(null, 0);
            if (hr < 0)
                Marshal.ThrowExceptionForHR(hr);
            return true;
        }

        private void SetMediaSampleGrabber()
        {
            if (GrabberFilter != null)
            {
                AMMediaType media = new AMMediaType();
                int hr;

                hr = Grabber.GetConnectedMediaType(media);
                if (hr < 0)
                {
                    Marshal.ThrowExceptionForHR(hr);
                }

                if ((media.formatType != FormatType.VideoInfo) || (media.formatPtr ==
                    IntPtr.Zero))
                {
                    throw new NotSupportedException(
                        "Unknown Grabber Media Format");
                }

                Marshal.FreeCoTaskMem(media.formatPtr);
                media.formatPtr = IntPtr.Zero;

                BufferGrabber = new MyVideoGrabber(this, GetInfoHeader());
                Grabber.SetCallback(BufferGrabber, 1);
            }
        }

        private VideoInfoHeader GetInfoHeader()
        {
            /*IPin outputPin = PreviewPin;
            IAMStreamConfig StreamConfig = outputPin as IAMStreamConfig;
            AMMediaType mediaType;
            StreamConfig.GetFormat(out mediaType);*/

            /*object StreamConfigObj;
            IAMStreamConfig StreamConfig;
            captureGraph.FindInterface(PinCategory.Capture, MediaType.Video, sourceFilter, typeof(IAMStreamConfig).GUID, out StreamConfigObj);
            StreamConfig = StreamConfigObj as IAMStreamConfig;
            AMMediaType mediaType;
            StreamConfig.GetFormat(out mediaType);*/
            VideoInfoHeader infoHeader = null;

            AMMediaType media = new AMMediaType();
            int hr = Grabber.GetConnectedMediaType(media);
            DsError.ThrowExceptionForHR(hr);

            if (media.formatType == DirectShowLib.FormatType.VideoInfo || media.formatType == DirectShowLib.FormatType.MpegVideo)
            {
                infoHeader = (VideoInfoHeader)Marshal.PtrToStructure(media.formatPtr, typeof(VideoInfoHeader));
            }
            DsUtils.FreeAMMediaType(media);
            return infoHeader;
        }


        private void UpdateOverlayDelegate()
        {
            if (mediaPosition == null)
                return;
            try
            {
                double origPosition;
                mediaPosition.get_CurrentPosition(out origPosition);

                IsTimerEvent = true;
                MediaControlPanel.Time = origPosition * FramePerSec;
                IsTimerEvent = false;
            }
            catch
            {
            }
        }

        private void RunGraph()
        {
            if (mediaControl != null)
            {
                OABool bCsf, bCsb;
                mediaPosition.CanSeekBackward(out bCsb);
                mediaPosition.CanSeekForward(out bCsf);

                Boolean isSeeking = (bCsb == OABool.True) && (bCsf == OABool.True);

                double duration;
                mediaPosition.get_Duration(out duration);

                MediaControlPanel.TotalDuration = duration * FramePerSec;
                MediaControlPanel.Time = 0;

                Play();
            }
        }

        private void Play()
        {
            if (graphBuilder == null && Filename != "")
            {
                Load(Filename);
                return;
            }
            if (mediaControl == null)
                return;
            BackgroundImage.Visibility = System.Windows.Visibility.Hidden;
            mediaControl.Run();
            CurrentState = MediaStatus.Playing;
            MediaControlPanel.IsPlaying = true;
        }


        private void Pause()
        {
            if (mediaControl == null)
                return;
            mediaControl.Pause();
            CurrentState = MediaStatus.Paused;
            MediaControlPanel.IsPlaying = false;
        }

        private void PlayButton(object sender, EventArgs e)
        {
            if (CurrentState == MediaStatus.Playing)
                Pause();
            else
                Play();
        }

        private void VideoHost_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (windowlessCtrl == null)
                return;
            int hr;
            hr = windowlessCtrl.SetVideoPosition(null, DsRect.FromRectangle(VideoPanel.ClientRectangle));
        }

        new private void KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (FocusManager.GetFocusedElement(this) is TextBox)
                return;
            e.Handled = true;

            // Gets the key code 
            //statusBox.Text = "KeyCode: " + e.Key.ToString();   
            /*if (e.Key == Keys.Up)
            {
                GoToTime(GetTimeInSeconds() + 300);
                statusBox.Text = "KeyCode: " + e.Key.ToString();
                // perform copy
                //e.Handled = true;
            }
            else
                if (e.Key == Keys.Down)
                {
                    GoToTime(GetTimeInSeconds() - 300);
                    statusBox.Text = "KeyCode: " + e.Key.ToString();
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Keys.Left && Control.ModifierKeys == Keys.Control)
                {
                    GoToTime(GetTimeInSeconds() - 5);
                    statusBox.Text = "KeyCode: " + e.Key.ToString();
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Keys.Right && Control.ModifierKeys == Keys.Control)
                {
                    GoToTime(GetTimeInSeconds() + 5);
                    statusBox.Text = "KeyCode: " + e.Key.ToString();
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Keys.Up && Control.ModifierKeys == Keys.Control)
                {
                    GoToTime(GetTimeInSeconds() + 30);
                    statusBox.Text = "KeyCode: " + e.Key.ToString();
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Keys.Down && Control.ModifierKeys == Keys.Control)
                {
                    GoToTime(GetTimeInSeconds() - 30);
                    statusBox.Text = "KeyCode: " + e.Key.ToString();
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Keys.Left && Control.ModifierKeys == Keys.Control && Control.ModifierKeys == Keys.Shift)
                {
                    GoToTime(GetTimeInSeconds() - 300);
                    statusBox.Text = "KeyCode: " + e.Key.ToString();
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Keys.Right && Control.ModifierKeys == Keys.Control && Control.ModifierKeys == Keys.Shift)
                {
                    GoToTime(GetTimeInSeconds() + 300);
                    statusBox.Text = "KeyCode: " + e.Key.ToString();
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Keys.Down && Control.ModifierKeys == Keys.Control && Control.ModifierKeys == Keys.Shift)
                {
                    GoToTime(GetTimeInSeconds() - 1800);
                    statusBox.Text = "KeyCode: " + e.Key.ToString();
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Keys.Up && Control.ModifierKeys == Keys.Control && Control.ModifierKeys == Keys.Shift)
                {
                    GoToTime(GetTimeInSeconds() + 1800);
                    statusBox.Text = "KeyCode: " + e.Key.ToString();
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Keys.Space)
                {
                    playBtn_Click(null, null);
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Keys.Right)
                {
                    GoToTime(GetTimeInSeconds() + 1.0f / m_framePerSec);
                }
                else if (e.Key == Keys.Left)
                {
                    GoToTime(GetTimeInSeconds() - 1.0f / m_framePerSec);
                }
                else if (e.Key == Keys.PageDown)
                {
                    DataGridView currGrid = (DataGridView)eventTabs.SelectedTab.Controls[0];
                    int prevRow = currGrid.CurrentCell.RowIndex;
                    if (prevRow + 1 < currGrid.Rows.Count)
                    {
                        //                  currGrid.ClearSelection();
                        //                 currGrid.Rows[prevRow + 1].Selected = true;
                        //                    currGrid.c = prevRow + 1;
                        currGrid.CurrentCell = currGrid.Rows[prevRow + 1].Cells[0];
                    }
                }
                else if (e.Key == Keys.PageUp)
                {
                    DataGridView currGrid = (DataGridView)eventTabs.SelectedTab.Controls[0];
                    int prevRow = currGrid.CurrentCell.RowIndex;
                    if (prevRow - 1 > 0)
                    {
                        //                    currGrid.ClearSelection();
                        //                    currGrid.Rows[prevRow - 1].Selected = true;
                        currGrid.CurrentCell = currGrid.Rows[prevRow - 1].Cells[0];


                    }
                }

            statusBox.Text = e.Handled.ToString();*/
        }

        public double GetTimeInSeconds()
        {
            if (mediaPosition == null)
                return 0;
            double origPosition;
            mediaPosition.get_CurrentPosition(out origPosition);
            return origPosition;
        }

        private void Grid_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;

            // Gets the key code 
            if (e.Key == Key.Up)
            {
                GoToTime(GetTimeInSeconds() + 300);
                // perform copy
                //e.Handled = true;
            }
            else
                if (e.Key == Key.Down)
                {
                    GoToTime(GetTimeInSeconds() - 300);
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Key.Left && Keyboard.Modifiers == ModifierKeys.Control)
                {
                    GoToTime(GetTimeInSeconds() - 5);
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Key.Right && Keyboard.Modifiers == ModifierKeys.Control)
                {
                    GoToTime(GetTimeInSeconds() + 5);
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Key.Up && Keyboard.Modifiers == ModifierKeys.Control)
                {
                    GoToTime(GetTimeInSeconds() + 30);
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Key.Down && Keyboard.Modifiers == ModifierKeys.Control)
                {
                    GoToTime(GetTimeInSeconds() - 30);
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Key.Left && Keyboard.Modifiers == ModifierKeys.Control && Keyboard.Modifiers == ModifierKeys.Shift)
                {
                    GoToTime(GetTimeInSeconds() - 300);
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Key.Right && Keyboard.Modifiers == ModifierKeys.Control && Keyboard.Modifiers == ModifierKeys.Shift)
                {
                    GoToTime(GetTimeInSeconds() + 300);
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Key.Down && Keyboard.Modifiers == ModifierKeys.Control && Keyboard.Modifiers == ModifierKeys.Shift)
                {
                    GoToTime(GetTimeInSeconds() - 1800);
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Key.Up && Keyboard.Modifiers == ModifierKeys.Control && Keyboard.Modifiers == ModifierKeys.Shift)
                {
                    GoToTime(GetTimeInSeconds() + 1800);
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Key.Space)
                {
                    PlayButton(null, null);
                    // perform copy
                    //e.Handled = true;
                }
                else if (e.Key == Key.Right)
                {
                    GoToTime(GetTimeInSeconds() + 1.0f / FramePerSec);
                }
                else if (e.Key == Key.Left)
                {
                    GoToTime(GetTimeInSeconds() - 1.0f / FramePerSec);
                }

        }

    }

    public class NewFrameEventArgs : EventArgs
    {
        public MyVideoGrabber Grabber { get; private set; }

        public NewFrameEventArgs(MyVideoGrabber grabber)
        {
            Grabber = grabber;
        }
    }

    public interface IMyVideoGrabber
    {
        void HandleNewVideoFrame();
    }

    public class MyVideoGrabber : ISampleGrabberCB
    {
        VideoInfoHeader InfoHeader;
        private byte[] FrameBuffer;
        static private Mutex mut = new Mutex();
        private bool Waiting = false;

        public Bitmap image = null;
        private IMyVideoGrabber Handle = null;

        public MyVideoGrabber(IMyVideoGrabber handle, VideoInfoHeader infoHeader)
        {
            InfoHeader = infoHeader;
            Handle = handle;
        }


        int ISampleGrabberCB.SampleCB(double SampleTime, IMediaSample pSample)
        {
            return 0;
        }

        Semaphore pool = new Semaphore(0, 1);

        public Bitmap GetFrame()
        {
            Waiting = true;
            pool.WaitOne();
            return image;
        }

        int ISampleGrabberCB.BufferCB(double SampleTime, IntPtr pBuffer, int BufferLen)
        {
            mut.WaitOne();
            try
            {
                if (!Waiting)
                    return 0;
                /*mut.WaitOne();
                if (!Waiting)
                    return 0;*/
                if (FrameBuffer == null || BufferLen > FrameBuffer.Length)
                    FrameBuffer = new byte[BufferLen];
                Marshal.Copy(pBuffer, FrameBuffer, 0, BufferLen);
                GCHandle handle = GCHandle.Alloc(FrameBuffer, GCHandleType.Pinned);
                int scan0 = (int)handle.AddrOfPinnedObject();
                int stride = (InfoHeader.BmiHeader.BitCount / 8) * InfoHeader.BmiHeader.Width;
                //scan0 += (InfoHeader.BmiHeader.Height - 1) * stride;
                image = new Bitmap(InfoHeader.BmiHeader.Width, InfoHeader.BmiHeader.Height > 0 ? InfoHeader.BmiHeader.Height : -InfoHeader.BmiHeader.Height, stride, System.Drawing.Imaging.PixelFormat.Format24bppRgb, (IntPtr)scan0);
                image.RotateFlip(RotateFlipType.RotateNoneFlipY);
                handle.Free();
                //VideoControl.SetFrame(bmp);
                Waiting = false;
                pool.Release();
                //NewFrameEventArgs args = new NewFrameEventArgs(this);
                Handle.HandleNewVideoFrame();
            }
            finally
            {
                mut.ReleaseMutex();
            }
            return 0;
        }
    }
}
