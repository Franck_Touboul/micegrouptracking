function sz = GetFileSize(filename)
if ~isunix
    basename = regexprep(filename, '.*[/\\]', '');
    [status, result] = system(['dir /T:C ' filename]);
    if status ~= 0
        sz = [];
        return;
    end
    [begin, finish] = regexp(result, ['[^\n]*' basename '\n']);
    if isempty(begin)
        sz = [];
        return;
    end
    line = result(begin:finish);
    line = regexprep(line, [ '[\t ]*' basename '\n'], '');
    fields = regexp(line,'[\t ]+','split');
    sz = str2double(fields{end});
else
    [status, result] = system(['ls -l ' filename]);
    if status ~= 0
        sz = [];
        return;
    end
    fields = regexp(result,'[\t ]+','split');
    sz = str2double(fields{5});
end