function me = TrainPottsModel(obj, orders, zones, inParallel, Regularize)

if nargin < 4
    inParallel = false;
end

if nargin < 5
    Regularize = false;
end

Options.nIters = 1;

if exist('zones', 'var') && ~isempty(zones)
    data = zones - 1;
else
    data = zeros(obj.nSubjects, sum(obj.valid));
    for s=1:obj.nSubjects
        currZones = obj.zones(s, :);
        data(s, :) = currZones(obj.valid == 1) - 1;
    end
end

me = cell(1, max(orders));
fprintf('# - training Potts model:\n');
h = cell(1, length(orders));
hidx = 1;
for level=orders
    fprintf('#      . level %d\n', level);
    local = obj;
    local.initialPottsWeights = [];
    local.n = level;
    %local.output = true;
    local.OutputToFile = false;
    local.count = obj.ROI.nZones;
    Options.nAlgIterms = obj.Analysis.Potts.nIters;
    Options.MinNumberOfIters = obj.Analysis.Potts.MinNumberOfIters;
    Options.Confidence = obj.Analysis.Potts.Confidence;
    Options.Regularize = Regularize;
    local.data = double(data);
    local.Options = Options;
    if inParallel
        h{hidx} = Job(SocialPackageObjForParallelPotts(local));
        h{hidx}.Run(@TrainPottsModelAux);
        hidx = hidx + 1;
    else
        temp = TrainPottsModelAux(local);
        me{level} = temp.Model;
    end
end
if inParallel
    hidx = 1;
    for level=orders
        h{hidx}.WaitUntilFinished;
        curr = h{hidx}.Object;
        me{level} = curr.Model;
        hidx = hidx + 1;
    end
end

