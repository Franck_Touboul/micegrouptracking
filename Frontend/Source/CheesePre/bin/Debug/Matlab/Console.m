classdef Console
    
    %% text formattings
    methods (Static) 
        function Color(varargin)
            cprintf(varargin{:});
        end
        
        function Orange(varargin)
            Console.StrFix('[\b', ']\b', varargin{:});
        end
        
        function Red(varargin)
            fprintf(2, varargin{:});
        end
        
        function Bold(varargin)
            Console.StrFix('<strong>', '</strong>', varargin{:});
        end
        
        function Green(varargin)
            cprintf('green', varargin{:});
        end
        
        function Link(varargin)
            Console.StrFix('<a href="">', '</a>', varargin{:});
        end

        function MatlabLink(filename, linenumber, varargin)
            Console.StrFix('<a href="matlab: opentoline(%s, %d)">', '</a>', filename, linenumber, varargin{:});
        end
        
        function StrFix(pre, suf, varargin)
            fprintf([pre varargin{1} suf], varargin{2:end});
        end
    end
    
    %% styles
    methods (Static)
        function xml = StructToXML(s)
            %%
            %xml = sprintf('<?xml version="1.0" encoding="UTF-8"?>\n');
            xml = '';
            xml = sprintf(['%s' '<struct>\n'], xml);
            f = fields(s);
            for i=1:length(s)
                xml = sprintf(['%s' ' <entry>\n'], xml);
                for j=1:length(f)
                    xml = sprintf(['%s' '  <%s>%s</%s>\n'], xml, f{j}, num2str(s(i).(f{j})), f{j});
                end
                xml = sprintf(['%s' ' </entry>\n'], xml);
            end
            xml = sprintf(['%s' '</struct>\n'], xml);
        end

        function str = Format(v)
            if ischar(v)
                v = regexprep(v, '\n', '\\n');
                v = regexprep(v, '\r', '\\r');
                v = regexprep(v, '\b', '\\b');
                str = ['''' v ''''];
                return;
            end
            if isscalar(v)
                str = sprintf('%g', v);
            elseif isvector(v)
                str = ['[' sprintf('%g, ', v(1:end-1)) sprintf('%g', v(end)) ']'];
            elseif ismatrix(v)
                sz = size(v); 
                str = '[';
                for i=1:size(v, 1)
                    if i<size(v, 1)
                        suff = ', ';
                    else
                        suff = '';
                    end
                    if ndims(v) > 2 %#ok<ISMAT>
                        str = [str, Console.Format(reshape(v(i, :), sz(2:end))), suff]; %#ok<AGROW>
                    else
                        str = [str, Console.Format(v(i, :)), suff]; %#ok<AGROW>
                    end
                end
                str = [str, ']'];
            end
        end    
        
        function str = StrudelError(err)
            Console.Red('#! %s\n', err.message);
            str = err.message;
            for i=1:length(err.stack)
                Console.Red('#! - <a href="matlab: opentoline(%s, %d)">%s (line %d)</a>\n', err.stack(i).file, err.stack(i).line, err.stack(i).name, err.stack(i).line);
                str = [str, sprintf('<a href="matlab: opentoline(%s, %d)">%s (line %d)</a>', err.stack(i).file, err.stack(i).line, err.stack(i).name, err.stack(i).line)];
            end
            str = Console.Strudel(struct('Error', str));
        end
        
        function str = Strudel(data, prefix)
            if nargin < 2
                prefix = '/';
            end
            if isstruct(data) || isobject(data)
                f = fields(data);
                str = '';
                if length(data) > 1
                    for i=1:length(data)
                        str = sprintf('%s%s', str, Console.Strudel(data(i), [prefix f{i} sprintf('(%d)', i)]));
                    end
                elseif isempty(data)
                else
                    for i=1:length(f)
                        if isstruct(data.(f{i}))
                            str = sprintf('%s%s', str, Console.Strudel(data.(f{i}), [prefix f{i} '/']));
                        else
                            curr = data.(f{i});
                            %%
                            sz = size(curr);
                            szstr = [num2str(sz(1)) sprintf(',%d', sz(2:end))];
                            str = sprintf('%s@%s[%s(%s)] =', str, [prefix f{i}], class(curr), szstr);
                            if iscell(curr)
                                str = sprintf('%s%s', str, Console.Strudel(curr, [prefix f{i}]));
                            else
                                str = sprintf('%s %s', str, Console.Format(curr));
                            end
                            str = sprintf('%s\n', str);
                        end
                    end
                end
            elseif iscell(data)
                str = '';
                coord = cell(1, ndims(data));
                for i=1:numel(data)
                    [coord{:}] = ind2sub(size(data), i);
                    curr = data{i};
                    sz = size(curr);
                    szstr = [num2str(sz(1)) sprintf(',%d', sz(2:end))];
                    for j=1:length(coord)
                        if j>1
                            coordstr = sprintf('%s, %d', coordstr, coord{j});
                        else
                            coordstr = sprintf('%d', coord{j});
                        end
                    end
                    str = sprintf('%s@%s/{%s}[%s(%s)] =', str, prefix, coordstr, class(curr), szstr);
                end
            else
                
            end
        end

        function CustomMessage(bullet, varargin)
            styles = {@(x) Console.Bold(x), @(x) fprintf(x)};
            
            level = max(length(dbstack)-1, 1);
            if isnumeric(varargin{1})
                level = level + varargin{1};
                varargin = varargin(2:end);
            end
            s = sprintf(varargin{:});
            fprintf('# ');
            if level > 1
                fprintf([repmat(' ', 1, (level-2)*2) bullet ' ']);
            end
            styles{min(level, length(styles))}(s);
            Console.NewLine;
        end
        
        function Warning(varargin)
            %%
            if isa(varargin{1}, 'MException')
                %%
                Console.Red('#! %s\n', err.message);
                for i=1:length(err.stack)
                    Console.Red('#! - <a href="matlab: opentoline(%s, %d)">%s (line %d)</a>\n', err.stack(i).file, err.stack(i).line, err.stack(i).name, err.stack(i).line);
                end
            else
                Console.Red('#! %s\n', sprintf(varargin{:}));
            end
        end
        
        function Message(varargin)
            styles = {@(x) Console.Bold(x), @(x) fprintf(x)};
            
            level = max(length(dbstack)-1, 1);
            if isnumeric(varargin{1})
                level = level + varargin{1};
                varargin = varargin(2:end);
            end
            s = sprintf(varargin{:});
            fprintf('# ');
            if level > 1
                fprintf([repmat(' ', 1, (level-2)*2) '- ']);
            end
            styles{min(level, length(styles))}(s);
            Console.NewLine;
        end
        
        function Header(varargin)
            s = sprintf(varargin{:});
            fprintf(['#  . ' s '\n']);
        end
        
        function Title(varargin)
            s = sprintf(varargin{:});
            fprintf(['# ' s '\n']);
        end

        function Subtitle(varargin)
            s = sprintf(varargin{:});
            fprintf(['# - ' s '\n']);
        end

        function SubSubtitle(varargin)
            s = sprintf(varargin{:});
            fprintf(['# - ' s '\n']);
        end
        
    end
    
    %% reports
    methods (Static)
        function Start(varargin)
            s = sprintf(varargin{:});
            fprintf(['#  . ' s ' ... ']);
        end
        
        function Done
            fprintf('[done]\n');
        end

        function Failed
            fprintf('[fail]\n');
        end

        function ProgRep(title, idx, count)
            persistent startTime;
            if idx <= 1 || isempty(startTime)
                Console.Reprintf(0, '');
                startTime = tic;
            end
            Console.Reprintf('# - %s: %d/%d (%.1f%%) [%.1f/%.1f] [x%.1fRT]', title, idx, count, idx/count*100, toc(startTime), toc(startTime) * count / idx, idx/toc(startTime));
        end
        
        function Report(title, idx, count)
            persistent startTime;
            if idx <= 1 || isempty(startTime)
                Console.Reprintf(0, '');
                startTime = tic;
            end
            Console.Reprintf('# - %s: %d/%d (%.1f%%) [%.1f/%.1f]', title, idx, count, idx/count*100, toc(startTime), toc(startTime) * count / idx);
        end
    end
    
    %% aux
    methods (Static)
        function Deprintf(varargin)
            s = sprintf(varargin{:});
            for i=1:length(s)
                s = sprintf('%s%s', s, '\b');
            end
            fprintf(s);
        end
        
        function n = Reprintf(varargin)
            persistent curr;
            if isempty(curr)
                curr = 0;
            end
            if isnumeric(varargin{1})
                prev = varargin{1};
                in = varargin(2:end);
            else
                if curr > 0
                    prev = curr;
                else
                    prev = 0;
                end
                in = varargin;
            end
            for i=1:prev
                fprintf('\b');
            end
            s = sprintf(in{:});
            n = length(s);
            curr = n;
            s = regexprep(s, '%', '%%');
            fprintf(s);
        end
        
        function NewLine
            Console.Reprintf(0, '');
            fprintf('\n');
        end
        
        function pos = MarkPosition()
            mde = com.mathworks.mde.desk.MLDesktop.getInstance;
            cw = mde.getClient('Command Window');
            xCmdWndView = cw.getComponent(0).getViewport.getComponent(0);
            pos = xCmdWndView.getCaretPosition;
        end
        
        function GoTo(pos)
            curr = Console.MarkPosition;
            for i=1:curr-pos
                fprintf('\b');
            end
        end
    end
end