function s = sec2time_v2(sec)
sep = ':';

s = sprintf('%02d%s%02d%s%02d.%02d', floor(sec / 3600), sep, mod(floor(sec / 60), 60), sep, mod(floor(sec), 60), round((sec - floor(sec)) * 100));
