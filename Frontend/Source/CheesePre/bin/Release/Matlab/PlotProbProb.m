function PlotProbProb(varargin)
PlotLogProbProb(varargin{:})
return;
% (y1, y2, count, isEquallAxis)
% if isstruct(varargin(1))
%     obj = varargin{1};
%     y1 = varargin{2};
%     y2 = varargin{3};
%     count = varargin{4};
%     isEquallAxis = varargin{5};
% 
% else
    obj = [];
    y1 = varargin{1};
    y2 = varargin{2};
    count = varargin{3};
    if nargin < 4
        isEquallAxis = true;
    else
        isEquallAxis = varargin{4};
    end
    if nargin >= 5
        valid = varargin{5};
    else
        valid = true(size(y1));
    end
% end

if ~exist('isEquallAxis')
    isEquallAxis = true;
end

logY1 = log10(y1);
logY2 = log10(y2);
avail = isfinite(logY1) & isfinite(logY2);
logY1 = logY1(avail);
logY2 = logY2(avail);
valid = valid(avail);
plot(logY1, logY2, '.', 'Color', [.6 .6 .6]);
a = axis;
if isEquallAxis
%     minval = max(a(1), a(3));
%     maxval = max(a(2), a(4));
%     axis([minval maxval minval maxval]);
    axis([log10(1/count) -1 log10(1/count)  -1]);
   % axis([min([logY1, logY2]), -1,  min([logY1, logY2]), -1]);
%    axis([floor(log10(1/count)) -1 floor(log10(1/count))  -1]);
    a = axis;
end
plot([a(1) a(2)], [a(1) a(2)], 'k-', 'LineWidth', 2, 'Color', [.7 .7 .7]);
hold on;
if exist('count', 'var') && ~isempty(count) && count > 0
    l = sequence(a(1), a(2), 100);
    r = 10.^l * count;
    [www, p] = binofit(r(r <= count), count);
    u1 = log10(p(:, 1));
    u2 = log10(p(:, 2));
    u1(~isfinite(u1) | u1 < a(3)-1) = a(3)-1;
    fill([l, flipdim(l, 2)], [u1', flipdim(u2', 2)], [.8 .8 .8], 'LineStyle', 'none');
end
%plot(logY1, logY2, '.', 'Color', [0 180 255]/255);
plot(logY1(~valid), logY2(~valid), '.', 'Color', [79 129 189]/255);
plot(logY1(valid), logY2(valid), '.', 'Color', [188 80 80 ]/255);

if isEquallAxis
    axis square
end
%%
% xtick = get(gca, 'XTick');
% ytick = get(gca, 'YTick');
% xticklabel = {};
% yticklabel = {};
% for i=1:length(xtick); xticklabel{i} = ['$10^{' num2str(xtick(i)) '}$']; end
% for i=1:length(ytick); yticklabel{i} = ['$10^{' num2str(ytick(i)) '}$']; end
% format_ticks(gca, xticklabel, yticklabel);
axis(a);
hold off;

%%
title(['D_{JS} = ' num2str(JensenShannonDivergence(y1(:)', y2(:)')) ', D_{KL} = ' num2str(KullbackLeiblerDivergence(y1(:)', y2(:)'))]);
