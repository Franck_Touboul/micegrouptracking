classdef CheeseSquare < Serializable
    properties
        Prefix = [];
        Tracking = [];
        Video = [];
        Hierarchy = struct();
        Background = struct();
        nSubjects = 0;
        Colors = struct();
        Meta = struct();
        Analysis = struct();
        ROI = [];
    end
    
    properties (Dependent)
        DateTime;
        Profile;
    end
    
    properties (Hidden)
        TrackingFields = {'nSubjects', 'Hierarchy', 'ROI', 'Colors', 'Profile'};
        Filename = MyFilename('');
        Profile_ = [];
    end
    
    methods (Static = true)
        function z = ZoneColormap
            z = {...
                'Open',         [220, 73, 89] / 255,...
                'Feeder1',      [53, 178, 87] / 255,...
                'Feeder2',      [51, 102, 51] / 255,...
                'Water',        [179, 213, 157] / 255,...
                'SmallNest',    [204, 102, 51] / 255,...
                '(SmallNest)',  [200, 200, 200] / 255,...
                'Labyrinth',    [168, 39, 76] / 255,...
                'BigNest',      [255, 153, 51] / 255,...
                '(BigNest)',    [230, 230, 230] / 255,...
                'Block',        [255, 204, 153] / 255,...
                '[Ramp1]',      [0, 177, 229] / 255,...
                '[Ramp2]',      [0, 177, 229] / 255 ...
                };
        end
        
        function z = MiceColormap
            z = {...
                'Purple', [202   152   227]/255, ...
                'Red', [196    77    88]/255, ...
                'Orange', [253   189    51]/255, ...
                'Yellow', [253   189    51]/255, ...
                'Green', [179   224   110]/255, ...
                'Blue', [0   177   229]/255 ...
                'White', [236 229 206]/255 ...
                };
        end
        
        function c = MiceColors(colors)
            % colors - for example: 'PRBY'
            z = CheeseSquare.MiceColormap;
            c = zeros(length(colors), 3);
            for i=1:length(colors)
                idx = find(cellfun(@(x) ~isempty(x), regexp(z(1:2:end), colors(i))), 1);
                c(i, :) = z{2 * (idx-1) + 2};
            end
        end
        
        function s = GeneratePrefix(GroupType, GroupId, DayId, CameraId)
            if isempty(DayId)
                s = sprintf('%s.exp%04d.day%%02d.cam%02d', GroupType, GroupId, CameraId);
                return;
            end
            s = sprintf('%s.exp%04d.day%02d.cam%02d', GroupType, GroupId, DayId, CameraId);
        end
        
        function ShowEthogram(obj)
            %%
            zmap = CheeseSquare.ZoneColormap;
            map = containers.Map(zmap(1:2:end), zmap(2:2:end));
            zones = obj.Tracking.zones;
            label = ones(size(zones ));
            cmap = zeros(obj.ROI.nZones + 1, 3);
            idx = 2;
            for z=1:obj.ROI.nZones
                %%
                if ~map.isKey(obj.ROI.ZoneNames{z})
                    warning('no color defined for ''%s''', obj.ROI.ZoneNames{z});
                else
                    label(zones == z) = idx;
                    cmap(idx, :) = map(obj.ROI.ZoneNames{z});
                    idx = idx + 1;
                end
            end
            %%
            subplot(1,10,2:10);
            imagesc(ind2rgb(label, cmap));
            axis off
            Fig.Fix
            colormap(cmap);
            h = colorbar;
            nc = size(cmap, 1);
            set(h, 'Ticks', linspace(0+.5/nc, 1-.5/nc, nc));
            set(h, 'TickLabels', {'Unknown' obj.ROI.ZoneNames{:}});
            subplot(1,10,1);
            m = obj.Colors.Marks;
            for i=1:obj.nSubjects
                plot(0, i, 'o', 'MarkerFaceColor', mean(m.color(m.id == i, :)/255), 'MarkerEdgeColor', 'none', 'MarkerSize', 20);
                Fig.Hon;
            end
            Fig.Hoff;
            Fig.YAxis(0.5, obj.nSubjects+.5);
            set(gca, 'YDir', 'reverse')
            axis off;
        end
    end
    
    methods
        function value = get.Profile(obj)
            if isempty(obj.Profile_) || isempty(fieldnames(obj.Profile_))
                try
                    obj = CheeseNewProfiler(obj);
                catch
                end
            end
            value = obj.Profile_;
        end
        
        function obj = set.Profile(obj, value)
            obj.Profile_ = value;
        end
        
        function value = get.DateTime(obj)
            value = obj.Meta.CreationDate + obj.Video.Time;
        end
        
        function obj = set.DateTime(obj, value)
            if ~ischar(obj.Video)
                obj.Video.Time = (value - obj.Meta.CreationDate) * 24 * 60 * 60;
            end
        end
        
        
        function obj = CheeseSquare(varargin)
            obj = obj.Load(varargin{:});
        end
        
        function obj = Load(obj, varargin)
            obj.Profile_ = [];
            if nargin >= 1
                arg = varargin{1};
                if isa(arg, 'CheeseSquare')
                    obj = arg;
                elseif isa(arg, 'char')
                    filename = MyFilename(varargin{1});
                    if strcmp(filename.Ext, '.mat')
                        classfield = fieldnames(obj);
                        warning('off', 'MATLAB:load:variableNotFound');
                        src = load(varargin{1}, obj.TrackingFields{:});
                        obj.Tracking = src;
                        src = load(varargin{1}, classfield{:});
                        warning('on', 'MATLAB:load:variableNotFound');
                        loaded = fieldnames(src);
                        for i=1:length(loaded)
                            obj.(loaded{i}) = src.(loaded{i});
                        end
                        obj.Filename = filename;
                    else % .avi
                        objmat = [filename.Full, '.obj.mat'];
                        if exist(objmat, 'file')
                            obj = obj.Load(objmat, filename.Full, varargin{2:end});
                        else
                            obj.Video = varargin{1};
                        end
                        obj.Filename = MyFilename(objmat);
                    end
                elseif isa(arg, 'CheeseSquare')
                    obj = varargin{1};
                elseif isa(arg, 'struct')
                    obj.Tracking = arg;
                    for i=1:length(obj.TrackingFields)
                        if isfield(arg, obj.TrackingFields{i})
                            %                             %obj.addprop(obj.TrackingFields{i});
                            obj.(obj.TrackingFields{i}) = arg.(obj.TrackingFields{i});
                        end
                    end
                    obj.Filename = obj.Tracking.Source;
                    obj.Video = obj.Tracking.VideoFile;
                end
                obj.Filename = MyFilename(obj.Filename);
                obj.Prefix = obj.GetPrefix;
                if length(varargin) > 1
                    obj.Video = varargin{2};
                end
                if ischar(obj.Video)
                    alt = MyFilename(obj.Video).SetPath(obj.Filename.Path).Full;
                    if exist(obj.Video, 'file')
                        obj.Video = MyVideoReader(obj.Video);
                    elseif exist(alt, 'file')
                        obj.Video = MyVideoReader(alt);
                    else
                        obj.Video = MyVideoReader(alt);
                    end
                else
                    alt = [MyFilename(obj.Prefix).SetPath(obj.Filename.Path).Full '.avi'];
                    if isempty(obj.Video)
                        obj.Video = MyVideoReader(alt);
                    else
                        obj.Video = MyVideoReader(obj.Video);
                    end
                end
                obj.Meta = Q.cpfield(obj.LoadMetaData, obj.Meta);
                if obj.nSubjects == 0 && Q.isfield(obj, 'Colors.Histrogram.Count')
                    obj.nSubjects = length(obj.Colors.Histrogram.Count);
                end
            end
        end
        
        function meta = LoadMetaData(obj)
            %% load group properties
            meta = struct();
            try
                meta = CheeseSquare.ParseName(obj.Video.Filename);
            catch
            end
            
            %%
            try
                videofile = MyFilename(obj.Video.Filename);
                videofile = videofile.SetExt([videofile.Ext, '.xml']);
                meta.CreationDate = addtodate(MyFilename(obj.Video.Filename).CreationDate, -round(obj.Video.NumberOfFrames / obj.Video.FrameRate*1000), 'millisecond');
                meta.Remarks = '';
                if exist(videofile.Full, 'file')
                    metaxml = xmlread(videofile.Full);
                    cdate = char(metaxml.getElementsByTagName('Start').item(0).getTextContent);
                    meta.CreationDate = datenum(cdate, 'yyyy-mm-ddTHH:MM:SS.FFF');
                    meta.Remarks =  char(metaxml.getElementsByTagName('Remarks').item(0).getTextContent);
                end
            catch
            end
        end
        
        function track = ToClassical(obj)
            if ischar(obj.Video)
                track = CheeseObject.Create(obj.Video);
            else
                track = CheeseObject.Create(obj.Video.Filename);
            end
            track.nSubjects = obj.GetField('nSubjects');
            track.Colors = obj.GetField('Colors');
            track.Colors.Centers = MyMouseColormap;
            track.BkgImage = obj.GetField('Background.im');
            track.ROI = obj.GetField('ROI');
            track.Hierarchy = obj.GetField('Hierarchy');
            
            if isstruct(obj.Tracking)
                f = fieldnames(obj.Tracking);
                for i=1:length(f)
                    if ~strcmpi(f{i}, 'Properties')
                        track.(f{i}) = obj.Tracking.(f{i});
                    end
                end
            end
        end
        
        function obj = SaveOldFormat(obj)
            if isstruct(obj.Tracking)
                f = fieldnames(obj.Tracking);
                track = struct();
                for i=1:length(f)
                    if ~strcmpi(f{i}, 'Properties')
                        track.(f{i}) = obj.Tracking.(f{i});
                    end
                end
            end
            track.Background = obj.Background;
            track.Colors = obj.Colors;
            track.ROI = obj.ROI;
            track.OutputPath = [obj.Filename.Path filesep];
            track.FilePrefix = regexprep(obj.Filename.Name, '\.obj', '', 'ignorecase');
            track.VideoFile = obj.GetField('Video.Filename');
            TrackSave(track);
        end
        
        function obj = ExportProfile(obj)
            %%
            profile = struct();
            %profile.x            = obj.GetField('Tracking.x');
            %profile.y            = obj.GetField('Tracking.y');
            %profile.Hierarchy    = obj.GetField('Hierarchy');
            %profile.Profile      = obj.GetField('Profile', 0);
            profile.Background   = im2uint8(obj.GetField('Background.im'));
            profile.Regions      = im2uint8(obj.GetField('Background.im'));
            profile.IsTracked    = obj.IsTracked();
            profile.IsReady      = obj.IsReady();
            profile.Remarks      = obj.GetField('Meta.Remarks', '');
            profile.CreationDate = datestr(obj.GetField('Meta.CreationDate'), 'yyyy-mm-ddTHH:MM:SS.FFF');
            profile.Duration     = obj.GetField('Video.Duration');
            profile.nSubjects    = obj.GetField('nSubjects', 0);
            profile.Colors       = [];
            save([obj.Video.Filename '.profile.mat'], '-struct', 'profile', '-v6');
        end
        
        function obj = Save(obj)
            out = obj.SerializeOut(); %#ok<NASGU>
            save(obj.Filename.Full, '-struct', 'out', '-v7.3');
            try
                obj.ExportProfile();
            catch
            end
        end
        
        function y = IsTracked(obj)
            y = true;
            y = y && EnsureField(obj, 'Tracking.x');
            y = y && EnsureField(obj, 'Tracking.y');
            y = y && EnsureField(obj, 'Tracking.zones');
        end
        
        function y = IsReady(obj)
            y = true;
            y = y && EnsureField(obj, 'Background.im');
            y = y && EnsureField(obj, 'Colors.Histrogram.Count');
            y = y && EnsureField(obj, 'ROI.Regions');
        end
       
        
        function Show(cheese)
            imagesc(cheese.Video.CurrentFrame);
            axis off;
            axis equal;
            w = 7;
            if Q.isfield(cheese, 'Tracking.x')
                Fig.Hon
                frame = cheese.Video.FrameNumber;
                for s=1:cheese.nSubjects
                    x = double(cheese.Tracking.x(s, frame));
                    y = double(cheese.Tracking.y(s, frame));
                    color = uint8(mean(cheese.Colors.Marks.color(cheese.Colors.Marks.id == s, :)));
                    patch([x-w x+w x+w x-w], [y-w y-w y+w y+w], 'w', 'edgecolor', 'w', 'facecolor', color);
                    text(x+w, y+w, num2str(s), 'color', color, 'HorizontalAlignment', 'left', 'VerticalAlignment', 'top');
                end
                Fig.Hoff
            end
        end
    end
    
    methods (Static = true)
        function meta = ParseName(name)
            %%
            %[pathstr, filename, ~] = fileparts(name);
            expression = '(?<grouptype>\w+)\.exp(?<groupid>\d+)\.day(?<dayid>\d+)\.cam(?<camid>\d+)';
            res = regexp(name, expression, 'names');
            meta.GroupType = res.grouptype;
            meta.GroupId = str2double(res.groupid);
            meta.DayId = str2double(res.dayid);
            meta.CameraId = str2double(res.camid);
        end
        
        %%
        function objs = RunOnGroup(obj, cmd, issave)
            if nargin < 3
                issave = true;
            end
            objs = CheeseSquare.LoadGroup(obj);
            for i=1:length(objs)
                Console.Message('running ''%s'' on ''%s''', func2str(cmd), objs{i}.Prefix);
                objs{i} = cmd(objs{i});
                if issave
                    objs{i}.Save;
                end
            end
        end
        
        function objs = LoadGroup(obj)
            if ischar(obj)
                fname = MyFilename(obj);
                meta = CheeseSquare.ParseName(fname.Full);
            elseif ischar(obj.Video)
                fname = MyFilename(obj.Video);
                meta = obj.Meta;
            else
                fname = MyFilename(obj.Video.Filename);
                meta = obj.Meta;
            end
            template = CheeseSquare.GeneratePrefix(meta.GroupType, meta.GroupId, [], meta.CameraId);
            objs = {};
            idx = 1;
            for i=1:99
                currfile = fname.SetName(sprintf(template, i)).Full;
                if exist(currfile, 'file') || exist([currfile '.obj.mat'], 'file')
                    Console.Message('loading ''%s''', sprintf(template, i));
                    objs{idx} = CheeseSquare(currfile);
                    idx = idx + 1;
                end
            end
        end
        
        
        %%
        function ShowFull(obj, type)
            %%
            im = obj.Video.CurrentFrame;
            if isempty(obj.Video.CurrentFrame)
                obj.Video.FrameNumber = 1;
                im = obj.Video.CurrentFrame;
            end
            if nargin == 1
                type = 'full';
            end
            i = obj.Video.FrameNumber;
            switch type
                case 'full'
                    %%
                    subplot(2,2,1);
                    imagesc(im);
                    axis off;
                    %
                    try
                        Fig.Hon
                        for s=1:obj.nSubjects
                            plot(obj.Tracking.x(s, i), obj.Tracking.y(s, i), 'o', 'MarkerFaceColor', mean(obj.Colors.Marks.color(obj.Colors.Marks.id == s, :)/255), 'MarkerEdgeColor', 'w');
                        end
                    catch
                    end
                    Fig.Hoff
                    %
                    subplot(2,2,2);
                    imagesc(CV.HistEq(im));
                    axis off;
                    %
                    Fig.Hon
                    try
                        for s=1:obj.nSubjects
                            color = mean(obj.Colors.Marks.color(obj.Colors.Marks.id == s, :)/255);
                            plot(obj.Tracking.x(s, i), obj.Tracking.y(s, i), 'o', 'Color', color);
                            text(obj.Tracking.x(s, i), obj.Tracking.y(s, i), obj.ROI.RegionNames{obj.Tracking.zone(s, i)}, 'color', color);
                        end
                    catch
                    end
                    Fig.Hoff
                    %%
                    Fig.Hon
                    try
                        zmap = CheeseSquare.ZoneColormap;
                        map = containers.Map(zmap(1:2:end), zmap(2:2:end));
                        for z=1:obj.ROI.nRegions
                            %%
                            if ~map.isKey(obj.ROI.RegionNames{z})
                                warning('no color defined for ''%s''', obj.ROI.RegionNames{z});
                            else
                                color = map(obj.ROI.RegionNames{z});
                                [row, col] = find(obj.ROI.Regions{z}, 1);
                                contour = bwtraceboundary(obj.ROI.Regions{z}, [row col],'W',8);
                                plot(contour(:, 2), contour(:, 1), '.-', 'color', color);
                            end
                        end
                    catch
                    end
                    Fig.Hoff
                    
            end
            %%
        end
    end
    
    methods (Hidden)
        function prefix = GetPrefix(obj)
            filename = obj.Filename.Full;
            [begi, endi] = regexp(filename, '[^\\\/]*\.exp\d+\.day\d+\.cam\d+');
            prefix = filename(begi:endi);
        end
        
        
        function y = GetField(obj, field, default)
            if nargin < 3
                default = [];
            end
            if obj.EnsureField(field)
                try
                    y = eval(['obj.' field]);
                catch
                    y = default;
                end
            else
                y = default;
            end
        end
        
        function y = EnsureField(obj, field) %#ok<INUSL>
            try
                y = eval(['~isempty(obj.' field ')']);
            catch
                y = false;
            end
        end
        
    end
end