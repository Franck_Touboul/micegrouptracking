classdef Job < handle
    properties
        TempPath = '../temp/';
        ID = '';
        Temp = [];
        Cmd = '';
        Args = '';
        JobID = 0;
        LogFile = '';
        ErrFile = '';
    end
    
    properties (Access=protected)
        CManager = ClusterManager;
        isTempFile = false;
        Source = [];
        TempFilename = '';
        OutputFilename = '';
    end

    properties (Dependent = true, SetAccess = private)
        Status;
        JobStatus;
        Object;
    end
    
    methods
        function obj = Job(source)
            EnsurePath(obj.TempPath);
            obj.Source = TrackLoad(source);
            obj.Temp = obj.Source;
            rand('twister',sum(100*clock));
            randn('seed',sum(100*clock));
            obj.ID = hash(randi(999999999), 'MD5');
            obj.Temp.FilePrefix = obj.ID;
            obj.Temp.OutputPath = obj.TempPath;
            obj.TempFilename = [obj.TempPath obj.ID '.obj.mat'];
            obj.OutputFilename = [obj.TempPath obj.ID '.output'];
            TouchFile(obj.OutputFilename);
        end
        
        function obj = Kill(obj)
            if obj.JobID > 0 && ~strcmp(obj.JobStatus, 'unavail')
                fprintf(['#     killing job id: ' num2str(obj.JobID) '\n']);
                [stat, output] = system(['bkill ' num2str(obj.JobID)]);
                fprintf( '#        . %s', output);
            end            
        end
        
        function obj = Run(obj, funcstr)
            Kill(obj);
            if ~obj.isTempFile
                TrackSave(obj.Temp);
                obj.isTempFile = true;
            end
            [stat, output] = system(['rm ' obj.OutputFilename]);
            
            if isa(funcstr, 'function_handle')
                funcstr = func2str(funcstr);
            end
            obj.CManager.WaitForVacancy;
            obj.Cmd = 'myTaskBkg';
            obj.Args = ['RunInBackgroundAux "''' obj.ID '''" "''' funcstr '''"'];

            [stat, output] = system([obj.Cmd ' ' obj.Args]);
            fprintf('%s', output);
            obj.JobID = str2double(regexprep(regexprep(output , '.*job:[ ]*', ''), ' +.*', ''));
            logs = regexp(regexprep(regexprep(output, '^[^\(]*\([ ]*', ''), '(^[^\)]*)\).*', '$1'), ' +', 'split');
            obj.LogFile = logs{1};
            obj.ErrFile = logs{2};
        end
        
        function value = get.Status(obj)
            jobstat = obj.JobStatus;
            if strcmp(jobstat, 'waiting')
                value = 'waiting';
            elseif strcmp(jobstat, 'running')
                value = 'running';
            elseif ~exist(obj.OutputFilename, 'file')
                value = 'waiting';
            else
                strud = ReadStrudels(obj.OutputFilename);
                if isfield(strud, 'status')
                    value = strud.status;
                else
                    value = 'waiting';
                end
            end
        end

        function value = get.JobStatus(obj)
            value = obj.CManager.GetJobStatus(obj.JobID);
        end
        
        
        function value = get.Object(obj)
            value = TrackLoad(obj.TempFilename);
        end

        
        function obj = WaitUntilFinished(obj)
            running = true;
            waiting = false;
            logid = 0;
            count = 1;
            while running || waiting
                if count > 1
                    b = '\b\b\b\b\b\b';
                else
                    b = '';
                end
                stat = obj.Status;
                waiting = false;
                switch stat
                    case 'done'
                        if count > 1
                            fprintf([b '\n']);
                        end
                        running = false;
                    case 'failed'
                        if count > 1
                            fprintf([b '\n']);
                        end
                        strud = ReadStrudels(obj.OutputFilename);
                        warning('Job:failed', 'failed running job for %s (%s): %s', obj.Source.FilePrefix, obj.ID, strud.message);
                        running = false;
                    case 'running'
                        running = true;
                    case 'waiting'
                        running = false;
                        waiting = true;
                    otherwise
                        throw(MException('Tracking:InvalidBackgroundObject', ['background object generation failed for ' bkgobj.FilePrefix ' (' bkgobj.SourceObj.FilePrefix '), due to ' strud.message]));
                end
                if waiting || running
                    if count == 1
                        fprintf(['# waiting for background object: ' obj.Source.FilePrefix ' (' obj.ID ')   ']);
                    end
%                     if running
%                         if logid == 0
%                             if ~exist(obj.LogFile, 'file')
%                                 waitforfile(obj.LogFile, 25);
%                             end
%                             logid = fopen(obj.LogFile);
%                         end
%                         tline = fgetl(logid);
%                         while ischar(tline)
%                             fprintf('\n# (<)   . %s', tline);
%                             tline = fgetl(logid);
%                         end
%                     end
                    switch mod(count-1,4)
                        case 0
                            fprintf([b '[   ] ']);
                        case 1
                            fprintf([b '[#  ] ']);
                        case 2
                            fprintf([b '[## ] ']);
                        case 3
                            fprintf([b '[###] ']);
                    end
                    count = count + 1;
                    pause(.5);
                end
            end
            if logid ~= 0
                fclose(logid);
            end
            
        end
        
        function delete(obj)
            if ~isempty(obj.TempFilename)
                fprintf(['# (x) removing temp file ' obj.TempFilename '\n']);
                system(['rm ' obj.TempFilename]);
                system(['rm ' obj.OutputFilename]);
            end
            Kill(obj);
        end
    end
end