function WaitForLicense(tb)
first = true;
while (~license('checkout',tb))
    % do nothing and wait indefinitely
    if first
        MyPrint(['waiting for available licenses for ''' tb '''...']);
        first = false;
    end
end
if ~first
    fprintf('[Done]\n');
end