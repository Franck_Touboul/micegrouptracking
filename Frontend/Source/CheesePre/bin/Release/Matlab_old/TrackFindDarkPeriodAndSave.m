function obj=TrackFindDarkPeriodAndSave(obj)
obj = TrackLoad(obj);
obj = TrackFindDarkPeriod(obj, .2);
TrackSave(obj);