function obj = CheeseHierarchy(obj, force, arg)
%%
obj = TrackLoad(obj);
if ~exist('force', 'var')
    force = true;
end
if ~exist('arg', 'var')
    arg = struct();
end
if force
    obj = SocialPredPreyModel(obj, arg);
    obj = SocialAggrClassifier(obj);
    %obj = SocialAnalysePredPreyModel(obj);
end

%% newer version
fields = {'ChaseEscape', 'AggressiveChase'};

for i=1:length(fields)
    edges = cell(1,2); edges{1} = 1:obj.nSubjects; edges{2} = edges{1};
    ce = hist3(...
        [obj.Contacts.Behaviors.(fields{i}).Chaser(obj.Contacts.Behaviors.(fields{i}).Map); ...
        obj.Contacts.Behaviors.(fields{i}).Escaper(obj.Contacts.Behaviors.(fields{i}).Map)]', ...
        'Edges', edges);
    mat = ce - ce';
    mat(binotest(ce, ce + ce')) = 0;
    mat = mat .* (mat > 0);
    
    [rank, removed] = TopoFeedbackArcSetHierarchicalOrder(mat);
    diluted = mat;
    diluted(removed ~= 0) = 0;
    
    obj.Hierarchy.(fields{i}).rank = rank;
    obj.Hierarchy.(fields{i}).nLevels = length(unique(rank));
    obj.Hierarchy.(fields{i}).strength = sum(diluted(:) > 0);
    obj.Hierarchy.(fields{i}).map = diluted;
    obj.Hierarchy.(fields{i}).interactions = obj.Interactions.PredPrey;
    obj.Hierarchy.(fields{i}).removed = removed;
    [q, obj.Hierarchy.(fields{i}).AlphaToDelta] = sort(obj.Hierarchy.ChaseEscape.rank, 'descend');
    obj.Hierarchy.(fields{i}).ChaseEscape = ce;
end
