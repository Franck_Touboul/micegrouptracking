function obj = CheesePotts(obj, param, confidence)
% Syntax:
%       obj = SocialPotts(obj)
%       obj = SocialPotts(obj, model)
%       obj = SocialPotts(obj, zones)
% Adds the following information to the obj:
%   obj.Analysis.Potts
%       + Model: a cell contatining the Maximum
%                                   Entropy (ME) model for each level
%                                   (independent, pairwise, etc.)
%       + Entropy: the entropy of each ME level
%       + In: the mutual information
%       + Ik:
%       + nIters: number of iters used the ME model
%%
if isa(obj, 'CheeseSquare')
    track = obj.ToClassical;
else
    track = obj;
end

output = isdesktop;
equalAxis = true;
showAllOrders = true;
plotToFile = false;
if nargin < 3
    confidence = 0.05;
end
%%
fprintf('# computing the potts model\n');
if nargin > 1
    if iscell(param)
        model = param;
        param = struct();
    elseif ~isempty(param) && isnumeric(param)
        zones = param;
        param = struct();
    end
else
    param = struct();
end
track = TrackLoad(track);
options = setDefaultParameters(param, 'order', 1:track.nSubjects, 'InParallel', false, 'Regularize', false);
%%
if output && track.OutputToFile
    savePrefix = [track.OutputPath track.FilePrefix '.potts'];
else
    savePrefix = '';
end
%savePrefix = [obj.OutputPath obj.FilePrefix '.potts'];
savePrefix = '';
track.Analysis.Potts.nIters = [5000 50000];
track.Analysis.Potts.MinNumberOfIters = [0 0];
track.Analysis.Potts.Confidence = confidence;

%
%% train potts model if needed
if exist('model', 'var')
    me = model;
else
    if exist('zones', 'var')
        me = TrainPottsModel(track, options.order, zones, options.InParallel, options.Regularize);
    else
        me = TrainPottsModel(track, options.order, [], options.InParallel, options.Regularize);
    end
end
track.Analysis.Potts.Model = me;
analysis.me = me;

try
    %% compute independent probabilities
    %independentProbs = computeIndependentProbs(data, count);
    
    %% compute joint probabilities
    %[jointProbs, jointPci] = computeJointProbs(data, count);
    
    %% compute probability for each sample
    fprintf('# - computing joint probabilities\n');
    [track, independentProbs, jointProbs] = SocialComputePatternProbs(track, false);
    
    % [sampleProbIndep, sampleProbJoint] = computeSampleProb(data, count, independentProbs, jointProbs);
    % fprintf('mean log-likelihood = %6.4f\n', mean(log(sampleProbJoint)));
    %% compute MaxEnt probabilities for each sample
    Entropy = [];
    for level=options.order
        fprintf('# computing max-entropy probabilities for level %d\n', level);
        %    p = exp(me{level}.features * me{level}.weights');
        p = exp(me{level}.perms * me{level}.weights');
        perms_p = exp(me{level}.perms * me{level}.weights');
        Z = sum(perms_p);
        p = p / Z;
        Entropy(level) = -p' * log2(p);
        %%
        track.Analysis.Potts.Model{level}.surprise = jointProbs(:)' .* log(jointProbs(:)') - jointProbs(:)' .* log(p(:)');
        track.Analysis.Potts.Model{level}.prob = p;
        track.Analysis.Potts.Model{level}.jointProb = jointProbs;
        track.Analysis.Potts.Model{level}.order = cellfun(@(x) size(x,2), track.Analysis.Potts.Model{level}.labels);
        Djs = JensenShannonDivergence(jointProbs, p');
        Dkl = KullbackLeiblerDivergence(jointProbs, p');
        track.Analysis.Potts.Model{level}.Djs = Djs;
        %% plot
        if output && (showAllOrders || level < track.nSubjects)
            if isempty(savePrefix)
                if plotToFile
                    figure(level);
                else
                    subplot(3,2,level);
                end
            else
                %subplot(2,2,1);
            end
            p1 = jointProbs;
            p2 = p;
            
            PlotProbProb(p1,p2',sum(track.valid), equalAxis)
            
            tit = ['Maximum-Entropy Model (D_{kl}=' num2str(Dkl) ' D_{js}=' num2str(Djs) ')'];
            prettyPlot(tit, 'observed pattern prob [log10]', 'ME pattern prob [log10]');
            
            if ~isempty(savePrefix)
                saveFigure([savePrefix sprintf('.level_%d', level)]);
            end
        end
    end
    if ~equalAxis
        if showAllOrders
            TieAxis(2,2,1:track.nSubjects);
        else
            TieAxis(2,2,1:track.nSubjects-1);
        end
    end
    %%
    if length(options.order) == track.nSubjects
        In = Entropy(1) - Entropy(end);
        Ik = -diff(Entropy);
        cmap = MyCategoricalColormap;
        if output
            if ~isempty(savePrefix)
                subplot(2,2,1);
            else
                if plotToFile
                    figure(track.nSubjects + 1);
                else
                    subplot(3,2,track.nSubjects + 1);
                end
            end
            %pie(Ik/In, {'pairs', 'triples', 'quadruples'});
            myCumBoxPlot(cumsum(Ik), {'Pairwise (I_{(2)})', 'Triplet (I_{(3)})', 'Quadruplet (I_{(4)})'}, cmap(1:end, :));
            title('K''th-Order Correlations');
            if ~isempty(savePrefix)
                saveFigure([savePrefix '.kth_order_corr']);
            end
        end
        %%
        track.Analysis.Potts.Entropy = Entropy;
        track.Analysis.Potts.In = In;
        track.Analysis.Potts.Ik = Ik;
    end
    if (plotToFile)
        %%
        p = 'Graphs/SocialPotts/';
        mkdir(p);
        for i=1:track.nSubjects + 1
            figure(i);
            saveFigure([p track.FilePrefix '.' num2str(i)]);
        end
    end
    %%
catch err
    err.message
end


if isa(obj, 'CheeseSquare')
    obj.Analysis.Potts = track.Analysis.Potts;
else
    obj = track;
end