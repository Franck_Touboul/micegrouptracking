function score = ComputeScore(groundTruth, result)
% [correct, false-positive, false-negative, count];
groundTruth = logical(groundTruth);
result = logical(result); 
score = [0 0 0 0];
score(1) = sum(groundTruth(result));
score(2) = sum(groundTruth(result) == false);
score(3) = sum(result(groundTruth) == false);
score(4) = length(result);

