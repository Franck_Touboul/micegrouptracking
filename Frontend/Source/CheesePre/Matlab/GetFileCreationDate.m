function creationDate = GetFileCreationDate(filename)
if ~isunix
    basename = regexprep(filename, '.*[/\\]', '');
    [status, result] = system(['dir /T:C ' filename]);
    if status ~= 0
        creationDate = [];
        return;
    end
    [begin, finish] = regexp(result, ['[^\n]*' basename '\n']);
    if isempty(begin)
        creationDate = [];
        return;
    end
    line = result(begin:finish);
    creationDate = datenum(regexprep(line, [' *[^ ]* *' basename '\n'], ''));
else
    [status, result] = system(['ls -l ' filename]);
    if status ~= 0
        creationDate = [];
        return;
    end
    fields = regexp(result,'[\t ]+','split');
    creationDate = datenum([fields{6} ' ' fields{7} ' ' fields{8}]);
end