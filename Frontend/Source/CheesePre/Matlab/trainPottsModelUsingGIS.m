function me = trainPottsModelUsingGIS(options, data, confidence)
options.param_report = false;
options = setDefaultParameters(options, ...
    'KLConvergenceRatio', 1e-7, ...
    'Statistics', false, ...
    'MinOccurance', 0);
options.NeutralZone = 1; % Open
options.MaxLog = 10;
options.MinExpectation = .1 / size(data, 2);
%%
me.KLConvergenceRatio = options.KLConvergenceRatio;

use_sparse = true;
if nargin < 3
    confidence = .05;
end
me.confidence = confidence;
me.neutralZone = options.NeutralZone;

if isfield(options, 'Output')
    output = options.Output;
else
    output = true;
end
%
if output; fprintf('# Training Potts Model\n'); end
%
if output; fprintf('# - finding all permutation of max dimension %d...\n', options.n); end;
%%
allPerms = nchoose(1:options.nSubjects);
nPerms = {};
j = 1;
for i=1:length(allPerms)
    if length(allPerms{i}) <= options.n 
        nPerms{j} = allPerms{i};
        j = j + 1;
    end
end

%%
if output; fprintf('# - computing constraints...\n'); end
[me.constraints, pci] = PottsComputeConstraints(data' + 1, options.count, nPerms, abs(confidence));
C = length(nPerms);
%
me.inputPerms = myPerms(options.nSubjects, options.count);
[me.perms, me.labels] = assignFeature(me.inputPerms, options.count, nPerms, use_sparse);

valid = false(1, length(me.labels));
% if options.count == 2
%     for i=1:length(me.labels)
%         if all(me.labels{i}(2, :) ~= 2)
%             valid(i) = true;
%         end
%     end
%     me.labels = me.labels(valid);
%     me.perms = me.perms(:, valid);
%     me.constraints = me.constraints(valid);
%     pci = pci(valid);
% end
%
me.nSubjects = options.nSubjects;
me.range = options.count;

%% remove small occurences
if options.MinOccurance > 0 && options.MinOccurance < inf
    occ = me.constraints * size(data, 2);
    valid = ~(occ > 0 & occ < options.MinOccurance);
    me.labels = me.labels(valid);
    me.perms = me.perms(:, valid);
    me.constraints = me.constraints(valid);
    pci = pci(valid, :);
end

%% initialize the weights
if isfield(options, 'initialPottsWeights') && ~isempty(options.initialPottsWeights)
    me.weights = options.initialPottsWeights;
else
    me.weights = randn(1, size(me.labels, 2)) / size(me.labels, 2);
end
orig = me;

%% filter neutral zone
neutral = false(1, length(orig.labels));
for l=1:length(me.labels)
    neutral(l) = any(orig.labels{l}(2, :) == options.NeutralZone);
end
me.constraints = orig.constraints(~neutral);
me.perms = orig.perms(:, ~neutral);
me.labels = orig.labels(~neutral);
% initialize the weights
if isfield(options, 'initialPottsWeights') && ~isempty(options.initialPottsWeights)
    me.weights = options.initialPottsWeights;
else
    me.weights = orig.weights(~neutral);
end
%
me.momentum = me.weights;
pci = pci(~neutral, :);

%% compute empirical probabilities
base = me.range;
baseVector = base.^(size(data, 1)-1:-1:0);
pEmp = histc(baseVector * data+1, 1:base ^ size(data, 1));
pEmp = pEmp / sum(pEmp);
%% initial halfKL
p = exp(me.perms * me.weights');
Z = sum(p);
p = p / Z;
kl2 = -pEmp * log2(p);
kl1 = pEmp * log2(pEmp' + (pEmp' == 0));
%%
if options.Statistics; me.Statistics = struct(); end
tic;
me.converged = false;
me.KLconverged = false;
for iter=1:options.nIters
    if output; fprintf('# - GIS, order %d, iter (%4d/%4d) : ', options.n, iter, options.nIters); end
    % compute the (un-normalized) pdf for each sample
    p = exp(me.perms * me.weights');
    % normalize the pdf (the Z)
    Z = sum(p);
    p = p / Z;
    prev_kl = kl1 + kl2;
    kl2 = -pEmp * log2(p + (pEmp' == 0));
    %E = full(sum(me.perms .* repmat(perms_p / Z, 1, size(me.perms, 2))));
    E = p' * me.perms;
    if confidence ~= 0
        %[sum(E'  > pci(:, 1) & E' < pci(:, 2)) (1 - confidence) * length(me.weights)]
        nconverged = sum(E'  > pci(:, 1) & E' < pci(:, 2));
        if output; fprintf('KL = %6.4f, convergence = %5.2f%%, KL-convergence = %6.4e\n', kl1 + kl2, (nconverged / length(me.weights) * 100), abs((kl1 + kl2) - prev_kl)); end
        if options.Statistics;
            me.Statistics(iter).convergence = nconverged / length(me.weights);
        end
        me.nconverged = nconverged;
        if nconverged == length(me.weights) && iter >= options.MinNumberOfIters 
            %%
            if output && ~me.converged;
                fprintf('# converged to confidence interval (alpha = %3.2f)\n', confidence);
            end
            me.converged = true;
            if me.KLConvergenceRatio > 0
                if abs((kl1 + kl2) - prev_kl) <= me.KLConvergenceRatio
                    if me.KLconverged
                        if output;
                            fprintf('# converged to KL convergence ratio (ratio = %3.2f)\n', me.KLConvergenceRatio);
                        end
                        break;
                    else
                        me.KLconverged = true;
                    end
                elseif me.KLconverged
                    me.KLconverged = false;
                end
            else
                break;
            end
        end
    else
        if output; fprintf('KL = %6.4f (%.3f)\n', kl1 + kl2, abs((kl1 + kl2 - prev_kl)/prev_kl)); end
    end
    %
    dw = log(me.constraints ./ E);
    %dw(me.constraints == 0) = me.constraints(me.constraints == 0) - E(me.constraints == 0);
    %dw = max(dw, options.MinLog);
    %dw = log(me.constraints ./ E');
    %dw(me.constraints == 0) = 0;
    dw(me.constraints == 0 & E < options.MinExpectation) = 0;
    dw = MyTrim(dw, [-options.MaxLog options.MaxLog]);
    me.weights = me.weights + 1/C * dw;
    %%
    if options.Statistics;
        me.Statistics(iter).times = toc; 
        me.Statistics(iter).KL = kl1 + kl2;
    end
end
me.nIters = iter;
me.Dkl = kl1 + kl2;
%me = rmfield(me, 'features');
