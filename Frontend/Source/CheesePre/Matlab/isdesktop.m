function b = isdesktop
a = javachk('desktop');
b = isempty(a);
