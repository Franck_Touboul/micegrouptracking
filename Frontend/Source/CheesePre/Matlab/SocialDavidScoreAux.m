function [w, w2, l, l2] = SocialDavidScoreAux(Pij)
w = sum(Pij, 2);
w2 = Pij * w;
l = sum(Pij, 1)';
l2 = Pij' * l;
