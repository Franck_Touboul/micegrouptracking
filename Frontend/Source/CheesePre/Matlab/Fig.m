classdef Fig
    properties (Constant)
        Margin = .025;
        Padding = .1;
    end
        
    methods (Static = true)
        function Hon
            hold on;
        end

        function Hoff
            hold off;
        end
        
        function Equal()
            %%
            
            axis square;
            x = xlim;
            y = ylim;
            z = [min(x(1), y(1)) max(x(2), y(2))];
            axis([z, z]);
            Fig.Hon
            plot([z(1) z(end)], [z(1) z(end)], 'k-');
            Fig.Hoff
            %set(gca, 'Ytick', get(gca, 'xtick'));
        end
        
        function AddLayer(alpha)
            %%
            if nargin < 1
                alpha = .5;
            end
            H = findall(gca, '-property', 'xdata');
            for i=1:length(H)
                h = H(i);
                %%
                meta = get(h);
                names = fieldnames(meta);
                %%
                props = ~cellfun(@isempty, regexpi(names, 'color'));
                for k=find(props(:))'
                    v = meta.(names{k});
                    if isnumeric(v) && length(v) == 3
                        color = v;
                        set(h, names{k}, color + (1-color) * alpha);
                    end
                end
                %%
                if false
                props = ~cellfun(@isempty, regexpi(names, 'FaceVertexCData'));
                for k=find(props(:))'
                    v = meta.(names{k});
                    if isnumeric(v) && size(v, ndims(v)) == 1
                        if size(v, 2) == 1
                            v = squeeze(ind2rgb(v, colormap));
                        else
                            v = ind2rgb(v, colormap);
                        end
                    end
                    if isnumeric(v) && size(v, ndims(v)) == 3
                        v = v + (1 - v) * alpha;
%                         hsv = Colors.RGB2HSV(v);
%                         if ndims(hsv) == 2
%                             hsv(:, 2) = hsv(:, 2) / 2;
%                         else
%                             hsv(:, :, 2) = hsv(:, :, 2) / 2;
%                         end
                        set(h, names{k}, v);
                    end
                end
                end
            end
            Fig.Hon
        end
        
        function Fix
            set(gca, ...
                'Box'         , 'off'     , ...
                'TickDir'     , 'out'     , ...
                'TickLength'  , [.005 .005] , ...
                'YMinorTick'  , 'on'      , ...
                'XColor'      , [.3 .3 .3], ...
                'YColor'      , [.3 .3 .3], ...
                'XMinorTick'  , 'on'      , ...
                'LineWidth'   , 1         );
            
            set(gca, 'FontName', 'Calibri Light', 'FontSize', 12);
            set(gcf, 'color', [1 1 1]);            
        end

        function FixFonts
            set(gca, 'FontName', 'Calibri Light', 'FontSize', 12);
        end

        function res = DefaultAxesFont
            res = {'FontName', 'Calibri Light', 'FontSize', 12,  'color', [.3 .3 .3]};
        end
        
        function res = DefaultLabelFont
            res = {'FontName', 'Calibri Light', 'FontSize', 14};
        end
        
        function Legend(t, varargin)
            if isnumeric(t)
                entries = cell(1, length(t));
                for i=1:length(t)
                    entries{i} = num2str(t(i));
                end
                h = legend(entries, varargin{:});
                legend boxoff;
            else
                h = legend(t{:}, varargin{:});
                legend boxoff;
            end
            fonts = Fig.DefaultLabelFont;
            set(h, fonts{:});
            
        end
        
        function s = IgnoreInLegend()
            s = {'HandleVisibility','off'};
        end
        
        function x = Axis(varargin)
            if nargin == 4
                Fig.XAxis(varargin{1}, varargin{2});
                Fig.YAxis(varargin{3}, varargin{4});
                return;
            end
            if nargin == 2 && length(varargin{1}) == 2 && length(varargin{2}) == 2
                Fig.XAxis(varargin{1});
                Fig.YAxis(varargin{2});
            end
            error('wrong number of arguments');
        end
        
        function XAxis(x1, x2)
            if nargin == 0
                limits = objbounds(findall(gca));
                if isempty(limits) % no objects in axes with data limits
                    limits = [get(gca,'XLim') get(gca,'YLim') get(gca,'ZLim')];
                end
                set(gca, 'xlim', limits(1:2));
                return;
            end
            a = axis;
            if nargin == 1
                x2 = x1(2);
                x1 = x1(1);
            end
            if isempty(x2); x2 = a(2); end
            if isempty(x1); x1 = a(1); end
            axis([x1 x2 a(3) a(4)]);
        end
        
        function YAxis(y1, y2)
            if nargin == 0
                limits = objbounds(findall(gca));
                if isempty(limits) % no objects in axes with data limits
                    limits = [get(gca,'XLim') get(gca,'YLim') get(gca,'ZLim')];
                end
                set(gca, 'ylim', limits(3:4));
                return;
            end
            a = axis;
            if nargin == 1
                y2 = y1(2);
                y1 = y1(1);
            end
            if isempty(y2); y2 = a(4); end
            if isempty(y1); y1 = a(3); end
            axis([a(1) a(2) y1 y2]);
        end
        
        function HLine(Y, varargin)
            % HLine(Y, varargin) draws a horizontal line on the current axes
            a = axis;
            if nargin >= 2
                line([a(1), a(2)], [Y Y], varargin{:});
            else
                line([a(1), a(2)], [Y Y], 'LineStyle', ':', 'Color', 'k');
            end
        end
        
        function VLine(X, varargin)
            % VLine(X, varargin) draws a vertical line on the current axes
            % VLine(X, 'text', text, varargin) draws a vertical line on the
            % current axes and adds text
            a = axis;
            %strcmpi(varargin{:}, 'text')
            if nargin >= 2
                for i=1:length(X)
                    x = X(i);
                    line([x, x], [a(3) a(4)], varargin{:});
                end
            else
                for i=1:length(X)
                    x = X(i);
                    line([x, x], [a(3) a(4)], 'LineStyle', ':', 'Color', 'k');
                end
            end
        end

        function XTick(x, labels)
            set(gca, 'xtick', x);
            if nargin > 1
                set(gca, 'xticklabel', labels);
            end
        end

        function YTick(y, labels)
            set(gca, 'ytick', y);
            if nargin > 1
                set(gca, 'yticklabel', labels);
            end
        end
        
        function YFlip(varargin)
            if nargin > 0
                if islogical(varargin{1}) || isnumeric(varargin{1})
                    if state
                        set(gca, 'ydir', 'normal')
                    else
                        set(gca, 'ydir', 'reverse')
                    end
                else
                    set(gca, 'ydir', varargin{1})
                end
            else
                switch get(gca, 'ydir')
                    case 'normal'
                        set(gca, 'ydir', 'reverse')
                    case 'reverse'
                        set(gca, 'ydir', 'normal')
                end
            end
        end

        function Labels(x, y, varargin)
            Fig.XLabel(x, varargin{:});
            Fig.YLabel(y, varargin{:});
        end
        
        function XLabel(varargin)
            s = sprintf(varargin{:});
            xlabel(s, 'FontSize', 14, 'FontName', 'Calibri Light');
            set(gca, 'FontName', 'Calibri Light', 'FontSize', 12);
        end

        function YLabel(varargin)
            s = sprintf(varargin{:});
            ylabel(s, 'FontSize', 14, 'FontName', 'Calibri Light');
            set(gca, 'FontName', 'Calibri Light', 'FontSize', 12);
        end
        
        function Suptitle(varargin)
            suptitle(varargin{:});
        end
        
        function Title(varargin)
            if ~iscell(varargin{1})
                data = {varargin};
            else
                data = varargin{1};
            end
            s = cell(size(data));
            for i=1:length(data)
                if iscell(data{i})
                    curr = regexprep(data{i}{:}, '\\', '\\\\');
                    s{i} = sprintf(curr);
                else
                    curr = regexprep(data{i}, '\\', '\\\\');
                    s{i} = sprintf(curr);
                end
            end
            title(s, 'FontSize', 16, 'FontName', 'Calibri Light');
            set(gca, 'FontName', 'Calibri Light', 'FontSize', 12);
        end
        
        function Square(count, idx)
            nrows = ceil(sqrt(count));
            ncols = ceil(count / nrows);
            subplot(nrows, ncols, idx);
        end
        
        function Subplot2(rows, cols, idx)
            row = floor((idx-1)/cols+1)
            col = mod(idx-1, cols)+1
            sz = (1 - 2*Fig.Margin);
            axes('Position', [...
                Fig.Margin + (col-1)/cols * sz, ...
                Fig.Margin + (rows - row)/rows * sz, ...
                (sz - Fig.Padding)/rows, (sz-Fig.Padding)/cols]);
        end
        
        function Subplot(rows, cols, row, col)
            if length(row) ~= length(col)
                if isscalar(row)
                    row = col * 0 + row;
                else
                    col = row * 0 + col;
                end
            end
            idx = sub2ind([cols, rows], col, row);
            subplot(rows, cols, idx);
        end
        
        function high = Capture()
            screen_DPI = get(0,'ScreenPixelsPerInch');
            tempfile = [tempname '.png'];
            self.source_fig = gcf;
            self.K = [4 4];
            current_paperpositionmode = get(self.source_fig,'PaperPositionMode');
            current_inverthardcopy = get(self.source_fig,'InvertHardcopy');
            set(self.source_fig,'PaperPositionMode','auto');
            set(self.source_fig,'InvertHardcopy','off');
            print(self.source_fig,['-r',num2str(screen_DPI*self.K(1))], '-dpng', tempfile);
            set(self.source_fig,'InvertHardcopy',current_inverthardcopy);
            set(self.source_fig,'PaperPositionMode',current_paperpositionmode);
            self.raw_hires = imread(tempfile);
            delete(tempfile);
            %%
            myconv = @imfilter;

            % Subsample hires figure image with standard anti-aliasing using a
            % butterworth filter
            kk = Fig.lpfilter(self.K(2)*3,self.K(2)*0.9,2);
            mm = myconv(ones(size(self.raw_hires(:,:,1))),kk,'same');
            a1 = max(min(myconv(single(self.raw_hires(:,:,1))/(256),kk,'same'),1),0)./mm;
            a2 = max(min(myconv(single(self.raw_hires(:,:,2))/(256),kk,'same'),1),0)./mm;
            a3 = max(min(myconv(single(self.raw_hires(:,:,3))/(256),kk,'same'),1),0)./mm;
            if abs(1-self.K(2)) > 0.001
                raw_lowres = double(cat(3,a1(2:self.K(2):end,2:self.K(2):end),a2(2:self.K(2):end,2:self.K(2):end),a3(2:self.K(2):end,2:self.K(2):end)));
            else
                raw_lowres = self.raw_hires;
            end
            %%
            high = raw_lowres;
        end
        
        %% A simple lowpass filter kernel (Butterworth).
        % sz is the size of the filter
        % subsmp is the downsampling factor to be used later
        % n is the degree of the butterworth filter
        function kk = lpfilter(sz, subsmp, n)
            sz = 2*floor(sz/2)+1; % make sure the size of the filter is odd
            cut_frequency = 0.5 / subsmp;
            range = (-(sz-1)/2:(sz-1)/2)/(sz-1);
            [ii,jj] = ndgrid(range,range);
            rr = sqrt(ii.^2+jj.^2);
            kk = ifftshift(1./(1+(rr./cut_frequency).^(2*n)));
            kk = fftshift(real(ifft2(kk)));
            kk = kk./sum(kk(:));
        end
        
        function SavePNG(filename)
            fn = MyFilename(filename);
            fn.SetExt('.png');
            saveas(gcf, fn.Full);
        end
    end
end