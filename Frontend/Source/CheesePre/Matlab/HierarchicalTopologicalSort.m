function rank = HierarchicalTopologicalSort(mat)
if ~graphisdag(sparse(mat))
    rank = [];
    return;
end

rank = ones(1, size(mat,1));
r = isValid(mat, rank);
while ~isempty(r)
    rank(r) = rank(r) + 1;
    r = isValid(mat, rank);
end
rank = max(rank) - rank + 1;
%rank(~(sum(mat, 1) | sum(mat, 2)')) = 1;

function r = isValid(mat, rank)
for i=1:length(rank)
    others = (mat(i, :) > 0) .* rank;
    others(mat(i, :) == 0) = inf;
    if any(others <= rank(i))
        r = find(others <= rank(i), 1);
        return;
    end
end
r = [];


return;
% levels = ones(1, size(mat,2)) * size(mat,2);
% for n=1:size(mat,2)
%     maxlevel = max([levels(mat(:, n) > 0)]) + 1;
%     minlevel = min([levels(mat(n, :) > 0)]) - 1;
%     levels(n) = max([maxlevel, minlevel]) + 1;
% end
% 
% levels(sum(mat>0, 2) + sum(mat>0, 1)' == 0) = max(levels) + 1;
%%
all = 1:size(mat,2);

T = mat;
L = [];
S = all(sum(mat ~= 0, 1) == 0);
R = all * inf;
R(S) = 1;
while ~isempty(S)
    n = S(1);
    S = S(2:end);
    L = [L, n];

    M = find(T(n, :) ~= 0);
    for m = M
        T(n, m) = 0;
        if ~any(T(:, m) ~= 0)
            S = [S, m];
            R(m) = R(n) + 1;
        end
    end
end
rank = max(R) - R + 1;
empty = ~sum(mat>0, 2) & ~sum(mat>0, 1)';
rank(sum(mat>0, 2) == 0) = 1;
rank(empty) = 0;

% %%
% V = all * 0;
% 
% 
% 
% for s=S
%     [V, L] = visit(mat, V, L, s)
% end
% levels = L;
% 
% function [V, L] = visit(mat, V, L, n)
% if ~V(n)
%     V(n) = 1;
%     S = find(mat(n, :) ~= 0);
%     for s=S
%         [V, L] = visit(mat, V, L, s);
%     end
%     L = [L, n];
% end
    