function y = decimal2base(x, base, c)

% Convert a decimanl number into a binary array
% 
% Similar to dec2bin but yields a numerical array instead of a string and is found to
% be rather faster

s = ceil(log(x + 1)/log(base)); % Number of divisions necessary ( rounding up the log2(x) )
if nargin < 2
    c = s;
end

y(c) = 0; % Initialize output array
for i = 1:min(c, s)
    r = floor(x / base);
    y(c+1-i) = x - base*r;
    x = r;
end
