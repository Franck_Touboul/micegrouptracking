function MyPrint(varargin)
[st, i] = dbstack;
n = length(st);
fprintf('# ');
if n>2
    for i=4:n
        fprintf(' ');
    end
    fprintf('- ');
end
fprintf(varargin{:});