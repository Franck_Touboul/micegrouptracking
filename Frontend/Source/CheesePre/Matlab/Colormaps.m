classdef Colormaps
    methods (Static)
        function c = Red;   c = [220 073 089] / 255; end
        function c = Green; c = [053 178 087] / 255; end
        function c = Blue;  c = [000 177 229] / 255; end
    end
    
    methods (Static)
        function z = BaseColors
            z = {...
                'Purple', [202   152   227]/255, ...
                'Red', [196    77    88]/255, ...
                'Orange', [253   189    51]/255, ...
                'Yellow', [253   189    51]/255, ...
                'Green', [179   224   110]/255, ...
                'Blue', [0   177   229]/255 ...
                'White', [236 229 206]/255 ...
                };
        end
        
        function c = Generate(colors)
            % colors - for example: 'PRBY'
            z = Colormaps.BaseColors;
            c = zeros(length(colors), 3);
            for i=1:length(colors)
                idx = find(cellfun(@(x) ~isempty(x), regexp(z(1:2:end), colors(i))), 1);
                c(i, :) = z{2 * (idx-1) + 2};
            end
        end        
        
        function im = MatToIm(mat, cmap)
            valid = ~isnan(mat);
            mat(~valid) = min(mat(:));
            ind = gray2ind(mat2gray(mat), 256) + 1;
            if nargin == 1
                cmap = Colormaps.BlueRed(256);
            end
            ind(~valid) = 0;
            im = ind2rgb(ind, [0 0 0; cmap]);
        end
        
        function cmap = BlueRed(nlevels)
            if nargin == 0
                nlevels = 256;
            end
            cmap = Colormaps.makeColorMap([17 113 185]/255,[170 55 73]/255,nlevels);
        end
        
        function cmap = BlueWhiteRed(nlevels)
            if nargin == 0
                nlevels = 256;
            end
            cmap = Colormaps.makeColorMap([17 113 185]/255,[255 255 255]/255,[170 55 73]/255,nlevels);
        end
        
        function cmap = UglyBlueRed(nlevels)
            if nargin == 0
                nlevels = 256;
            end
            cmap = Colormaps.makeColorMap([0 0 1],[1 0 0],nlevels);
        end
        
        function color = GetColor(idx)
            cmap = Colormaps.Categorical;
            color = cmap(mod(idx-1, size(cmap, 1))+1, :);
        end
        
        function cmap = Categorical
            cmap = Colormaps.SubCategorical;
            cmap = cmap(2:2:end, :);
        end
        
        function cmap = SubCategorical
            cmap = ...
                [148 216 239;
                0 177 229;
                179 213 157;
                53 178 87;
                234 165 193;
                220 73 89;
                240 191 148;
                240 145 55;
                189 190 200;
                98 111 179;
                246 237 170;
                248 231 83]/255;
        end
    end
    
    methods (Static, Access = private)
        function cMap = makeColorMap(varargin)
            %% MAKECOLORMAP makes smoothly varying colormaps
            % a = makeColorMap(beginColor, middleColor, endColor, numSteps);
            % a = makeColorMap(beginColor, endColor, numSteps);
            % a = makeColorMap(beginColor, middleColor, endColor);
            % a = makeColorMap(beginColor, endColor);
            %
            % all colors are specified as RGB triples
            % numSteps is a scalar saying howmany points are in the colormap
            %
            % Examples:
            %
            % peaks;
            % a = makeColorMap([1 0 0],[1 1 1],[0 0 1],40);
            % colormap(a)
            % colorbar
            %
            % peaks;
            % a = makeColorMap([1 0 0],[0 0 1],40);
            % colormap(a)
            % colorbar
            %
            % peaks;
            % a = makeColorMap([1 0 0],[1 1 1],[0 0 1]);
            % colormap(a)
            % colorbar
            %
            % peaks;
            % a = makeColorMap([1 0 0],[0 0 1]);
            % colormap(a)
            % colorbar
            
            % Reference:
            % A. Light & P.J. Bartlein, "The End of the Rainbow? Color Schemes for
            % Improved Data Graphics," Eos,Vol. 85, No. 40, 5 October 2004.
            % http://geography.uoregon.edu/datagraphics/EOS/Light&Bartlein_EOS2004.pdf
            
            defaultNum = 100;
            errorMessage = 'See help MAKECOLORMAP for correct input arguments';
            
            if nargin == 2 %endPoints of colormap only
                color.start  = varargin{1};
                color.middle = [];
                color.end    = varargin{2};
                color.num    = defaultNum;
            elseif nargin == 4 %endPoints, midPoint, and N defined
                color.start  = varargin{1};
                color.middle = varargin{2};
                color.end    = varargin{3};
                color.num    = varargin{4};
            elseif nargin == 3 %endPoints and num OR endpoints and Mid
                if numel(varargin{3}) == 3 %color
                    color.start  = varargin{1};
                    color.middle = varargin{2};
                    color.end    = varargin{3};
                    color.num    = defaultNum;
                elseif numel(varargin{3}) == 1 %numPoints
                    color.start  = varargin{1};
                    color.middle = [];
                    color.end    = varargin{2};
                    color.num    = varargin{3};
                else
                    error(errorMessage)
                end
            else
                error(errorMessage)
            end
            
            if color.num <= 1
                error(errorMessage)
            end
            
            if isempty(color.middle) %no midPoint
                cMap = Colormaps.interpMap(color.start, color.end, color.num);
            else %midpointDefined
                [topN, botN] = Colormaps.sizePartialMaps(color.num);
                cMapTop = Colormaps.interpMap(color.start, color.middle, topN);
                cMapBot = Colormaps.interpMap(color.middle, color.end, botN);
                cMap = [cMapTop(1:end-1,:); cMapBot];
            end
        end
        
        function cMap = interpMap(colorStart, colorEnd, n)
            
            for i = 1:3
                cMap(1:n,i) = linspace(colorStart(i), colorEnd(i), n);
            end
        end
        
        function [topN, botN] = sizePartialMaps(n)
            n = n + 1;
            
            topN =  ceil(n/2);
            botN = floor(n/2);
        end
    end
end