function [issig, pvalue] = binotest(X, N, hypP, alpha)
if ~exist('hypP', 'var')
    hypP = .5;
end
if ~exist('alpha', 'var')
    alpha = 0.05;
end

side = N * hypP < X;
c = binocdf(X-side, N, hypP);
pvalue = side - side .* c + (1-side) .* c;
issig = pvalue > alpha;
