function y = flog2(x)
thresh = 1e-200;
t = (x > thresh);
if (min(min(t)) == 0)
   x = t.* x + (1-t) * thresh;
end
y = log2(x);