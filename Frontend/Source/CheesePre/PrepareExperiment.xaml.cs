﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CheeseOne
{
    /// <summary>
    /// Interaction logic for PrepareExperiment.xaml
    /// </summary>
    public partial class PrepareExperiment : Window
    {
        private String Filename;
        public PrepareExperiment(string filename)
        {
            InitializeComponent();
            Filename = filename;
            MainFrame.Navigate(new FindBackground(filename));
        }
    }
}
