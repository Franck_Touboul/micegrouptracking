﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO.Ports;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] Command = Encoding.ASCII.GetBytes("It works");//{0x00,0x01,0x88};

            SerialPort BlueToothConnection = new SerialPort();
            BlueToothConnection.BaudRate = (9600);

            BlueToothConnection.PortName = "COM6";
            BlueToothConnection.Open();
            if (BlueToothConnection.IsOpen)
            {

                BlueToothConnection.ErrorReceived += new SerialErrorReceivedEventHandler(BlueToothConnection_ErrorReceived);

                // Declare a 2 bytes vector to store the message length header
                Byte[] MessageLength = { 0x00, 0x00 };

                //set the LSB to the length of the message
                MessageLength[0] = (byte)Command.Length;


                //send the 2 bytes header
                BlueToothConnection.Write(MessageLength, 0, MessageLength.Length);

                // send the message itself

                System.Threading.Thread.Sleep(2000);
                BlueToothConnection.Write(Command, 0, Command.Length);

                Messages(5, ""); //This is the last thing that prints before it just waits for response.

                int length = BlueToothConnection.ReadByte();
                Messages(6, "");
                // retrieve the reply data
                for (int i = 0; i < length; i++)
                {
                    Messages(7, (BlueToothConnection.ReadByte().ToString()));
                }
            }
        }

        static void BlueToothConnection_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
        }

        static public void Messages(int idx, string str)
        {
        }
    }
}
