﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace SocialCamera
{
    static class Program
    {
        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                Exception ex = (Exception)e.ExceptionObject;

                MessageBox.Show("Whoops! Please contact the developers with the following"
                      + " information:\n\n" + ex.Message + ex.StackTrace,
                      "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                SocialCamera.MainForm.SendEmail("SocialCamera: Fatal Error", "Please contact the developers with the following"
                      + " information:\n\n" + ex.Message + ex.StackTrace);
            }
            finally
            {
                Application.Exit();
            }
        }

        public static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            DialogResult result = DialogResult.Abort;
            try
            {
                Exception ex = (Exception)e.Exception;

                result = MessageBox.Show("Encountered an unexpected error.\n\nDo you want to ignore it and try to continue?",
                  "Warning", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Stop);

                /*result = MessageBox.Show("Encountered an error, shall I continue?\n\nPlease contact the developers with the"
                  + " following information:\n\n" + e.Exception.Message + e.Exception.StackTrace,
                  "Warning", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Stop);*/
                SocialCamera.MainForm.SendEmail("SocialCamera: Warning", "Please contact the developers with the following"
                    + " information:\n\n" + ex.Message + ex.StackTrace);
            }
            finally
            {
                if (result == DialogResult.No)
                {
                    Application.Exit();
                }
            }
        }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm(args));
        }
    }
}
