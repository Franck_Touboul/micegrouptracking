﻿//#define OLDVERSION 
#define EURESYS

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Threading;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using Euresys.MultiCam;
using DirectShowLib;
using SocialCamera.Properties;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Xml.Linq;
using System.Threading.Tasks;
using GMFBridgeLib;

using mf = MediaFoundation;

using Tamir.SharpSsh;

namespace SocialCamera
{
    public partial class MainForm : Form, IMyVideoGrabber
    {
        //
        private const int WS_CHILD = 0x40000000;	// attributes for video window
        private const int WS_CLIPCHILDREN = 0x02000000;
        public const int WM_GRAPHNOTIFY = 0x8000 + 1;

        // prevent windows from entering screensaver mode
        private const int WM_SYSCOMMAND = 0x112;
        private const int SC_SCREENSAVE = 0xF140;
        private const int SC_MONITORPOWER = 0xF170;

        // Creation of an event for asynchronous call to paint function
        public delegate void PaintDelegate(Graphics g);
        public delegate void UpdateStatusBarDelegate(String text);

        // The object that will contain the acquired image
        private Bitmap image = null;

        // The Mutex object that will protect image objects during processing
        private static Mutex imageMutex = new Mutex();
        private static Mutex videoMutex = new Mutex(false, "SocialCameraVideoMutex");

        // shceduled recordings
        DateTime m_scheduledDay = DateTime.Now.Date;
        TimeSpan m_recordingFrequency = TimeSpan.FromDays(1);
        List<System.Windows.Forms.Timer> m_timerList = null;
        List<DateTime> m_dueDates = null;

#if BUGTRAPPER
        private static Mutex bugTrackingMutex;
        private static System.Diagnostics.Process bugTrackingProcess = new System.Diagnostics.Process();
#endif

        // The MultiCam object that controls the acquisition
        //private UInt32 channel;

        // The MultiCam object that contains the acquired buffer
        //private UInt32 currentSurface;

        //
        private IGraphBuilder Graph = null;
        private ICaptureGraphBuilder2 Builder = null;
        private IBaseFilter Source = null;
        private IAMDroppedFrames DroppedFrames = null;
        private IBaseFilter AVIMux = null;
        private IFileSinkFilter FileWriter;

        // VMR
        private IBaseFilter VMR = null;
        private enum VMRTypes { EVR, VMR9, VMR7 };
        private VMRTypes VMRType = VMRTypes.EVR;
        private mf.EVR.IMFVideoDisplayControl EVRDisplay = null;
        private static object EVRLock = new object();

        private IMediaControl Control = null;
        private IMediaEventEx mediaEventEx = null;
        private string CaptureFilename;
        private IPin PreviewPin;

        // bridge
        private GMFBridgeController Bridge;
        private IBaseFilter PreviewSink;
        private IGraphBuilder PreviewGraph;
        private IBaseFilter StreamSource;
        private ICaptureGraphBuilder2 PreviewCaptureGraph;
        private IMediaControl PreviewControl;

        // Grabber
        public bool EnableGrabber = false;
        private IBaseFilter GrabberFilter;
        private ISampleGrabber Grabber;
        private MyVideoGrabber BufferGrabber;
        public delegate void NewFrameHandler(object sender, NewFrameEventArgs e);
        public event NewFrameHandler OnNewFrame;
        public void HandleNewVideoFrame()
        {
            NewFrameEventArgs args = new NewFrameEventArgs(BufferGrabber);
            OnNewFrame(this, args);
        }

        private string uniqueName = GetUniqueKey(5);

        DsROTEntry rot = null;

        FileStream logFile = new FileStream("SocialCamera.log", FileMode.Append, FileAccess.Write, FileShare.ReadWrite);

        // Recovery filename (incase of an error)
        string recoverFromFilename = "";

        // Twitter notifications
#if TWITTER
        System.Threading.Timer TwitterTimer;
        LinqToTwitter.PinAuthorizer twitterAuth;
        int TwitterUpdateFrequency = (int)TimeSpan.FromMinutes(60).TotalMilliseconds;
#endif
        System.Timers.Timer MaintenanceTimer;

        //
        //private MC.CALLBACK multiCamCallback;
        //
        //private System.Timers.Timer eventTimer;
        //private System.Timers.Timer trackTimer;
        //System.Threading.Timer timer;

        //
        private int currentDay = 1;
        private Boolean isRecording = false;
        private Boolean isScheduled = false;
        private Boolean graphCreatedSuccessfully = true;
        private Boolean afterReboot = false;

        //private DateTime recordingRealStartTime;
        //private DateTime recordingDueStartTime;
        //private DateTime recordingDueEndTime;

        private uint m_previousExecutionState;

        private string progressLog = "";
        private MyDevice selectedDevice;
        private MyDevice currentDevice;

        private SSHFileTransfer fileTransfer;

        [DllImport("oleaut32.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
        public static extern int OleCreatePropertyFrame(
            IntPtr hwndOwner,
            int x,
            int y,
            [MarshalAs(UnmanagedType.LPWStr)] string lpszCaption,
            int cObjects,
            [MarshalAs(UnmanagedType.Interface, ArraySubType = UnmanagedType.IUnknown)] 
			ref object ppUnk,
            int cPages,
            IntPtr lpPageClsID,
            int lcid,
            int dwReserved,
            IntPtr lpvReserved);

        public MainForm(string[] args)
        {
            InitializeComponent();
            this.Text = "Social Camera (Version " + Settings.Default.Version + ")";

#if BUGTRAPPER 
            try
            {
                if (Settings.Default.ReportOnError)
                {
                    string mutexName = GetUniqueKey(10);
                    bugTrackingMutex = new Mutex(true, "SocialCameraBugTracking" + mutexName);
                    bugTrackingProcess.StartInfo.FileName = "MyBugTrapper.exe";
                    bugTrackingProcess.StartInfo.Arguments =
                            " /mutex=" + "SocialCameraBugTracking" + mutexName + 
                          " /account=" + Settings.Default.EmailAccount +
                        " /password=" + Settings.Default.EmailPassword +
                            " /from=" + Settings.Default.EmailFrom +
                              " /to=" + Settings.Default.EmailTo +
                            " /smtp=" + Settings.Default.EmailSMTP +
                            " /port=" + Settings.Default.EmailPort + 
                          " /useSSL=" + (Settings.Default.UseSSL ? "yes" : "no");
                    bugTrackingProcess.StartInfo.CreateNoWindow = true;
                    bugTrackingProcess.Start();
                }
            }
            catch
            {
            }
#endif
            // Set new state to prevent system sleep (note: still allows screen saver)


            m_previousExecutionState = NativeMethods.SetThreadExecutionState(
                NativeMethods.ES_CONTINUOUS | NativeMethods.ES_SYSTEM_REQUIRED | NativeMethods.ES_DISPLAY_REQUIRED);
            /*if (0 == m_previousExecutionState)
            {
                MessageBox.Show("Call to SetThreadExecutionState failed unexpectedly.",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                // No way to recover; fail gracefully
                Close();
            }*/

            if (args.Count() > 0)
                recoverFromFilename = args[0];
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.StartTimeDTP.Format = DateTimePickerFormat.Time;

#if BUGTRAPPER
#else
                emailCheck.Enabled = false;
#endif
                PopulateTypes();

                if (Settings.Default.Path == "")
                    PathTB.Text = System.IO.Directory.GetCurrentDirectory();
                else
                    PathTB.Text = Settings.Default.Path;
                NumberOfDaysUD.Value = Settings.Default.NumberOfRuns;
                StartTimeDTP.Value = Settings.Default.FilmingStartTime;
                EndTimeDTP.Value = Settings.Default.FilmingEndTime;
                
               
                //enumerate Video Input filters and add them to comboBox1
                foreach (MyDevice ds in MyDevice.GetDevices())
                {
                    CameraCB.Items.Add(ds);
                    if (ds.Path.Equals(Settings.Default.Camera))
                    {
                        this.CameraCB.Text = ds.Name;
                        selectedDevice = ds;
                    }
                }

                RecPanel.Parent = this.Controls.Owner;

                emailFrom.Text = Settings.Default.EmailFrom;
                emailTo.Text = Settings.Default.EmailTo;
                smtpServer.Text = Settings.Default.EmailSMTP;
                smtpPort.Text = Settings.Default.EmailPort;
                username.Text = Settings.Default.EmailAccount;
                password.Text = Settings.Default.EmailPassword;
                useSSL.Checked = Settings.Default.UseSSL;
                reportOnError.Checked = Settings.Default.ReportOnError;

                // SSH
                usernameTB.Text = Settings.Default.SSHUsername;
                passwordTB.Text = Settings.Default.SSHPassword;
                serverTB.Text = Settings.Default.SSHServer;
                remotePathTB.Text = Settings.Default.SSHPath;
                transferFilesCB.Checked = Settings.Default.SSHUse;

                usernameTB.Enabled = Settings.Default.SSHUse;
                passwordTB.Enabled = Settings.Default.SSHUse;
                serverTB.Enabled = Settings.Default.SSHUse;
                remotePathTB.Enabled = Settings.Default.SSHUse;

                // see if should try and recover previous session
                if (recoverFromFilename != "")
                    Recover(recoverFromFilename);


                MaintenanceTimer = new System.Timers.Timer(100);
                MaintenanceTimer.Elapsed += new System.Timers.ElapsedEventHandler(OnMaintenanceEvent);

                MaintenanceTimer.Enabled = true;

#if TWITTER
                // start twitter timer
                TimerCallback twitterTimerCallback = TimerUpdateTwitter;
                AutoResetEvent TwitterAutoEvent = new AutoResetEvent(false);
                TwitterTimer = new System.Threading.Timer(twitterTimerCallback, TwitterAutoEvent, int.MaxValue, Timeout.Infinite);
#endif
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Social Camera Exception");
                Close();
                // An exception has occurred in the try {...} block. 
                // Retrieve its description and display it in a message box.
            }
        }

        private void OnMaintenanceEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            if (DroppedFrames != null)
            {
                //int nCaptured;
                if (InvokeRequired)
                {
                    Invoke((MethodInvoker)delegate { OnMaintenanceEvent(source, e); });
                    return;
                }

                int nDropped;
                DroppedFrames.GetNumDropped(out nDropped);
                //DroppedFrames.GetNumNotDropped(out nCaptured);

                DroppedFrameStatusLabel.Text = String.Format("Dropped frames: {0}", nDropped);
                //MainStatusStrip.Items[0].Text = String.Format("{0}", nDropped);
            }
        }


        protected override void OnClosed(System.EventArgs e)
        {
#if BUGTRAPPER
            if (!bugTrackingProcess.HasExited)
                bugTrackingProcess.Kill();
#endif
            logFile.Close();
            logFile = null;
            base.OnClosed(e);

            // Restore previous state
            if (0 == NativeMethods.SetThreadExecutionState(m_previousExecutionState))
            {
                // No way to recover; already exiting
            }
        }

        /// <summary>
        /// Gets a unqiue key based on GUID string hash code.
        /// </summary>
        /// <param name="length">The length of the key returned.</param>
        /// <returns>Unique key returned.</returns>
        public static string GetUniqueKey(int length)
        {
            string guidResult = string.Empty;

            while (guidResult.Length < length)
            {
                // Get the GUID.
                guidResult += Guid.NewGuid().ToString().GetHashCode().ToString("x");
            }

            // Make sure length is valid.
            if (length <= 0 || length > guidResult.Length)
                throw new ArgumentException("Length must be between 1 and " + guidResult.Length);

            // Return the first length bytes.
            return guidResult.Substring(0, length);
        }

        private IBaseFilter CreateFilter(Guid category, MyDevice device)
        {
            try
            {
                LoggedMessage("creating filter...");
                object source = null;
                Guid iid = typeof(IBaseFilter).GUID;

                // 
#if EURESYS
                /*                try
                {
                    dlog("  testing picolo diligent");
                    // the strange code ahead solves a problem with the picolo dilegent card
                    // which doesn't allow you to change the encoding of the camera to PAL
                    source = (IBaseFilter)new ESFilter();
                    IESFProperties Prop = (IESFProperties)source;
                    //            if (Prop == null)
                    //              throw new Exception("Failed to QueryInterface IESFProperties");
                    for (int i = 0; i < 4; ++i)
                    {
                        try {
                            Prop.SetConnector(i);
                            Prop.SetVideoStandard(1, false);
                        }
                        catch (Exception) {
                            log("Picolo diligent channel " + i.ToString() + " failed");
                        }
                    }
                    source = null;
                }
                catch (Exception)
                {
                    log("Picolo diligent failed");
                }*/

                Match match = Regex.Match(device.Name, "Euresys PICOLO DILIGENT");
                if (match.Success)
                {
                    LoggedMessage("  biniding PICOLO DILIGENT filter");
                    source = (IBaseFilter)new ESFilter();
                    IESFProperties Prop = (IESFProperties)source;
                    //            if (Prop == null)
                    //              throw new Exception("Failed to QueryInterface IESFProperties");
                    Match indexMatch = Regex.Match(device.Name, "[0-9]*$");
                    Prop.SetConnector(int.Parse(indexMatch.Value) - 1);
                    Prop.SetVideoStandard(0, false);
                }
                else
#endif
                {
                    LoggedMessage("  biniding filter");
                    LoggedMessage("  device set: " + device.Name);
                    device.Moniker.BindToObject(null, null, ref iid, out source);
                }
                LoggedMessage("  returning filter");
                return (IBaseFilter)source;
            }
            catch (Exception)
            {
            }
            return null;
        }

        private void CreateGraph()
        {
            InitializeGraph();
            CreatePreviewGraph();
            StartPreview();
        }

        private void GetDeviceList()
        {
        }


        private void DisplayPropertyPage(IBaseFilter dev)
        {
            //Get the ISpecifyPropertyPages for the filter
            ISpecifyPropertyPages pProp = dev as ISpecifyPropertyPages;
            int hr = 0;

            if (pProp == null)
            {
                //If the filter doesn't implement ISpecifyPropertyPages, try displaying IAMVfwCompressDialogs instead!
                IAMVfwCompressDialogs compressDialog = dev as IAMVfwCompressDialogs;
                if (compressDialog != null)
                {

                    hr = compressDialog.ShowDialog(VfwCompressDialogs.Config, IntPtr.Zero);
                    DsError.ThrowExceptionForHR(hr);
                }
                return;
            }

            //Get the name of the filter from the FilterInfo struct
            FilterInfo filterInfo;
            hr = dev.QueryFilterInfo(out filterInfo);
            DsError.ThrowExceptionForHR(hr);

            // Get the propertypages from the property bag
            DsCAUUID caGUID;
            hr = pProp.GetPages(out caGUID);
            DsError.ThrowExceptionForHR(hr);

            // Create and display the OlePropertyFrame
            object oDevice = (object)dev;
            hr = OleCreatePropertyFrame(this.Handle, 0, 0, filterInfo.achName, 1, ref oDevice, caGUID.cElems, caGUID.pElems, 0, 0, IntPtr.Zero);
            DsError.ThrowExceptionForHR(hr);

            // Release COM objects
            Marshal.FreeCoTaskMem(caGUID.pElems);
            Marshal.ReleaseComObject(pProp);
            if (filterInfo.pGraph != null)
            {
                Marshal.ReleaseComObject(filterInfo.pGraph);
            }
        }

        private void ShowPins(IBaseFilter filter)
        {
            int hr;
            IEnumPins enumPins;
            IPin[] pins = new IPin[1];
            IntPtr fetched = IntPtr.Zero;

            hr = filter.EnumPins(out enumPins);
            DsError.ThrowExceptionForHR(hr);
            StatusMessage("Querying pins: ");
            try
            {
                while (enumPins.Next(pins.Length, pins, fetched) == 0)
                {
                    try
                    {
                        string pinid;
                        PinDirection pindir;
                        IPin pinout;
                        PinInfo pinfo;
                        pins[0].QueryId(out pinid);
                        pins[0].QueryDirection(out pindir);
                        pins[0].ConnectedTo(out pinout);
                        pins[0].QueryPinInfo(out pinfo);
                        if (pinout == null)
                            StatusMessage(pinid + ", name: " + pinfo.name + ", dir: " + pindir.ToString() + ", NOT CONNECTED!");
                        else
                        {
                            PinInfo pinfo2;
                            pinout.QueryPinInfo(out pinfo2);
                            StatusMessage(pinid + ", name: " + pinfo.name + ", dir: " + pindir.ToString() + ", connected to: " + pinfo2.name);
                        }
                    }
                    finally
                    {
                    }
                }
            }
            finally
            {
                Marshal.ReleaseComObject(enumPins);
            }

        }

        void RemoveDownstream(IBaseFilter filter)
        {
            if (filter == null)
                return;

#if DEBUG

            FilterInfo filterInfo;
            filter.QueryFilterInfo(out filterInfo);
            LoggedMessage("removing filter " + filterInfo.ToString());
#endif

            IEnumPins enumPins;
            IPin[] pins = new IPin[1];
            IntPtr fetched = IntPtr.Zero;

            int hr = filter.EnumPins(out enumPins);
            if (hr == 0)
            {
                while (enumPins.Next(pins.Length, pins, fetched) == 0)
                {
                    IPin ppPin;
                    pins[0].ConnectedTo(out ppPin);
                    if (ppPin != null)
                    {
                        PinInfo pInfo;
                        hr = ppPin.QueryPinInfo(out pInfo);
                        if (hr == 0)
                        {
                            if (pInfo.dir == PinDirection.Input)
                            {
                                RemoveDownstream(pInfo.filter);
                                Graph.Disconnect(ppPin);
                                Graph.Disconnect(pins[0]);
                                Graph.RemoveFilter(pInfo.filter);
                            }
                            pInfo.filter = null;
                        }
                        ppPin = null;
                    }
                    pins[0] = null;
                }
            }

        }

        private void TearDownGraph()
        {
            if (Control != null)
                Control.Stop();
            StopPreview();

            FileWriter = null;
            AVIMux = null;
            mediaEventEx = null;
            DroppedFrames = null;

            if (VMR != null)
            {
                int hr = 0;
                
                /*IVMRWindowlessControl9 windowlessControl = VMR as IVMRWindowlessControl9;
                if (windowlessControl != null)
                {
                    hr = windowlessControl.SetVideoClippingWindow(IntPtr.Zero);
                    TestForError("Error!", hr);
                }*/
                VMR = null;
                
            }

            RemoveDownstream(Source);
        }

        public void StopPreview()
        {
            if (Bridge != null)
            {
                Bridge.BridgeGraphs(null, null);
                if (PreviewControl != null)
                    PreviewControl.Stop();
                VMR = null;
                PreviewControl = null;
                PreviewGraph = null;
                StreamSource = null;
            }
        }

        public void StartPreview()
        {
            if (Bridge == null)
                return;

            int hr;
            try
            {
                PreviewGraph = (IGraphBuilder)new FilterGraph();
                StreamSource = Bridge.InsertSourceFilter(PreviewSink, PreviewGraph) as IBaseFilter;

                PreviewCaptureGraph = (ICaptureGraphBuilder2)new CaptureGraphBuilder2();
                hr = PreviewCaptureGraph.SetFiltergraph(PreviewGraph);
                TestForError("Error setting capture graph", hr);

                LoggedMessage("  rendering preview stream");
                if (VMRType == VMRTypes.EVR)
                    VMR = (IBaseFilter)new mf.EVR.EnhancedVideoRenderer();
                else 
                    VMR = (IBaseFilter)new VideoMixingRenderer9();
                LoggedMessage("  setting video mixing");
                hr = PreviewGraph.AddFilter(VMR, "Renderer");
                TestForError("Error setting preview graph", hr);

                ConfigureWindowlessMode();

                hr = PreviewCaptureGraph.RenderStream(null, MediaType.Video, StreamSource, null, VMR);
                TestForError("Source stream error", hr);

                PreviewControl = (IMediaControl)PreviewGraph;
                hr = PreviewControl.Run();
                TestForError("Error starting color source graph", hr);

                Bridge.BridgeGraphs(PreviewSink, StreamSource);

            }
            catch (Exception e)
            {
                Error("Error", e.Message);
            }

        }

        private void AddPreviewSubGraph()
        {
            int hr;
            // Bridge
            Bridge = new GMFBridgeController();
            Bridge.AddStream(1, eFormatType.eMuxInputs, 1);
            PreviewSink = (IBaseFilter)Bridge.InsertSinkFilter(Graph);

            GrabberFilter = null;
            hr = Builder.RenderStream(PinCategory.Preview, MediaType.Video, Source, GrabberFilter, PreviewSink);

            // set preview framerate
            object StreamConfigObj;
            IAMStreamConfig StreamConfig;
            Builder.FindInterface(PinCategory.Capture, MediaType.Video, Source, typeof(IAMStreamConfig).GUID, out StreamConfigObj);
            StreamConfig = StreamConfigObj as IAMStreamConfig;
            AMMediaType mediaType;
            StreamConfig.GetFormat(out mediaType);

            if (mediaType.formatType == DirectShowLib.FormatType.VideoInfo || mediaType.formatType == DirectShowLib.FormatType.MpegVideo)
            {
                VideoInfoHeader infoHeader = (VideoInfoHeader)Marshal.PtrToStructure(mediaType.formatPtr, typeof(VideoInfoHeader));
                infoHeader.AvgTimePerFrame = (10000000 / 15);
                Marshal.StructureToPtr(infoHeader, mediaType.formatPtr, false);
                StreamConfig.SetFormat(mediaType);
            }

        }

        public void CreateCaptureGraph(string filename)
        {
            int hr;

            try
            {
                Control.Stop();
                TearDownGraph();

                Guid guid = DirectShowLib.MediaSubType.Avi;
                hr = Builder.SetOutputFileName(guid, filename, out AVIMux, out FileWriter);
                TestForError("Error setting capture graph", hr);

                // Now tell the AVIMUX to write out AVI files that old apps can read properly.
                // If we don't, most apps won't be able to tell where the keyframes are,
                // slowing down editing considerably
                // Doing this will cause one seek (over the area the index will go) when
                // you capture past 1 Gig, but that's no big deal.
                // NOTE: This is on by default, so it's not necessary to turn it on

                IConfigAviMux configAviMux = AVIMux as IConfigAviMux;
                if (configAviMux != null)
                    configAviMux.SetOutputCompatibilityIndex(false);

                hr = Builder.RenderStream(PinCategory.Capture, MediaType.Video, Source, null, AVIMux);
                TestForError("Encountered stream error", hr);

                AddPreviewSubGraph();

                Control = (IMediaControl)this.Graph;
                this.mediaEventEx = (IMediaEventEx)this.Graph;
                hr = this.mediaEventEx.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                TestForError("Error setting notification window", hr);

                LoggedMessage("  graph done");
                graphCreatedSuccessfully = true;
                afterReboot = false;

                DroppedFrames = Source as IAMDroppedFrames;


               /* Guid guid = DirectShowLib.MediaSubType.Avi;
                SourceGraph = (IGraphBuilder)new FilterGraph();
                StreamSource = BridgeController.InsertSourceFilter(StreamSink, SourceGraph) as IBaseFilter;

                DirectShowLib.SBE.IStreamBufferSink

                CaptureGraph = (ICaptureGraphBuilder2)new CaptureGraphBuilder2();
                hr = CaptureGraph.SetFiltergraph(SourceGraph);

                TestForError("Error setting capture graph", hr);

                IBaseFilter AviMux;
                IFileSinkFilter outFileWriter;

                hr = CaptureGraph.SetOutputFileName(guid, filename, out AviMux, out outFileWriter);
                TestForError("Unable to set output file", hr);

                hr = CaptureGraph.RenderStream(null, MediaType.Video, StreamSource, null, AviMux);
                TestForError("Encountered stream error", hr);

                CaptureEventEx = (IMediaEventEx)SourceGraph;
                hr = CaptureEventEx.SetNotifyWindow(Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                TestForError("", hr);

                SourceMediaControl = (IMediaControl)SourceGraph;
                hr = SourceMediaControl.Run();
                TestForError("Error starting color source graph", hr);

                BridgeController.BridgeGraphs(StreamSink, StreamSource);*/
            }
            catch (Exception e)
            {
                Error("Error", e.Message);
            }
        }

        int VFW_S_NOPREVIEWPIN = 0x0004027E;
        private void InitializeGraph()
        {
            currentDevice = null;
            CloseInterfaces();
            int hr;
            Graph = (IGraphBuilder)new FilterGraph();

            Source = CreateFilter(FilterCategory.VideoInputDevice, selectedDevice);
            currentDevice = selectedDevice;
            hr = Graph.AddFilter(this.Source, "ESP");
            TestForError("Error adding source filter", hr);
            LoggedMessage("  filter set");

            Builder = (ICaptureGraphBuilder2)new CaptureGraphBuilder2();
            LoggedMessage("  controls set");

            hr = Builder.SetFiltergraph(Graph);
            TestForError("Error setting preview graph", hr);

        }

        private void CreatePreviewGraph()
        {
            int hr;
            try
            {
                if (Graph == null || !selectedDevice.Equals(currentDevice))
                    InitializeGraph();
                else
                    TearDownGraph();

                LoggedMessage("creating graph...");

                AddPreviewSubGraph();

                Control = (IMediaControl)this.Graph;
                this.mediaEventEx = (IMediaEventEx)this.Graph;
                hr = this.mediaEventEx.SetNotifyWindow(this.Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                TestForError("Error setting notification window", hr);

                LoggedMessage("  graph done");
                graphCreatedSuccessfully = true;
                afterReboot = false;

                //ShowPins(AVIMux);
            }
            catch (Exception ex)
            {
                CloseInterfaces();
                StatusMessage("Unable to open capture device '" + selectedDevice + "': " + ex.Message);
            }
        }

        private bool InitSampleGrabber()
        {
            int hr = 0;

            // Get SampleGrabber
            Grabber = new SampleGrabber() as ISampleGrabber;

            if (Grabber == null)
                return false;

            GrabberFilter = Grabber as IBaseFilter;
            if (GrabberFilter == null)
            {
                Marshal.ReleaseComObject(Grabber);
                Grabber = null;
            }
            AMMediaType media = new AMMediaType();

            media.majorType = MediaType.Video;
            media.subType = MediaSubType.RGB24;
            media.formatPtr = IntPtr.Zero;
            hr = Grabber.SetMediaType(media);
            if (hr < 0)
                Marshal.ThrowExceptionForHR(hr);
            hr = Graph.AddFilter(GrabberFilter, "SampleGrabber");

            if (hr < 0)
                Marshal.ThrowExceptionForHR(hr);
            hr = Grabber.SetBufferSamples(true);
            if (hr == 0)
                hr = Grabber.SetOneShot(false);
            if (hr == 0)
                hr = Grabber.SetCallback(null, 0);
            if (hr < 0)
                Marshal.ThrowExceptionForHR(hr);
            return true;
        }

        private VideoInfoHeader GetInfoHeader()
        {
            IPin outputPin = PreviewPin;
            IAMStreamConfig StreamConfig = outputPin as IAMStreamConfig;
            AMMediaType mediaType;
            StreamConfig.GetFormat(out mediaType);

            if (mediaType.formatType == DirectShowLib.FormatType.VideoInfo || mediaType.formatType == DirectShowLib.FormatType.MpegVideo)
            {
                VideoInfoHeader infoHeader = (VideoInfoHeader)Marshal.PtrToStructure(mediaType.formatPtr, typeof(VideoInfoHeader));
                return infoHeader;
            }
            return null;
        }


        private void SetMediaSampleGrabber()
        {
            if (GrabberFilter != null)
            {
                AMMediaType media = new AMMediaType();
                int hr;

                hr = Grabber.GetConnectedMediaType(media);
                if (hr < 0)
                {
                    Marshal.ThrowExceptionForHR(hr);
                }

                if ((media.formatType != FormatType.VideoInfo) || (media.formatPtr ==
                    IntPtr.Zero))
                {
                    throw new NotSupportedException(
                        "Unknown Grabber Media Format");
                }

                Marshal.FreeCoTaskMem(media.formatPtr);
                media.formatPtr = IntPtr.Zero;

                BufferGrabber = new MyVideoGrabber(this, GetInfoHeader());
                Grabber.SetCallback(BufferGrabber, 1);
            }
        }

        public void Restart()
        {
            try
            {
                string uid = GetUniqueKey(15);

                XDocument doc = new XDocument(
                         new XDeclaration("1.0", "utf-8", "yes"),
                         new XElement("Experiment",
                             new XElement("Scheduled", isScheduled),
                             new XElement("ScheduledDay", m_scheduledDay.ToString()),
                             new XElement("Device", selectedDevice),
                             new XElement("GroupType", GroupTypeTB.Text),
                             new XElement("GroupNumber", GroupNumberUD.Value),
                             new XElement("StartTime", StartTimeDTP.Value.ToString("HH:mm:ss")),
                             new XElement("EndTime", EndTimeDTP.Value.ToString("HH:mm:ss")),
                             new XElement("CurrentDay", currentDay),
                             new XElement("TotalNumberOfDays", NumberOfDaysUD.Value),
                             new XElement("Path", PathTB.Text)
                            ));

                string recoveryFilename = /*System.IO.Path.GetTempPath() +*/ uid + ".xml";
                doc.Save(recoveryFilename);

                CloseInterfaces();

                System.Diagnostics.Process newInstance = new System.Diagnostics.Process();
                newInstance.StartInfo.FileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
                newInstance.StartInfo.Arguments = recoveryFilename;
                newInstance.StartInfo.CreateNoWindow = true;
                newInstance.Start();

                Application.Exit();
            }
            catch
            {
                MessageBox.Show("Failed to restart program");
            }
        }

        public void Recover(string filename)
        {
            try
            {
                XDocument doc = XDocument.Load(filename);
                var data = from item in doc.Descendants("Experiment")
                           select new
                               {
                                   Scheduled = item.Element("Scheduled").Value,
                                   ScheduledDay = item.Element("ScheduledDay").Value,
                                   Device = item.Element("Device").Value,
                                   GroupType = item.Element("GroupType").Value,
                                   GroupNumber = item.Element("GroupNumber").Value,
                                   StartTime = item.Element("StartTime").Value,
                                   EndTime = item.Element("EndTime").Value,
                                   CurrentDay = item.Element("CurrentDay").Value,
                                   TotalNumberOfDays = item.Element("TotalNumberOfDays").Value,
                                   Path = item.Element("Path").Value
                               };

                foreach (var parameters in data)
                {
                    m_scheduledDay = DateTime.Parse(parameters.ScheduledDay);
                    CameraCB.SelectedIndex = CameraCB.FindString(parameters.Device);
                    GroupTypeTB.Text = parameters.GroupType;
                    GroupNumberUD.Value = int.Parse(parameters.GroupNumber);
                    StartTimeDTP.Value = DateTime.Parse(parameters.StartTime);
                    EndTimeDTP.Value = DateTime.Parse(parameters.EndTime);
                    currentDayCtrl.Value = int.Parse(parameters.CurrentDay);
                    NumberOfDaysUD.Value = int.Parse(parameters.TotalNumberOfDays);
                    PathTB.Text = parameters.Path;
                    if (bool.Parse(parameters.Scheduled))
                        StartBtn_Click(null, null);
                    break;
                }
                afterReboot = true;
            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to recover previous session: " + e.Message);
            }
        }

        public void CloseInterfaces()
        {
            // Stop previewing data
            if (Control != null)
            {
                try
                {
                    Control.Stop();
                    LoggedMessage("stopping control");
                }
                catch (Exception e)
                {
                    MessageBox.Show("Unable to recover previous session: " + e.Message);
                }
            }

            // Stop receiving events
            if (this.mediaEventEx != null)
                this.mediaEventEx.SetNotifyWindow(IntPtr.Zero, WM_GRAPHNOTIFY, IntPtr.Zero);

            // Relinquish ownership (IMPORTANT!) of the video window.
            // Failing to call put_Owner can lead to assert failures within
            // the video renderer, as it still assumes that it has a valid
            // parent window.

            // Remove filter graph from the running object table
            if (rot != null)
            {
                rot.Dispose();
                rot = null;
            }

            // Release DirectShow interfaces
            //Marshal.ReleaseComObject(this.Control); 
            this.Control = null;
            this.mediaEventEx = null;
            //Marshal.ReleaseComObject(this.videoWin); 
            //Marshal.ReleaseComObject(this.Graph); 
            this.Graph = null;
            //Marshal.ReleaseComObject(this.Builder); 
            this.Builder = null;
            this.Source = null;
        }

        public void HandleGraphEvent()
        {
            int hr = 0;
            EventCode evCode;
            IntPtr evParam1, evParam2;

            if (this.mediaEventEx == null)
                return;

            while (this.mediaEventEx.GetEvent(out evCode, out evParam1, out evParam2, 0) == 0)
            {

                LoggedMessage("event: " + evCode.ToString() + " with: " + evParam1.ToString() + ", " + evParam2.ToString());
                if (evCode == DirectShowLib.EventCode.OleEvent)
                {
                    LoggedMessage("params: '" + Marshal.PtrToStringBSTR((IntPtr)evParam1) + ", '" + Marshal.PtrToStringBSTR((IntPtr)evParam2) + "'");
                }
                // Free event parameters to prevent memory leaks associated with
                // event parameter data.  While this application is not interested
                // in the received events, applications should always process them.
                hr = this.mediaEventEx.FreeEventParams(evCode, evParam1, evParam2);
                DsError.ThrowExceptionForHR(hr);

                // Insert event processing code here, if desired
            }
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_GRAPHNOTIFY:
                    {
                        HandleGraphEvent();
                        break;
                    }
            }

            // Pass this message to the video window for notification of system changes

            base.WndProc(ref m);
        }

        /*        private void MultiCamCallback(ref MC.SIGNALINFO signalInfo)
                {
                    switch (signalInfo.Signal)
                    {
                        case MC.SIG_SURFACE_PROCESSING:
                            ProcessingCallback(signalInfo);
                            break;
                        case MC.SIG_ACQUISITION_FAILURE:
                            AcqFailureCallback(signalInfo);
                            break;
                        default:
                            throw new Euresys.MultiCamException("Unknown signal");
                    }
                }
                */
        /*private void ProcessingCallback(MC.SIGNALINFO signalInfo)
        {
            UInt32 currentChannel = (UInt32)signalInfo.Context;

            StatusBar.Text = "Processing";
            currentSurface = signalInfo.SignalInfo;

            // + PicoloVideo Sample Program

            try
            {
                // Update the image with the acquired image buffer data 
                Int32 width, height, bufferPitch;
                IntPtr bufferAddress;
                MC.GetParam(currentChannel, "ImageSizeX", out width);
                MC.GetParam(currentChannel, "ImageSizeY", out height);
                MC.GetParam(currentChannel, "BufferPitch", out bufferPitch);
                MC.GetParam(currentSurface, "SurfaceAddr", out bufferAddress);

                try
                {
                    imageMutex.WaitOne();

                    image = new Bitmap(width, height, bufferPitch, PixelFormat.Format24bppRgb, bufferAddress);

                    /* Insert image analysis and processing code here */
        /*}
        finally
        {
            imageMutex.ReleaseMutex();
        }

        // Retrieve the frame rate
        Double frameRate_Hz;
        MC.GetParam(channel, "PerSecond_Fr", out frameRate_Hz);

        // Retrieve the channel state
        String channelState;
        MC.GetParam(channel, "ChannelState", out channelState);

        // Display frame rate and channel state
        StatusBar.Text = String.Format("Frame Rate: {0:f2}, Channel State: {1}", frameRate_Hz, channelState);

        // Display the new image
        this.BeginInvoke(new PaintDelegate(Redraw), new object[1] { CreateGraphics() });
    }
    catch (Euresys.MultiCamException exc)
    {
        MessageBox.Show(exc.Message, "MultiCam Exception");
    }
    catch (System.Exception exc)
    {
        MessageBox.Show(exc.Message, "System Exception");
    }
}

private void AcqFailureCallback(MC.SIGNALINFO signalInfo)
{
    UInt32 currentChannel = (UInt32)signalInfo.Context;

    // + PicoloVideo Sample Program

    try
    {
        // Display frame rate and channel state
        StatusBar.Text = String.Format("Acquisition Failure, Channel State: IDLE");
        this.BeginInvoke(new PaintDelegate(Redraw), new object[1] { CreateGraphics() });
    }
    catch (System.Exception exc)
    {
        MessageBox.Show(exc.Message, "System Exception");
    }

    // - PicoloVideo Sample Program
}
*/

        private void UpdateStatusBar(String text)
        {
            StatusBar.Text = text;
        }

        void Redraw(Graphics g)
        {
            // + PicoloVideo Sample Program

            try
            {
                imageMutex.WaitOne();

                if (image != null)
                    g.DrawImage(image, 0, 0);
                UpdateStatusBar(StatusBar.Text);
            }
            catch (System.Exception exc)
            {
                MessageBox.Show(exc.Message, "System Exception");
            }
            finally
            {
                imageMutex.ReleaseMutex();
            }

            // - PicoloVideo Sample Program
        }

        public void Error(string title, string msg)
        {
            System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackFrame(1, true);

            string method = stackFrame.GetMethod().ToString();
            int line = stackFrame.GetFileLineNumber();
            //MessageBox.Show(str + "[" + method + "; " + line + "]" + Environment.NewLine + e.Message);
            MessageBox.Show(Environment.NewLine + msg + Environment.NewLine + Environment.NewLine + "[" + method + "; " + line + "]", title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        public void TestForError(string str, int hr)
        {
            try
            {
                DsError.ThrowExceptionForHR(hr);
            }
            catch (Exception e)
            {
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackFrame(1, true);

                string method = stackFrame.GetMethod().ToString();
                int line = stackFrame.GetFileLineNumber();
                //MessageBox.Show(str + "[" + method + "; " + line + "]" + Environment.NewLine + e.Message);
                MessageBox.Show(Environment.NewLine + e.Message + Environment.NewLine + Environment.NewLine + "[" + method + "; " + line + "]", str, MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw (e);
            }
        }

        private IntPtr userId = new IntPtr(unchecked((int)0xACDCACDC));
/*        public void ConfigureWindowlessMode()
        {
            int hr = 0;
            IVMRFilterConfig9 filterConfig = VMR as IVMRFilterConfig9;

            if (filterConfig != null)
            {
                hr = filterConfig.SetRenderingMode(VMR9Mode.Renderless);
                TestForError("Error!", hr);

                // With 2 input stream
                hr = filterConfig.SetNumberOfStreams(1);
                TestForError("Error!", hr);
            }

            IVMRSurfaceAllocatorNotify9 vmrSurfAllocNotify = (IVMRSurfaceAllocatorNotify9)VMR;

            try
            {
                VMRAllocator = new TempVMR(Scene.OutputDevice);
                VMRAllocator.OnSurfaceReady += new TempVMR.SurfaceReadyHandler(Scene.OnSurfaceReady);
                VMRAllocator.OnLostDevice += new TempVMR.LostDeviceHandler(this.LostDevice);

                hr = vmrSurfAllocNotify.AdviseSurfaceAllocator(userId, VMRAllocator);
                DsError.ThrowExceptionForHR(hr);

                hr = VMRAllocator.AdviseNotify(vmrSurfAllocNotify);
                DsError.ThrowExceptionForHR(hr);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Social Camera Exception");
                VMRAllocator = null;
                throw;
            }

        }*/

        private void LostDevice()
        {
            if (this.InvokeRequired)
            {
                Invoke((MethodInvoker)delegate { LostDevice(); });
                return;
            }
            StopPreview();
            StartPreview();
        }

        /*private void ConfigureWindowlessModeOld()
        {
            int hr = 0;
            IVMRFilterConfig9 filterConfig = VMR as IVMRFilterConfig9;

            // Configure VMR-9 in Windowsless mode
            hr = filterConfig.SetRenderingMode(VMR9Mode.Windowless);
            TestForError("Error!", hr);

            // With 2 input stream
            hr = filterConfig.SetNumberOfStreams(1);
            TestForError("Error!", hr);

            IVMRWindowlessControl9 windowlessControl = VMR as IVMRWindowlessControl9;
            if (windowlessControl != null)
            {
                // Keep the aspect-ratio OK
                hr = windowlessControl.SetAspectRatioMode(VMR9AspectRatioMode.LetterBox);
                TestForError("Error!", hr);

                hr = windowlessControl.SetBorderColor(56 << 16 | 56 << 8 | 56);
                TestForError("Error!", hr);


                IntPtr handle = videoPanel.Handle;
                hr = windowlessControl.SetVideoClippingWindow(handle);
                TestForError("Error!", hr);
            }
            // Init the VMR-9 with default size values
            ResizeVideoWindow();
        }

        private void ResizeVideoWindow()
        {
            
            /*if (VMR != null)
            {
                // The main form is hosting the VMR-9
                int hr = 0;
                IVMRWindowlessControl9 windowlessControl = VMR as IVMRWindowlessControl9;

                if (windowlessControl != null)
                {
                    //System.Windows.Point offset = this.TransformToAncestor(Application.Current.MainWindow).Transform(new System.Windows.Point(VideoRegion.Margin.Left, VideoRegion.Margin.Top));
                    Rectangle rc = videoPanel.ClientRectangle;
                    hr = windowlessControl.SetVideoPosition(null, new DsRect(0, 0, rc.Right, rc.Bottom));
                }
            }*/
        //}

        private void ConfigureWindowlessMode()
        {
            if (VMRType == VMRTypes.EVR)
            {
                mf.EVR.IMFVideoDisplayControl pDisplay;

                mf.IMFGetService pGetService = null;
                pGetService = (mf.IMFGetService)VMR;
                object serviceObj;
                pGetService.GetService(mf.MFServices.MR_VIDEO_RENDER_SERVICE, typeof(mf.EVR.IMFVideoDisplayControl).GUID, out serviceObj);
                try
                {
                    pDisplay = (mf.EVR.IMFVideoDisplayControl)serviceObj;
                }
                catch
                {
                    Marshal.ReleaseComObject(serviceObj);
                    throw;
                }
                try
                {
                    lock (EVRLock)
                    {
                        // Set the number of streams.
                        pDisplay.SetVideoWindow(videoPanel.Handle);

                        // Set the display position to the entire window.
                        Rectangle r = videoPanel.ClientRectangle;
                        mf.Misc.MFRect rc = new mf.Misc.MFRect(r.Left, r.Top, r.Right, r.Bottom);

                        pDisplay.SetVideoPosition(null, rc);

                        EVRDisplay = pDisplay;
                    }
                    // Return the IMFVideoDisplayControl pointer to the caller.
                    //ppDisplay = pDisplay;
                    //m_pDisplay
                }
                finally
                {
                    //Marshal.ReleaseComObject(pDisplay);
                }
                //m_pMixer = null;

            }
            else
            {

                int hr = 0;
                IVMRFilterConfig9 filterConfig = VMR as IVMRFilterConfig9;

                // Configure VMR-9 in Windowsless mode
                hr = filterConfig.SetRenderingMode(VMR9Mode.Windowless);
                TestForError("Error!", hr);

                // With 2 input stream
                hr = filterConfig.SetNumberOfStreams(1);
                TestForError("Error!", hr);

                IVMRWindowlessControl9 windowlessControl = VMR as IVMRWindowlessControl9;
                if (windowlessControl != null)
                {
                    // Keep the aspect-ratio OK
                    hr = windowlessControl.SetAspectRatioMode(VMR9AspectRatioMode.LetterBox);
                    TestForError("Error!", hr);

                    hr = windowlessControl.SetBorderColor(56 << 16 | 56 << 8 | 56);
                    TestForError("Error!", hr);


                    IntPtr handle = videoPanel.Handle;
                    hr = windowlessControl.SetVideoClippingWindow(handle);
                    TestForError("Error!", hr);
                }
                // Init the VMR-9 with default size values
            }
            ResizeVideoWindow();
        }

        private void ResizeVideoWindow()
        {
            if (VMR != null)
            {
                int hr = 0;
                if (VMRType == VMRTypes.VMR9)
                {
                    // The main form is hosting the VMR-9
                    IVMRWindowlessControl9 windowlessControl = VMR as IVMRWindowlessControl9;

                    if (windowlessControl != null)
                    {
                        //System.Windows.Point offset = this.TransformToAncestor(Application.Current.MainWindow).Transform(new System.Windows.Point(VideoRegion.Margin.Left, VideoRegion.Margin.Top));
                        Rectangle rc = videoPanel.ClientRectangle;
                        hr = windowlessControl.SetVideoPosition(null, new DsRect(0, 0, rc.Right, rc.Bottom));
                    }
                }
                else if (VMRType == VMRTypes.EVR)
                {
                    lock (EVRLock)
                    {
                        Rectangle r = videoPanel.ClientRectangle;
                        mf.Misc.MFRect src = new mf.Misc.MFRect(r.Left, r.Top, r.Right, r.Bottom);
                        mf.Misc.MFRect dst = new mf.Misc.MFRect();
                        EVRDisplay.GetVideoPosition(null, dst);

                        if (dst != src)
                            EVRDisplay.SetVideoPosition(null, src);
                    }
                }
            }
        }


        void tryToSetupVideoWindow()
        {
        }

        private void MakeNewCaptureFile()
        {
            Match indexMatch = Regex.Match(currentDevice.Name, "[0-9]*$");
            int cameraIndex = 0;
            if (indexMatch.Length > 0)
                cameraIndex = int.Parse(indexMatch.Value) - 1;
            string sBaseName = String.Format(this.GroupTypeTB.Text + ".exp{0:D4}.day{1:D2}.cam{2:D2}",
                System.Decimal.ToInt32(this.GroupNumberUD.Value),
                currentDay,
                cameraIndex + 1);

            // Find an unused file name
            int indexFile = 0;
            string sFileName = string.Format(@"{0}\{1}.avi", folderBrowserDialog1.SelectedPath, sBaseName);
            while (File.Exists(sFileName))
            {
                sFileName = string.Format(@"{0}\{1}.{2}.avi", folderBrowserDialog1.SelectedPath, sBaseName, ++indexFile);
            }
            CaptureFilename = sFileName;
        }

        private void WriteMetaData()
        {
            XDocument doc = new XDocument(
                     new XDeclaration("1.0", "utf-8", "yes"),
                     new XElement("Experiment",
                         new XElement("Start", DateTime.Now),
                         new XElement("Remarks", RemarksTB.Text)
                        ));
            doc.Save(CaptureFilename + ".xml");
        }

        // This is the method to run when the timer is raised.
        private void TimerStopCapture(Object myObject, EventArgs myEventArgs)
        {
            System.Windows.Forms.Timer timer = myObject as System.Windows.Forms.Timer;
            if (timer != null)
                timer.Stop();
            StopRecording();

            foreach (DateTime nextEvent in m_dueDates)
            {
                if (nextEvent.CompareTo(DateTime.Now) >= 0)
                {
                    StatusMessage("next event is scheduled to " + nextEvent.ToString());
                    return;
                }
            }
            StatusMessage("no more scheduled events");
            isScheduled = false;
            StartBtn.Text = "Start";
            ToggleOptionsControl(true);
        }

        // This is the method to run when the timer is raised.
        private void TimerStartCapture(Object myObject, EventArgs myEventArgs)
        {
            ((System.Windows.Forms.Timer)myObject).Stop();
            currentDayCtrl.Value = DateTime.Now.Date.Subtract(m_scheduledDay).Days + 1;
            StartRecording();
        }

        private void ScheduleCapture()
        {
            if (StartTimeDTP.Value.TimeOfDay.TotalSeconds > EndTimeDTP.Value.TimeOfDay.TotalSeconds)
            {
                MessageBox.Show("Capturing start time comes after end time", "GMFPreview capture", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            m_timerList = new List<System.Windows.Forms.Timer>();
            m_dueDates = new List<DateTime>();
            bool firstEvent = true;
            for (int i = 0; i < Decimal.ToInt32(NumberOfDaysUD.Value - currentDayCtrl.Value) + 1; ++i)
            {
                DateTime startTime = DateTime.Now.Date;
                DateTime endTime = DateTime.Now.Date;
                for (int k = 0; k < i; ++k)
                {
                    startTime = startTime.Add(m_recordingFrequency);
                    endTime = endTime.Add(m_recordingFrequency);
                }
                startTime = startTime.Add(StartTimeDTP.Value.TimeOfDay);
                endTime = endTime.Add(EndTimeDTP.Value.TimeOfDay);

                System.Windows.Forms.Timer startCapTimer = new System.Windows.Forms.Timer();
                System.Windows.Forms.Timer endCapTimer = new System.Windows.Forms.Timer();

                startCapTimer.Tick += new EventHandler(TimerStartCapture);
                startCapTimer.Interval = Math.Max((int)startTime.Subtract(DateTime.Now).TotalMilliseconds + 1, 1);

                endCapTimer.Tick += new EventHandler(TimerStopCapture);
                endCapTimer.Interval = Math.Max((int)endTime.Subtract(DateTime.Now).TotalMilliseconds + 1, 1);

                if (i > 0 || DateTime.Now.TimeOfDay.TotalSeconds < EndTimeDTP.Value.TimeOfDay.TotalSeconds)
                {
                    startCapTimer.Start();
                    endCapTimer.Start();
                    m_timerList.Add(startCapTimer);
                    m_timerList.Add(endCapTimer);
                    m_dueDates.Add(startTime);
                    m_dueDates.Add(endTime);
                    if (firstEvent)
                    {
                        m_scheduledDay = startTime.Date;
                        StatusMessage("capture scheduled to: " + DateTime.Now.AddMilliseconds(startCapTimer.Interval).ToString());
                    }
                    firstEvent = false;
                }
            }
            this.StartBtn.Text = "Cancel";
            ToggleOptionsControl(false);
            isScheduled = true;
        }

        private void CancelScheduledCapture()
        {
            StopRecording();
            if (m_timerList == null)
                return;
            foreach (System.Windows.Forms.Timer timer in m_timerList)
                timer.Stop();
            m_timerList = null;
            StatusMessage("capturing is canceled!");
            StartBtn.Text = "Start";
            ToggleOptionsControl(true);
            isScheduled = false;
        }

        private void ToggleOptionsControl(bool enable)
        {
            //tabControl.Enabled = enable;
            foreach (Control item in tabControl.Controls)
            {
                item.Enabled = enable;
            }
        }

        private void StartBtn_Click(object sender, EventArgs e)
        {
            //eventTimer = new System.Timers.Timer;
            if (this.StartBtn.Text == "Start")
                ScheduleCapture();
            else
                CancelScheduledCapture();

            //SetupVideoWindow();
            //Control.Run();
            // + PicoloVideo Sample Program
            /*IVMRWindowlessControl pWc = (IVMRWindowlessControl)this.VMR;
            //                System.Console.WriteLine(this.si);
            //pWc.SetVideoClippingWindow(this.cl);

            int lWidth, lHeight;
            int lARWidth, lARHeight;
            pWc.GetNativeVideoSize(out lWidth, out lHeight, out lARWidth, out lARHeight);
            DsRect src = new DsRect(0, 0, lWidth, lHeight);
            //                System.Console.WriteLine("Source size: " + lWidth.ToString() + " x " + lHeight.ToString());

            //this.ClientRectangle
            pWc.SetVideoPosition(src, new DsRect(this.ClientRectangle));
            */

            // Start an acquisition sequence by activating the channel
            /*            String channelState;
                        MC.GetParam(channel, "ChannelState", out channelState);
                        if (channelState != "ACTIVE")
                        {
                            //MC.SetParam(channel, "ChannelState", "ACTIVE");
                        }
                        Refresh();*/
        }

        public delegate void StatusDelegate();
        private void updateStatus()
        {
            Status1.Text = progressLog;
            Status1.SelectionLength = 0;
            Status1.SelectionStart = Status1.Text.Length;
            Status1.ScrollToCaret();
        }

        private void StatusMessage(String msg)
        {
            progressLog = progressLog + System.Environment.NewLine + "- " + msg;
            if (logFile != null)
            {
                byte[] info = new UTF8Encoding(true).GetBytes("(" + uniqueName + ") " + selectedDevice + " [" + DateTime.Now.ToString("HH:mm:ss.ff dd/MM/yyyy") + "]: - " + msg + System.Environment.NewLine);
                logFile.Write(info, 0, info.Length);
            }
            this.Status1.BeginInvoke(new StatusDelegate(updateStatus));
        }

        private void LoggedMessage(String msg)
        {
#if DEBUG
            progressLog = progressLog + System.Environment.NewLine + "D " + msg;
            this.Status1.BeginInvoke(new StatusDelegate(updateStatus));
#endif
            if (logFile != null)
            {
                byte[] info = new UTF8Encoding(true).GetBytes("(" + uniqueName + ") " + selectedDevice + " (" + DateTime.Now.ToString("HH:mm:ss.ff dd/MM/yyyy") + "): D " + msg + System.Environment.NewLine);
                logFile.Write(info, 0, info.Length);
            }
        }

        private void RestartScheduledRecording()
        {
            CancelScheduledCapture();
            MakeNewCaptureFile();
            WriteMetaData();
            ScheduleCapture();
        }

        /*private void ScheduleRecording()
        {
            StatusMessage("Experiment started!");

            this.StartBtn.Text = "Stop";
            tabControl.SelectedIndex = 0;
            tabControl.Enabled = false;
            //                movieFilename = String.Format(this.GroupTypeTB.Text + ".exp{0:D4}");
            MakeNewCaptureFile();

#if TWITTER
            TwitterTimer.Change(TwitterUpdateFrequency, Timeout.Infinite);
            Thread TwitterStartedThread = new Thread(this.TwitterExperimentStarted);
            TwitterStartedThread.Start();
#endif

            recordingDueStartTime = this.StartTimeDTP.Value;
            isScheduled = true;
            isRecording = false;
        }*/

        /*private void StopScheduledRecording(Boolean userRequest)
        {
            if (userRequest)
            {
                DialogResult result = MessageBox.Show("Are you sure you want to stop the current recording?", "Stop Recording", MessageBoxButtons.YesNo);
                if (result == System.Windows.Forms.DialogResult.No)
                {
                    return;
                }
            }

            this.StartBtn.Text = "Start";
            tabControl.Enabled = true;
            StopScheduledRecording();
        }

        private void StopScheduledRecording()
        {
            isScheduled = false;
            isRecording = false;
            StopRecording();
            StatusMessage("Interrupted");
        }*/

        /*private void FinishedScheduledRecording()
        {
            isScheduled = false;
            isRecording = false;

#if TWITTER
            TwitterTimer.Change(int.MaxValue, Timeout.Infinite);
            Thread TwitterFinishedThread = new Thread(this.TwitterExperimentEnded);
            TwitterFinishedThread.Start();
#endif
            StatusMessage("Finished");

            tabControl.Enabled = true;
            this.StartBtn.Text = "Start";
        }*/

        private void StartRecording()
        {
            try
            {
                videoMutex.WaitOne();
                isRecording = true;
                RecPanel.Visible = true;
                RecPanel.BringToFront();
                MakeNewCaptureFile();
                WriteMetaData();
                StatusMessage("saving to " + CaptureFilename);
                CreateCaptureGraph(CaptureFilename);
                if (Graph != null)
                {
                    //SetupVideoWindow();
                    LoggedMessage("starting graph");
                    Control.Run();
                    StartPreview();

                    LoggedMessage("  setting pangel to front");
                    RecPanel.BringToFront();
#if DEBUG
                FilterState fs;
                Control.GetState(1000, out fs);
                LoggedMessage("  graph status: " + fs.ToString());
#endif
                }
            }
            finally
            {
                videoMutex.ReleaseMutex();
            }
        }

        private void StopRecording()
        {
            try
            {
                videoMutex.WaitOne();
                isRecording = false;
                RecPanel.Visible = false;
                CreatePreviewGraph();
                if (Graph != null)
                {
                    LoggedMessage("starting graph");
                    int hr = Control.Run();
                    StartPreview();
#if DEBUG
                    FilterState fs;
                    Control.GetState(1000, out fs);
                    LoggedMessage("  graph status: " + fs.ToString());
#endif
                }

            }
            finally
            {
                videoMutex.ReleaseMutex();
            }
        }

        /*        private void OnTimedEvent(object source, System.Timers.ElapsedEventArgs e)
                {

                    //MainForm.ActiveForm.
                    //            this.Status1.Text = String.Format("Time to Next Run is {0:dd\\.hh\\:mm\\:ss}", DateTime.Now.Subtract(this.StartTimeDTP.Value));
                    //            this.Status1.Text = String.Format("Next Run is Scheduled in {0:hh\\:mm\\:ss} Hours", DateTime.Now.Subtract(this.StartTimeDTP.Value));
                    if (!this.StartTimeDTP.Enabled)
                    {
                        //this.StartTimeDTP.BeginInvoke(
                        this.StartTimeDTP.Value = DateTime.Now;
                    }
                    this.Status1.Text = String.Format("Next Run is Scheduled to {0:hh\\:mm\\:ss} Hours", DateTime.Now.Subtract(this.StartTimeDTP.Value));
                    //DateTime.Now.Subtract(this.StartTimeDTP.Value).TotalMilliseconds;
                }*/


        private void TransferFile()
        {
            /*if (!transferFilesCB.Checked)
                return;
            fileTransfer = new SSHFileTransfer(this);
            fileTransfer.Show();
            fileTransfer.TransferFile(serverTB.Text, usernameTB.Text, passwordTB.Text, CaptureFilename, remotePathTB.Text);*/
        }

        public void SSHFileTransferDone()
        {
            fileTransfer.Hide();
            fileTransfer = null;
        }

        public delegate void InvokeDelegate();
        private void updateTime()
        {
            this.StartTimeDTP.Value = DateTime.Now;
        }

        /*private void timeEvent(object source)
        {
            try
            {
                //MainForm.ActiveForm.
                //            this.Status1.Text = String.Format("Time to Next Run is {0:dd\\.hh\\:mm\\:ss}", DateTime.Now.Subtract(this.StartTimeDTP.Value));
                //            this.Status1.Text = String.Format("Next Run is Scheduled in {0:hh\\:mm\\:ss} Hours", DateTime.Now.Subtract(this.StartTimeDTP.Value));
                if (!isScheduled && !isRecording && !this.StartTimeDTP.Enabled)
                    this.StartTimeDTP.BeginInvoke(new InvokeDelegate(updateTime));

                if (isScheduled && !isRecording && DateTime.Now.Subtract(recordingDueStartTime).TotalMilliseconds > 0)
                {
                    recordingRealStartTime = DateTime.Now;
                    recordingDueEndTime = recordingRealStartTime.Add(this.FilmingDuration.Value.TimeOfDay);
                    log("Recording Started at " + DateTime.Now.ToString());
                    log("Writing to '" + CaptureFilename + "'");
                    this.BeginInvoke(new InvokeDelegate(StartRecording));
                    isRecording = true;
                }

                if (isScheduled && isRecording && DateTime.Now.Subtract(recordingDueEndTime).TotalMilliseconds > 0)
                {
                    log("Recording Ended at " + DateTime.Now.ToString());
                    this.BeginInvoke(new InvokeDelegate(StopRecording));
                    if (currentDay < this.NumberOfDaysUD.Value)
                    {
                        ++currentDay;
                        currentDayCtrl.Value = currentDay;
                        if (this.FilmingFrequency.Value.TimeOfDay.TotalMilliseconds == 0)
                            recordingDueStartTime = recordingDueStartTime.Add(TimeSpan.FromDays(1));
                        else
                            recordingDueStartTime = recordingDueStartTime.Add(this.FilmingFrequency.Value.TimeOfDay);
                        log("Next recording is scheduled to " + recordingDueStartTime.ToString());
                    }
                    else
                    {
                        isScheduled = false;
                        FinishedScheduledRecording();
                    }
                    isRecording = false;
                }
            }
            finally
            {
                timer.Change(1000, Timeout.Infinite);
            }
            //this.Status1.Text = String.Format("Next Run is Scheduled to {0:hh\\:mm\\:ss} Hours", DateTime.Now.Subtract(this.StartTimeDTP.Value));
            //DateTime.Now.Subtract(this.StartTimeDTP.Value).TotalMilliseconds;
        }*/

        private void CameraCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Create the filter for the selected video input device
            try
            {
                selectedDevice = (MyDevice)CameraCB.SelectedItem;
                videoMutex.WaitOne();
                CreateGraph();
                if (Graph != null)
                {
                    LoggedMessage("starting graph");
                    int hr = Control.Run();
                    LoggedMessage("  setting pangel to front");
                    RecPanel.BringToFront();
#if DEBUG
                    FilterState fs;
                    Control.GetState(1000, out fs);
                    LoggedMessage("  graph status: " + fs.ToString());
#endif
                }
                // Find the string in ListBox2.

                if (selectedDevice != null) {
                Settings.Default.Camera = selectedDevice.Path;
                Settings.Default.Save();
                    }
            }
            catch (Exception ex)
            {
                StatusMessage("Unable to open capture device '" + selectedDevice + "': " + ex.Message);
            }
            finally
            {
                videoMutex.ReleaseMutex();
            }

        }

        private void PathBtn_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = PathTB.Text;
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
                this.PathTB.Text = folderBrowserDialog1.SelectedPath;
        }

        private void PathTB_TextChanged(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = PathTB.Text;
            Settings.Default.Path = this.PathTB.Text;
            Settings.Default.Save();
        }

        private void propertiesButton_Click(object sender, EventArgs e)
        {
            if (this.Source != null)
                DisplayPropertyPage(Source);
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            ResizeVideoWindow();
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void username_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void emailSaveSettings_Click(object sender, EventArgs e)
        {
            Settings.Default.EmailFrom = emailFrom.Text;
            Settings.Default.EmailTo = emailTo.Text;
            Settings.Default.EmailSMTP = smtpServer.Text;
            Settings.Default.EmailPort = smtpPort.Text;
            Settings.Default.EmailAccount = username.Text;
            Settings.Default.EmailPassword = password.Text;
            Settings.Default.UseSSL = useSSL.Checked;
            Settings.Default.ReportOnError = reportOnError.Checked;
            Settings.Default.Save();

#if BUGTRAPPER
            bugTrackingProcess.Kill();
            if (Settings.Default.ReportOnError)
            {
                bugTrackingProcess.StartInfo.FileName = "myBugTrapper.exe";
                bugTrackingProcess.StartInfo.Arguments =
                      "/account=" + Settings.Default.EmailAccount +
                    " /password=" + Settings.Default.EmailPassword +
                        " /from=" + Settings.Default.EmailFrom +
                          " /to=" + Settings.Default.EmailTo +
                        " /smtp=" + Settings.Default.EmailSMTP +
                        " /port=" + Settings.Default.EmailPort +
                      " /useSSL=" + (Settings.Default.UseSSL ? "yes" : "no");
                bugTrackingProcess.StartInfo.CreateNoWindow = true;
                bugTrackingProcess.Start();
            }
#endif
        }

        public static bool SendEmail(String smtp, String port, Boolean useSSL, String account, String password, String from, String to, String subject, String body)
        {
            try
            {
                if (smtp == null)
                    return false;
                SmtpClient client = new System.Net.Mail.SmtpClient(smtp);
                if (port != null && port.Trim() != "")
                    client.Port = int.Parse(port);
                client.EnableSsl = useSSL;

                if (account != null && account.Trim() != "")
                {
                    System.Net.NetworkCredential Credentials = new System.Net.NetworkCredential(account, password);
                    client.Credentials = Credentials;
                }
                client.Send(from, to, subject, body);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public static bool SendEmail(String subject, String body)
        {
            return SendEmail(
                Settings.Default.EmailSMTP,
                Settings.Default.EmailPort,
                Settings.Default.UseSSL,
                Settings.Default.EmailAccount,
                Settings.Default.EmailPassword,
                Settings.Default.EmailFrom,
                Settings.Default.EmailTo,
                subject,
                body);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //throw new Exception("fgsdsfdlkfjslsdfjlksfdjlfsd");

            if (!SendEmail(
                smtpServer.Text,
                smtpPort.Text,
                useSSL.Checked,
                username.Text,
                password.Text,
                emailFrom.Text,
                emailTo.Text,
                "SocialCamera: Email Settings Test", "Congratulations! You have made it! Have a nice day."))
            {
                MessageBox.Show("Unable to send Email. Please check your settings.");
            }
            else
            {
                MessageBox.Show("Success! Please check you Email to ensure.");
            }
        }

        private void copyFilesCB_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = sender as CheckBox;
            usernameTB.Enabled = cb.Checked;
            passwordTB.Enabled = cb.Checked;
            serverTB.Enabled = cb.Checked;
            remotePathTB.Enabled = cb.Checked;
        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void currentDayCtrl_ValueChanged(object sender, EventArgs e)
        {
            currentDay = Decimal.ToInt32(currentDayCtrl.Value);
        }

        //      int repaintCount = 0;
        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            lock (EVRLock)
            {
                if (EVRDisplay != null)
                    EVRDisplay.RepaintVideo();
            }
        }

        private void refreshLogBtn_Click(object sender, EventArgs e)
        {
            try
            {
                logFile.Flush();
                //
                FileStream fs = new FileStream("SocialCamera.log", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

                byte[] bytes = new byte[fs.Length];
                int numBytesToRead = (int)fs.Length;
                int numBytesRead = 0;
                while (numBytesToRead > 0)
                {
                    // Read may return anything from 0 to numBytesToRead.
                    int n = fs.Read(bytes, numBytesRead, numBytesToRead);

                    // Break when the end of the file is reached.
                    if (n == 0)
                        break;
                    numBytesRead += n;
                    numBytesToRead -= n;
                }
                numBytesToRead = bytes.Length;
                logTB.Text = new UTF8Encoding().GetString(bytes);
                fs.Close();

                logTB.SelectionStart = logTB.Text.Length;
                logTB.ScrollToCaret();
                logTB.Refresh();
                /*StreamReader logFileRead = new StreamReader("SocialCamera.log");
                //fs.Write(
                while (true)
                {
                    string line = logFileRead.ReadLine();
                    if (line == null)
                        break;
                    logTB.Text = logTB.Text + line;
                }
                */
            }
            catch (Exception)
            {

            }
        }

        private void tabPage4_Enter(object sender, EventArgs e)
        {
            refreshLogBtn_Click(sender, e);
        }

        private void usernameTB_TextChanged(object sender, EventArgs e)
        {
        }

        private void passwordTB_TextChanged(object sender, EventArgs e)
        {
        }

        private void serverTB_TextChanged(object sender, EventArgs e)
        {
        }

        private void remotePathTB_TextChanged(object sender, EventArgs e)
        {
        }

        private void saveSSHBtn_Click(object sender, EventArgs e)
        {
            Settings.Default.Path = PathTB.Text;
            Settings.Default.SSHUsername = usernameTB.Text;
            Settings.Default.SSHPassword = passwordTB.Text;
            Settings.Default.SSHServer = serverTB.Text;
            Settings.Default.SSHPath = remotePathTB.Text;
            Settings.Default.SSHUse = transferFilesCB.Checked;
            Settings.Default.Save();
        }

        private void CaptureFrame_Click(object sender, EventArgs e)
        {
        }

        private void StartTimeDTP_ValueChanged(object sender, EventArgs e)
        {
            //EndTimeDTP.Value = StartTimeDTP.Value.Add(FilmingDuration.Value.TimeOfDay);
            Settings.Default.FilmingStartTime = StartTimeDTP.Value;
            Settings.Default.Save();
        }

        private void PanicBtn_Click(object sender, System.EventArgs e)
        {
            Restart();
        }

        private void twittBtn_Click(object sender, EventArgs e)
        {
#if TWITTER
            // configure the OAuth object
            var auth = new LinqToTwitter.PinAuthorizer
            {
                Credentials = new LinqToTwitter.InMemoryCredentials
                {
                    ConsumerKey = Settings.Default.ConsumerKey,
                    ConsumerSecret = Settings.Default.ConsumerSecret
                },
                UseCompression = true,
                GoToTwitterAuthorization = pageLink => System.Diagnostics.Process.Start(pageLink),
                GetPin = () =>
                {
                    // this executes after user authorizes, which begins with the call to auth.Authorize() below.
                    TwitterPIN twitterPin = new TwitterPIN();
                    twitterPin.ShowDialog();
                    return Settings.Default.TwitterPIN;
                }
            };

            // start the authorization process (launches Twitter authorization page).
            auth.Authorize();
            twitterAuth = auth;
            if (twitterAuth.IsAuthorized)
            {
                TwitterStatusLabel.Text = "Logged In";
                TwitterStatusLabel.ForeColor = Color.Green;
            }
#endif
        }

        private void NumberOfDaysUD_ValueChanged(object sender, EventArgs e)
        {
            Settings.Default.NumberOfRuns = Decimal.ToInt32(this.NumberOfDaysUD.Value);
            Settings.Default.Save();
        }

        private void EndTimeDTP_ValueChanged(object sender, EventArgs e)
        {
            Settings.Default.FilmingEndTime = EndTimeDTP.Value;
            Settings.Default.Save();
        }

        private void GroupTypeTB_TextChanged(object sender, EventArgs e)
        {
            if (GroupTypeTB.Text.IndexOf(' ') > 0) {
                int pos = GroupTypeTB.SelectionStart;
                GroupTypeTB.Text = GroupTypeTB.Text.Replace(" ", "");
                GroupTypeTB.SelectionStart = pos-1;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Settings.Default.DefaultGroupType = GroupTypeSelectCB.SelectedIndex;
            Settings.Default.Save();

            Regex type = new Regex(@"\((?<type>\w+)\)");
            GroupTypeTB.Text = type.Match(Settings.Default.GroupTypes[GroupTypeSelectCB.SelectedIndex]).Groups["type"].Value;
        }

        private void comboBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void typeRemoveBtn_Click(object sender, EventArgs e)
        {
            Settings.Default.GroupTypes.Remove(typesListBox.Items[typesListBox.SelectedIndex].ToString());
            Settings.Default.Save();
            PopulateTypes();
        }

        private void PopulateTypes()
        {
            GroupTypeSelectCB.Items.Clear();
            typesListBox.Items.Clear();
            foreach (string s in Settings.Default.GroupTypes)
            {
                Regex type = new Regex(@"\s*\((?<type>\w+)\)");
                GroupTypeSelectCB.Items.Add(s);
                typesListBox.Items.Add(s);
            }
            GroupTypeSelectCB.SelectedIndex = Math.Min(Settings.Default.DefaultGroupType, GroupTypeSelectCB.Items.Count - 1);

        }

        private void typeAddBtn_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length > 0 && textBox2.Text.Length > 0)
            {
                Settings.Default.GroupTypes.Add(textBox2.Text + " (" + textBox1.Text + ")");
                Settings.Default.Save();
                PopulateTypes();
                textBox1.Text = "";
                textBox2.Text = "";
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            Regex r = new Regex(@"[\(\)]");
            if (r.IsMatch(textBox2.Text))
            {
                textBox2.Text = r.Replace(textBox2.Text, "");
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '(' || e.KeyChar == ')')
                e.Handled = true;
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Regex r = new Regex("^[0-9]+");
            if (r.IsMatch(textBox1.Text))
            {
                textBox1.Text = r.Replace(textBox1.Text, "");
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar))
            {
                if (textBox1.Text.Length == 0)
                    e.Handled = true;
            } 
            else if (!(Char.IsLetterOrDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
                e.Handled = true;
        }

        private void tabPage6_Click(object sender, EventArgs e)
        {

        }

/*        private void KinectButton_Click(object sender, EventArgs e)
        {
            try
            {
                KinectDevice.WindowControl = KinectPanel;
                KinectDevice.EnableGrabber = true;
                KinectDevice.Preview();
            }
            catch
            {
            }
        }

        private void KinectCaptureButton_Click(object sender, EventArgs e)
        {
            Bitmap bmp = KinectDevice.GrabCurrentFrame();


            // Find an unused file name
            int cameraIndex = selectedDeviceIndex;
            string sBaseName = String.Format(this.GroupTypeTB.Text + ".exp{0:D4}.day{1:D2}.cam{2:D2}",
                System.Decimal.ToInt32(this.GroupNumberUD.Value),
                currentDay,
                cameraIndex + 1);

            int indexFile = 0;
            string sFileName = string.Format(@"{0}\{1}.{2}.depth.bmp", folderBrowserDialog1.SelectedPath, sBaseName, DateTime.Now.ToString("s").Replace(':', '-'));
            while (File.Exists(sFileName))
            {
                sFileName = string.Format(@"{0}\{1}.{2}.{3}.depth.bmp", folderBrowserDialog1.SelectedPath, sBaseName, DateTime.Now.ToString("s").Replace(':', '-'), ++indexFile);
            }
            KinectPictBox.Image = bmp;
            bmp.Save(sFileName);
        }

        private void KinectStopButton_Click(object sender, EventArgs e)
        {
            KinectDevice.Stop();
        }*/

        private void BeepButton_Click(object sender, EventArgs e)
        {
            MyAudioPlayer audio = new MyAudioPlayer(MyAudioPlayer.WaveType.Square, 12.5, MyAudioPlayer.WaveType.Sine, 400, 10);
            //string filename = System.IO.Path.GetTempFileName();
            //audio.Save(@"d:\beep2.wav");
            

            String filename = "C:\\Windows\\Media\\tada.wav";
            System.Media.SoundPlayer sp = new System.Media.SoundPlayer(filename);

            //TaskFactory uiFactory = new TaskFactory(TaskScheduler.FromCurrentSynchronizationContext());
            //var task = uiFactory.StartNew(() => audio.Play());

            audio.Play();
        }

        private void elementHost1_ChildChanged(object sender, System.Windows.Forms.Integration.ChildChangedEventArgs e)
        {

        }

        private void tabPage6_Paint(object sender, PaintEventArgs e)
        {
            //KinectControl.Width = 0;
            //KinectControl.Height = 0;
            //KinectControl.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            //KinectControl.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
            KinectControl.Margin = new System.Windows.Thickness(0, 0, 0, 0);
        }

        private void videoPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        public void RTTracking(object sender, NewFrameEventArgs e)
        {
            RTPB.Image = e.Grabber.image;
        }

        private void RTCB_CheckedChanged(object sender, EventArgs e)
        {
            if (RTCB.Checked)
            {
                EnableGrabber = true;
                OnNewFrame += RTTracking;
                CreatePreviewGraph();
                StartPreview();
            }
            else
            {
                EnableGrabber = false;
                OnNewFrame += RTTracking;
                CreatePreviewGraph();
                StartPreview();
            }
        }

        private void ReviveButton_Click(object sender, EventArgs e)
        {
            LostDevice();
        }



#if TWITTER
        private void TimerUpdateTwitter(object source)
        {
            TwitterTimer.Change(TwitterUpdateFrequency, Timeout.Infinite);
            if (twitterAuth == null || !twitterAuth.IsAuthorized)
                return;
            if (isRecording)
            {
                Twitter("Group '" + GroupTypeTB.Text + "' no. " + GroupNumberUD.Value.ToString() + " day " + currentDay + ", is currently recorded on camera " + (CameraCB.SelectedIndex + 1).ToString() + " (from " + recordingRealStartTime.ToShortTimeString() + " till " + recordingDueEndTime.ToShortTimeString() + ")");
            }
            else if (isScheduled)
            {
                Twitter("Group '" + GroupTypeTB.Text + "' no. " + GroupNumberUD.Value.ToString() + " day " + currentDay + ", will start recording at " + recordingDueStartTime.ToShortTimeString() + "...");
            }
        }

        private void TwitterExperimentStarted()
        {
            Twitter("Group '" + GroupTypeTB.Text + "' no. " + GroupNumberUD.Value.ToString() + ", has started the experiment!");
        }

        private void TwitterExperimentEnded()
        {
            Twitter("Group '" + GroupTypeTB.Text + "' no. " + GroupNumberUD.Value.ToString() + ", has finished the experiment!");
        }

        private void Twitter(string status)
        {
            using (var twitterCtx = new LinqToTwitter.TwitterContext(twitterAuth, "https://api.twitter.com/1/", "https://search.twitter.com/"))
            {
                //Log
                twitterCtx.Log = Console.Out;

                // the \u00C7 is C Cedilla, which I've included to ensure that non-ascii characters appear properly
                //var status = "\u00C7 Testing LINQ to Twitter update status on " + DateTime.Now.ToString() + " #linqtotwitter";

                //log("Status being sent: " + status);

                //                var tweet = twitterCtx.UpdateStatus(status, 37.78215m, -122.40060m, true);
                var tweet = twitterCtx.UpdateStatus(status);

                /*log(String.Format(
                    "User: {0}, Tweet: {1}\nLatitude: {2}, Longitude: {3}",
                    tweet.User.Identifier.ScreenName,
                    tweet.Text,
                    tweet.Coordinates.Latitude,
                    tweet.Coordinates.Longitude));   */
            }
        }
#endif
    }


    internal static class NativeMethods
    {
        [DllImport("I2CAPI32.dll", CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.BStr)]
        public static extern string I2cGetInterfaceTypeStr([MarshalAs(UnmanagedType.BStr)] string Interface);

        // Import SetThreadExecutionState Win32 API and necessary flags
        [DllImport("kernel32.dll")]
        public static extern uint SetThreadExecutionState(uint esFlags);
        public const uint ES_CONTINUOUS = 0x80000000;
        public const uint ES_SYSTEM_REQUIRED = 0x00000001;
        public const uint ES_DISPLAY_REQUIRED = 0x00000002;

    }

    public class NewFrameEventArgs : EventArgs
    {
        public MyVideoGrabber Grabber { get; private set; }

        public NewFrameEventArgs(MyVideoGrabber grabber)
        {
            Grabber = grabber;
        }
    }

    public interface IMyVideoGrabber
    {
        void HandleNewVideoFrame();
    }

    public class MyVideoGrabber : ISampleGrabberCB
    {
        VideoInfoHeader InfoHeader;
        private byte[] FrameBuffer;
        static private Mutex mut = new Mutex();
        private bool Waiting = false;

        public Bitmap image = null;
        private IMyVideoGrabber Handle = null;

        public MyVideoGrabber(IMyVideoGrabber handle, VideoInfoHeader infoHeader)
        {
            InfoHeader = infoHeader;
            Handle = handle;
        }


        int ISampleGrabberCB.SampleCB(double SampleTime, IMediaSample pSample)
        {
            return 0;
        }

        Semaphore pool = new Semaphore(0, 1);

        public Bitmap GetFrame()
        {
            Waiting = true;
            pool.WaitOne();
            return image;
        }

        int ISampleGrabberCB.BufferCB(double SampleTime, IntPtr pBuffer, int BufferLen)
        {
            mut.WaitOne();
            try
            {
                if (!Waiting)
                    return 0;
                /*mut.WaitOne();
                if (!Waiting)
                    return 0;*/
                if (FrameBuffer == null || BufferLen > FrameBuffer.Length)
                    FrameBuffer = new byte[BufferLen];
                Marshal.Copy(pBuffer, FrameBuffer, 0, BufferLen);
                GCHandle handle = GCHandle.Alloc(FrameBuffer, GCHandleType.Pinned);
                int scan0 = (int)handle.AddrOfPinnedObject();
                int stride = (InfoHeader.BmiHeader.BitCount / 8) * InfoHeader.BmiHeader.Width;
                //scan0 += (InfoHeader.BmiHeader.Height - 1) * stride;
                image = new Bitmap(InfoHeader.BmiHeader.Width, InfoHeader.BmiHeader.Height > 0 ? InfoHeader.BmiHeader.Height : -InfoHeader.BmiHeader.Height, stride, System.Drawing.Imaging.PixelFormat.Format24bppRgb, (IntPtr)scan0);
                image.RotateFlip(RotateFlipType.RotateNoneFlipY);
                handle.Free();
                //VideoControl.SetFrame(bmp);
                Waiting = false;
                pool.Release();
                //NewFrameEventArgs args = new NewFrameEventArgs(this);
                Handle.HandleNewVideoFrame();
            }
            finally
            {
                mut.ReleaseMutex();
            }
            return 0;
        }

    }

    class MyDevice
    {
        private DsDevice DirectShowDevice;

        public string Name   // the Name property
        {
            get
            {
                return DirectShowDevice.Name;
            }
        }

        public string Path   // the Name property
        {
            get
            {
                return DirectShowDevice.DevicePath;
            }
        }


        public UCOMIMoniker Moniker   // the Name property
        {
            get
            {
                return (UCOMIMoniker)DirectShowDevice.Mon;
            }
        }

        public MyDevice(DsDevice ds)
        {
            DirectShowDevice = ds;
        }

        public static implicit operator MyDevice(DsDevice ds)
        {
            return new MyDevice(ds);
        }

        public static MyDevice[] GetDevices()
        {
            DsDevice[] dsDevices = DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice);
            MyDevice[] myDevices = new MyDevice[dsDevices.Length];
            int index = 0;
            foreach (DsDevice ds in dsDevices)
            {
                myDevices[index] = ds;
                ++index;
            }
            return myDevices;
        }

        public override string ToString()
        {
            return DirectShowDevice.Name;
        }
    }




}

