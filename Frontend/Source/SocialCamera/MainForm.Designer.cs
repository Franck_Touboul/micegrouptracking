﻿
namespace SocialCamera
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            if (disposing)
            {
                // Stop capturing and release interfaces
                CloseInterfaces();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.videoPanel = new System.Windows.Forms.Panel();
            this.RecPanel = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.GroupTypeSelectCB = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.EndTimeDTP = new System.Windows.Forms.DateTimePicker();
            this.currentDayCtrl = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.propertiesButton = new System.Windows.Forms.Button();
            this.RemarksTB = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.NumberOfDaysUD = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.StartTimeDTP = new System.Windows.Forms.DateTimePicker();
            this.GroupNumberUD = new System.Windows.Forms.NumericUpDown();
            this.GroupTypeTB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CameraCB = new System.Windows.Forms.ComboBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.saveSSHBtn = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.passwordTB = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.PathBtn = new System.Windows.Forms.Button();
            this.usernameTB = new System.Windows.Forms.TextBox();
            this.serverTB = new System.Windows.Forms.TextBox();
            this.PathTB = new System.Windows.Forms.TextBox();
            this.transferFilesCB = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.remotePathTB = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.emailCheck = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.emailSaveSettings = new System.Windows.Forms.Button();
            this.reportOnError = new System.Windows.Forms.CheckBox();
            this.useSSL = new System.Windows.Forms.CheckBox();
            this.emailTo = new System.Windows.Forms.TextBox();
            this.emailFrom = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.TextBox();
            this.username = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.smtpPort = new System.Windows.Forms.TextBox();
            this.smtpServer = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.refreshLogBtn = new System.Windows.Forms.Button();
            this.logTB = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.CaptureFrame = new System.Windows.Forms.Button();
            this.SnapPictureBox = new System.Windows.Forms.PictureBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.KinectControl = new KinectToolbox.KinectControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.typeAddBtn = new System.Windows.Forms.Button();
            this.typeRemoveBtn = new System.Windows.Forms.Button();
            this.typesListBox = new System.Windows.Forms.ListBox();
            this.RTTabPage = new System.Windows.Forms.TabPage();
            this.RTPB = new System.Windows.Forms.PictureBox();
            this.RTCB = new System.Windows.Forms.CheckBox();
            this.StatusBox = new System.Windows.Forms.GroupBox();
            this.Status1 = new System.Windows.Forms.TextBox();
            this.MainStatusStrip = new System.Windows.Forms.StatusStrip();
            this.StatusBar = new System.Windows.Forms.ToolStripStatusLabel();
            this.DroppedFrameStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.StartBtn = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.PanicBtn = new System.Windows.Forms.Button();
            this.twittBtn = new System.Windows.Forms.Button();
            this.TwitterStatusLabel = new System.Windows.Forms.Label();
            this.BeepButton = new System.Windows.Forms.Button();
            this.ReviveButton = new System.Windows.Forms.Button();
            this.videoPanel.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.currentDayCtrl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumberOfDaysUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupNumberUD)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.emailCheck.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SnapPictureBox)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.RTTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RTPB)).BeginInit();
            this.StatusBox.SuspendLayout();
            this.MainStatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // videoPanel
            // 
            this.videoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.videoPanel.BackColor = System.Drawing.Color.Black;
            this.videoPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.videoPanel.Controls.Add(this.RecPanel);
            this.videoPanel.Location = new System.Drawing.Point(11, 11);
            this.videoPanel.Name = "videoPanel";
            this.videoPanel.Size = new System.Drawing.Size(465, 423);
            this.videoPanel.TabIndex = 2;
            this.videoPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.videoPanel_Paint);
            // 
            // RecPanel
            // 
            this.RecPanel.BackColor = System.Drawing.Color.Red;
            this.RecPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RecPanel.ForeColor = System.Drawing.Color.Black;
            this.RecPanel.Location = new System.Drawing.Point(25, 22);
            this.RecPanel.Name = "RecPanel";
            this.RecPanel.Size = new System.Drawing.Size(20, 20);
            this.RecPanel.TabIndex = 0;
            this.RecPanel.Visible = false;
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage3);
            this.tabControl.Controls.Add(this.emailCheck);
            this.tabControl.Controls.Add(this.tabPage4);
            this.tabControl.Controls.Add(this.tabPage5);
            this.tabControl.Controls.Add(this.tabPage6);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.RTTabPage);
            this.tabControl.Location = new System.Drawing.Point(493, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(413, 303);
            this.tabControl.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.GroupTypeSelectCB);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.EndTimeDTP);
            this.tabPage1.Controls.Add(this.currentDayCtrl);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.propertiesButton);
            this.tabPage1.Controls.Add(this.RemarksTB);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.NumberOfDaysUD);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.StartTimeDTP);
            this.tabPage1.Controls.Add(this.GroupNumberUD);
            this.tabPage1.Controls.Add(this.GroupTypeTB);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.CameraCB);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(405, 277);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Common";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // GroupTypeSelectCB
            // 
            this.GroupTypeSelectCB.FormattingEnabled = true;
            this.GroupTypeSelectCB.Items.AddRange(new object[] {
            "Hello",
            "Bye"});
            this.GroupTypeSelectCB.Location = new System.Drawing.Point(105, 58);
            this.GroupTypeSelectCB.Name = "GroupTypeSelectCB";
            this.GroupTypeSelectCB.Size = new System.Drawing.Size(269, 21);
            this.GroupTypeSelectCB.TabIndex = 10;
            this.GroupTypeSelectCB.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.GroupTypeSelectCB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox1_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(201, 134);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 13);
            this.label21.TabIndex = 22;
            this.label21.Text = "End Time";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // EndTimeDTP
            // 
            this.EndTimeDTP.CustomFormat = "HH:mm:ss";
            this.EndTimeDTP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.EndTimeDTP.Location = new System.Drawing.Point(297, 130);
            this.EndTimeDTP.Name = "EndTimeDTP";
            this.EndTimeDTP.ShowUpDown = true;
            this.EndTimeDTP.Size = new System.Drawing.Size(77, 20);
            this.EndTimeDTP.TabIndex = 5;
            this.EndTimeDTP.Value = new System.DateTime(2010, 11, 7, 0, 0, 0, 0);
            this.EndTimeDTP.ValueChanged += new System.EventHandler(this.EndTimeDTP_ValueChanged);
            // 
            // currentDayCtrl
            // 
            this.currentDayCtrl.Location = new System.Drawing.Point(105, 172);
            this.currentDayCtrl.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.currentDayCtrl.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.currentDayCtrl.Name = "currentDayCtrl";
            this.currentDayCtrl.Size = new System.Drawing.Size(77, 20);
            this.currentDayCtrl.TabIndex = 6;
            this.currentDayCtrl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.currentDayCtrl.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.currentDayCtrl.ValueChanged += new System.EventHandler(this.currentDayCtrl_ValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(16, 174);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(63, 13);
            this.label19.TabIndex = 19;
            this.label19.Text = "Current Day";
            this.label19.Click += new System.EventHandler(this.label19_Click);
            // 
            // propertiesButton
            // 
            this.propertiesButton.Location = new System.Drawing.Point(311, 16);
            this.propertiesButton.Name = "propertiesButton";
            this.propertiesButton.Size = new System.Drawing.Size(71, 23);
            this.propertiesButton.TabIndex = 1;
            this.propertiesButton.Text = "Properties";
            this.propertiesButton.UseVisualStyleBackColor = true;
            this.propertiesButton.Click += new System.EventHandler(this.propertiesButton_Click);
            // 
            // RemarksTB
            // 
            this.RemarksTB.Location = new System.Drawing.Point(105, 214);
            this.RemarksTB.Multiline = true;
            this.RemarksTB.Name = "RemarksTB";
            this.RemarksTB.Size = new System.Drawing.Size(269, 41);
            this.RemarksTB.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 214);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Remarks";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(201, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Number of Days";
            // 
            // NumberOfDaysUD
            // 
            this.NumberOfDaysUD.Location = new System.Drawing.Point(297, 174);
            this.NumberOfDaysUD.Name = "NumberOfDaysUD";
            this.NumberOfDaysUD.Size = new System.Drawing.Size(77, 20);
            this.NumberOfDaysUD.TabIndex = 7;
            this.NumberOfDaysUD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NumberOfDaysUD.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.NumberOfDaysUD.ValueChanged += new System.EventHandler(this.NumberOfDaysUD_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Start Time";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // StartTimeDTP
            // 
            this.StartTimeDTP.CustomFormat = "HH:mm:ss";
            this.StartTimeDTP.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.StartTimeDTP.Location = new System.Drawing.Point(105, 130);
            this.StartTimeDTP.Name = "StartTimeDTP";
            this.StartTimeDTP.ShowUpDown = true;
            this.StartTimeDTP.Size = new System.Drawing.Size(77, 20);
            this.StartTimeDTP.TabIndex = 4;
            this.StartTimeDTP.Value = new System.DateTime(2010, 11, 7, 0, 0, 0, 0);
            this.StartTimeDTP.ValueChanged += new System.EventHandler(this.StartTimeDTP_ValueChanged);
            // 
            // GroupNumberUD
            // 
            this.GroupNumberUD.Location = new System.Drawing.Point(105, 95);
            this.GroupNumberUD.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.GroupNumberUD.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.GroupNumberUD.Name = "GroupNumberUD";
            this.GroupNumberUD.Size = new System.Drawing.Size(77, 20);
            this.GroupNumberUD.TabIndex = 3;
            this.GroupNumberUD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.GroupNumberUD.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // GroupTypeTB
            // 
            this.GroupTypeTB.Location = new System.Drawing.Point(17, 249);
            this.GroupTypeTB.Name = "GroupTypeTB";
            this.GroupTypeTB.Size = new System.Drawing.Size(77, 20);
            this.GroupTypeTB.TabIndex = 2;
            this.GroupTypeTB.Text = "SC";
            this.GroupTypeTB.Visible = false;
            this.GroupTypeTB.TextChanged += new System.EventHandler(this.GroupTypeTB_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Group Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Group Number";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Device";
            // 
            // CameraCB
            // 
            this.CameraCB.FormattingEnabled = true;
            this.CameraCB.Location = new System.Drawing.Point(105, 18);
            this.CameraCB.Name = "CameraCB";
            this.CameraCB.Size = new System.Drawing.Size(200, 21);
            this.CameraCB.TabIndex = 0;
            this.CameraCB.SelectedIndexChanged += new System.EventHandler(this.CameraCB_SelectedIndexChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.textBox3);
            this.tabPage3.Controls.Add(this.label22);
            this.tabPage3.Controls.Add(this.saveSSHBtn);
            this.tabPage3.Controls.Add(this.label20);
            this.tabPage3.Controls.Add(this.passwordTB);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.PathBtn);
            this.tabPage3.Controls.Add(this.usernameTB);
            this.tabPage3.Controls.Add(this.serverTB);
            this.tabPage3.Controls.Add(this.PathTB);
            this.tabPage3.Controls.Add(this.transferFilesCB);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.remotePathTB);
            this.tabPage3.Controls.Add(this.label17);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(405, 277);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Files";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(106, 45);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(238, 20);
            this.textBox3.TabIndex = 32;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(16, 52);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(29, 13);
            this.label22.TabIndex = 31;
            this.label22.Text = "Path";
            this.label22.Click += new System.EventHandler(this.label22_Click);
            // 
            // saveSSHBtn
            // 
            this.saveSSHBtn.Location = new System.Drawing.Point(18, 238);
            this.saveSSHBtn.Name = "saveSSHBtn";
            this.saveSSHBtn.Size = new System.Drawing.Size(76, 23);
            this.saveSSHBtn.TabIndex = 30;
            this.saveSSHBtn.Text = "Save";
            this.saveSSHBtn.UseVisualStyleBackColor = true;
            this.saveSSHBtn.Click += new System.EventHandler(this.saveSSHBtn_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(15, 121);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 13);
            this.label20.TabIndex = 24;
            this.label20.Text = "Username";
            // 
            // passwordTB
            // 
            this.passwordTB.Location = new System.Drawing.Point(105, 144);
            this.passwordTB.Name = "passwordTB";
            this.passwordTB.Size = new System.Drawing.Size(238, 20);
            this.passwordTB.TabIndex = 23;
            this.passwordTB.UseSystemPasswordChar = true;
            this.passwordTB.TextChanged += new System.EventHandler(this.passwordTB_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 177);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "Remote Server";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(15, 147);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 13);
            this.label18.TabIndex = 21;
            this.label18.Text = "Password";
            // 
            // PathBtn
            // 
            this.PathBtn.Location = new System.Drawing.Point(350, 17);
            this.PathBtn.Name = "PathBtn";
            this.PathBtn.Size = new System.Drawing.Size(25, 23);
            this.PathBtn.TabIndex = 8;
            this.PathBtn.Text = "...";
            this.PathBtn.UseVisualStyleBackColor = true;
            this.PathBtn.Click += new System.EventHandler(this.PathBtn_Click);
            // 
            // usernameTB
            // 
            this.usernameTB.Location = new System.Drawing.Point(105, 118);
            this.usernameTB.Name = "usernameTB";
            this.usernameTB.Size = new System.Drawing.Size(238, 20);
            this.usernameTB.TabIndex = 20;
            this.usernameTB.TextChanged += new System.EventHandler(this.usernameTB_TextChanged);
            // 
            // serverTB
            // 
            this.serverTB.Location = new System.Drawing.Point(105, 174);
            this.serverTB.Name = "serverTB";
            this.serverTB.Size = new System.Drawing.Size(238, 20);
            this.serverTB.TabIndex = 19;
            this.serverTB.TextChanged += new System.EventHandler(this.serverTB_TextChanged);
            // 
            // PathTB
            // 
            this.PathTB.Location = new System.Drawing.Point(106, 19);
            this.PathTB.Name = "PathTB";
            this.PathTB.Size = new System.Drawing.Size(238, 20);
            this.PathTB.TabIndex = 4;
            this.PathTB.TextChanged += new System.EventHandler(this.PathTB_TextChanged);
            // 
            // transferFilesCB
            // 
            this.transferFilesCB.AutoSize = true;
            this.transferFilesCB.Location = new System.Drawing.Point(19, 95);
            this.transferFilesCB.Name = "transferFilesCB";
            this.transferFilesCB.Size = new System.Drawing.Size(147, 17);
            this.transferFilesCB.TabIndex = 18;
            this.transferFilesCB.Text = "Transfer Files when Done";
            this.transferFilesCB.UseVisualStyleBackColor = true;
            this.transferFilesCB.CheckedChanged += new System.EventHandler(this.copyFilesCB_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Path";
            // 
            // remotePathTB
            // 
            this.remotePathTB.Location = new System.Drawing.Point(105, 200);
            this.remotePathTB.Name = "remotePathTB";
            this.remotePathTB.Size = new System.Drawing.Size(238, 20);
            this.remotePathTB.TabIndex = 13;
            this.remotePathTB.TextChanged += new System.EventHandler(this.remotePathTB_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(15, 204);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 13);
            this.label17.TabIndex = 12;
            this.label17.Text = "Remote Path";
            // 
            // emailCheck
            // 
            this.emailCheck.Controls.Add(this.label10);
            this.emailCheck.Controls.Add(this.button2);
            this.emailCheck.Controls.Add(this.emailSaveSettings);
            this.emailCheck.Controls.Add(this.reportOnError);
            this.emailCheck.Controls.Add(this.useSSL);
            this.emailCheck.Controls.Add(this.emailTo);
            this.emailCheck.Controls.Add(this.emailFrom);
            this.emailCheck.Controls.Add(this.label12);
            this.emailCheck.Controls.Add(this.label14);
            this.emailCheck.Controls.Add(this.label15);
            this.emailCheck.Controls.Add(this.label13);
            this.emailCheck.Controls.Add(this.password);
            this.emailCheck.Controls.Add(this.username);
            this.emailCheck.Controls.Add(this.label11);
            this.emailCheck.Controls.Add(this.smtpPort);
            this.emailCheck.Controls.Add(this.smtpServer);
            this.emailCheck.Location = new System.Drawing.Point(4, 22);
            this.emailCheck.Name = "emailCheck";
            this.emailCheck.Padding = new System.Windows.Forms.Padding(3);
            this.emailCheck.Size = new System.Drawing.Size(405, 277);
            this.emailCheck.TabIndex = 2;
            this.emailCheck.Text = "Email";
            this.emailCheck.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(273, 127);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 13);
            this.label10.TabIndex = 31;
            this.label10.Text = "Port";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(18, 238);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(76, 23);
            this.button2.TabIndex = 30;
            this.button2.Text = "Check";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // emailSaveSettings
            // 
            this.emailSaveSettings.Location = new System.Drawing.Point(100, 238);
            this.emailSaveSettings.Name = "emailSaveSettings";
            this.emailSaveSettings.Size = new System.Drawing.Size(76, 23);
            this.emailSaveSettings.TabIndex = 29;
            this.emailSaveSettings.Text = "Save";
            this.emailSaveSettings.UseVisualStyleBackColor = true;
            this.emailSaveSettings.Click += new System.EventHandler(this.emailSaveSettings_Click);
            // 
            // reportOnError
            // 
            this.reportOnError.AutoSize = true;
            this.reportOnError.Location = new System.Drawing.Point(18, 13);
            this.reportOnError.Name = "reportOnError";
            this.reportOnError.Size = new System.Drawing.Size(116, 17);
            this.reportOnError.TabIndex = 28;
            this.reportOnError.Text = "Send Error Reports";
            this.reportOnError.UseVisualStyleBackColor = true;
            // 
            // useSSL
            // 
            this.useSSL.AutoSize = true;
            this.useSSL.Location = new System.Drawing.Point(276, 166);
            this.useSSL.Name = "useSSL";
            this.useSSL.Size = new System.Drawing.Size(117, 17);
            this.useSSL.TabIndex = 26;
            this.useSSL.Text = "SSL Authentication";
            this.useSSL.UseVisualStyleBackColor = true;
            // 
            // emailTo
            // 
            this.emailTo.Location = new System.Drawing.Point(105, 84);
            this.emailTo.Name = "emailTo";
            this.emailTo.Size = new System.Drawing.Size(143, 20);
            this.emailTo.TabIndex = 24;
            // 
            // emailFrom
            // 
            this.emailFrom.Location = new System.Drawing.Point(105, 44);
            this.emailFrom.Name = "emailFrom";
            this.emailFrom.Size = new System.Drawing.Size(143, 20);
            this.emailFrom.TabIndex = 23;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 127);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "SMTP server";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 87);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "To";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 47);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "From";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 207);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "Password";
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(105, 204);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(143, 20);
            this.password.TabIndex = 14;
            this.password.UseSystemPasswordChar = true;
            this.password.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // username
            // 
            this.username.Location = new System.Drawing.Point(105, 164);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(143, 20);
            this.username.TabIndex = 10;
            this.username.TextChanged += new System.EventHandler(this.username_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 167);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "User Name";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // smtpPort
            // 
            this.smtpPort.Location = new System.Drawing.Point(311, 124);
            this.smtpPort.Name = "smtpPort";
            this.smtpPort.Size = new System.Drawing.Size(63, 20);
            this.smtpPort.TabIndex = 7;
            // 
            // smtpServer
            // 
            this.smtpServer.Location = new System.Drawing.Point(105, 124);
            this.smtpServer.Name = "smtpServer";
            this.smtpServer.Size = new System.Drawing.Size(143, 20);
            this.smtpServer.TabIndex = 6;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.refreshLogBtn);
            this.tabPage4.Controls.Add(this.logTB);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(405, 277);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "Log";
            this.tabPage4.UseVisualStyleBackColor = true;
            this.tabPage4.Enter += new System.EventHandler(this.tabPage4_Enter);
            // 
            // refreshLogBtn
            // 
            this.refreshLogBtn.Location = new System.Drawing.Point(18, 238);
            this.refreshLogBtn.Name = "refreshLogBtn";
            this.refreshLogBtn.Size = new System.Drawing.Size(76, 23);
            this.refreshLogBtn.TabIndex = 34;
            this.refreshLogBtn.Text = "Refresh";
            this.refreshLogBtn.UseVisualStyleBackColor = true;
            this.refreshLogBtn.Click += new System.EventHandler(this.refreshLogBtn_Click);
            // 
            // logTB
            // 
            this.logTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logTB.BackColor = System.Drawing.Color.White;
            this.logTB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.logTB.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.logTB.Location = new System.Drawing.Point(6, 6);
            this.logTB.Multiline = true;
            this.logTB.Name = "logTB";
            this.logTB.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.logTB.Size = new System.Drawing.Size(393, 226);
            this.logTB.TabIndex = 2;
            this.logTB.WordWrap = false;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.button4);
            this.tabPage5.Controls.Add(this.CaptureFrame);
            this.tabPage5.Controls.Add(this.SnapPictureBox);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(405, 277);
            this.tabPage5.TabIndex = 5;
            this.tabPage5.Text = "Snapshot";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(100, 238);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(76, 23);
            this.button4.TabIndex = 32;
            this.button4.Text = "Save";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // CaptureFrame
            // 
            this.CaptureFrame.Location = new System.Drawing.Point(18, 238);
            this.CaptureFrame.Name = "CaptureFrame";
            this.CaptureFrame.Size = new System.Drawing.Size(76, 23);
            this.CaptureFrame.TabIndex = 31;
            this.CaptureFrame.Text = "Capture";
            this.CaptureFrame.UseVisualStyleBackColor = true;
            this.CaptureFrame.Click += new System.EventHandler(this.CaptureFrame_Click);
            // 
            // SnapPictureBox
            // 
            this.SnapPictureBox.BackColor = System.Drawing.Color.DimGray;
            this.SnapPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SnapPictureBox.Location = new System.Drawing.Point(7, 7);
            this.SnapPictureBox.Name = "SnapPictureBox";
            this.SnapPictureBox.Size = new System.Drawing.Size(392, 225);
            this.SnapPictureBox.TabIndex = 0;
            this.SnapPictureBox.TabStop = false;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.elementHost1);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(405, 277);
            this.tabPage6.TabIndex = 7;
            this.tabPage6.Text = "Kinect";
            this.tabPage6.UseVisualStyleBackColor = true;
            this.tabPage6.Click += new System.EventHandler(this.tabPage6_Click);
            this.tabPage6.Paint += new System.Windows.Forms.PaintEventHandler(this.tabPage6_Paint);
            // 
            // elementHost1
            // 
            this.elementHost1.BackColor = System.Drawing.Color.DarkRed;
            this.elementHost1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(3, 3);
            this.elementHost1.Margin = new System.Windows.Forms.Padding(0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(399, 271);
            this.elementHost1.TabIndex = 16;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.ChildChanged += new System.EventHandler<System.Windows.Forms.Integration.ChildChangedEventArgs>(this.elementHost1_ChildChanged);
            this.elementHost1.Child = this.KinectControl;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.textBox1);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.typeAddBtn);
            this.tabPage2.Controls.Add(this.typeRemoveBtn);
            this.tabPage2.Controls.Add(this.typesListBox);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(405, 277);
            this.tabPage2.TabIndex = 6;
            this.tabPage2.Text = "Types";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(110, 236);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(154, 20);
            this.textBox2.TabIndex = 8;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.textBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox2_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 239);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Description";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(110, 206);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(154, 20);
            this.textBox1.TabIndex = 6;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 209);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Name";
            // 
            // typeAddBtn
            // 
            this.typeAddBtn.Location = new System.Drawing.Point(324, 234);
            this.typeAddBtn.Name = "typeAddBtn";
            this.typeAddBtn.Size = new System.Drawing.Size(75, 23);
            this.typeAddBtn.TabIndex = 2;
            this.typeAddBtn.Text = "Add";
            this.typeAddBtn.UseVisualStyleBackColor = true;
            this.typeAddBtn.Click += new System.EventHandler(this.typeAddBtn_Click);
            // 
            // typeRemoveBtn
            // 
            this.typeRemoveBtn.Location = new System.Drawing.Point(324, 6);
            this.typeRemoveBtn.Name = "typeRemoveBtn";
            this.typeRemoveBtn.Size = new System.Drawing.Size(75, 23);
            this.typeRemoveBtn.TabIndex = 1;
            this.typeRemoveBtn.Text = "Remove";
            this.typeRemoveBtn.UseVisualStyleBackColor = true;
            this.typeRemoveBtn.Click += new System.EventHandler(this.typeRemoveBtn_Click);
            // 
            // typesListBox
            // 
            this.typesListBox.FormattingEnabled = true;
            this.typesListBox.Location = new System.Drawing.Point(6, 6);
            this.typesListBox.Name = "typesListBox";
            this.typesListBox.Size = new System.Drawing.Size(312, 186);
            this.typesListBox.TabIndex = 0;
            // 
            // RTTabPage
            // 
            this.RTTabPage.Controls.Add(this.RTPB);
            this.RTTabPage.Controls.Add(this.RTCB);
            this.RTTabPage.Location = new System.Drawing.Point(4, 22);
            this.RTTabPage.Name = "RTTabPage";
            this.RTTabPage.Size = new System.Drawing.Size(405, 277);
            this.RTTabPage.TabIndex = 8;
            this.RTTabPage.Text = "RT";
            this.RTTabPage.UseVisualStyleBackColor = true;
            // 
            // RTPB
            // 
            this.RTPB.Location = new System.Drawing.Point(6, 26);
            this.RTPB.Name = "RTPB";
            this.RTPB.Size = new System.Drawing.Size(227, 164);
            this.RTPB.TabIndex = 1;
            this.RTPB.TabStop = false;
            // 
            // RTCB
            // 
            this.RTCB.AutoSize = true;
            this.RTCB.Location = new System.Drawing.Point(6, 3);
            this.RTCB.Name = "RTCB";
            this.RTCB.Size = new System.Drawing.Size(141, 17);
            this.RTCB.TabIndex = 0;
            this.RTCB.Text = "enable real-time tracking";
            this.RTCB.UseVisualStyleBackColor = true;
            this.RTCB.CheckedChanged += new System.EventHandler(this.RTCB_CheckedChanged);
            // 
            // StatusBox
            // 
            this.StatusBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.StatusBox.Controls.Add(this.Status1);
            this.StatusBox.Location = new System.Drawing.Point(497, 350);
            this.StatusBox.Name = "StatusBox";
            this.StatusBox.Size = new System.Drawing.Size(409, 81);
            this.StatusBox.TabIndex = 4;
            this.StatusBox.TabStop = false;
            this.StatusBox.Text = "Status";
            // 
            // Status1
            // 
            this.Status1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Status1.BackColor = System.Drawing.SystemColors.Control;
            this.Status1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Status1.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Status1.Location = new System.Drawing.Point(6, 12);
            this.Status1.Multiline = true;
            this.Status1.Name = "Status1";
            this.Status1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Status1.Size = new System.Drawing.Size(396, 63);
            this.Status1.TabIndex = 1;
            // 
            // MainStatusStrip
            // 
            this.MainStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusBar,
            this.DroppedFrameStatusLabel});
            this.MainStatusStrip.Location = new System.Drawing.Point(0, 445);
            this.MainStatusStrip.Name = "MainStatusStrip";
            this.MainStatusStrip.Size = new System.Drawing.Size(916, 22);
            this.MainStatusStrip.TabIndex = 5;
            this.MainStatusStrip.Text = "statusStrip1";
            // 
            // StatusBar
            // 
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Size = new System.Drawing.Size(0, 17);
            // 
            // DroppedFrameStatusLabel
            // 
            this.DroppedFrameStatusLabel.AutoSize = false;
            this.DroppedFrameStatusLabel.Name = "DroppedFrameStatusLabel";
            this.DroppedFrameStatusLabel.Size = new System.Drawing.Size(100, 17);
            // 
            // StartBtn
            // 
            this.StartBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.StartBtn.Location = new System.Drawing.Point(827, 321);
            this.StartBtn.Name = "StartBtn";
            this.StartBtn.Size = new System.Drawing.Size(75, 23);
            this.StartBtn.TabIndex = 9;
            this.StartBtn.Text = "Start";
            this.StartBtn.UseVisualStyleBackColor = true;
            this.StartBtn.Click += new System.EventHandler(this.StartBtn_Click);
            // 
            // PanicBtn
            // 
            this.PanicBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PanicBtn.Location = new System.Drawing.Point(578, 321);
            this.PanicBtn.Name = "PanicBtn";
            this.PanicBtn.Size = new System.Drawing.Size(75, 23);
            this.PanicBtn.TabIndex = 7;
            this.PanicBtn.Text = "Panic";
            this.PanicBtn.UseCompatibleTextRendering = true;
            this.PanicBtn.UseVisualStyleBackColor = false;
            this.PanicBtn.Visible = false;
            this.PanicBtn.Click += new System.EventHandler(this.PanicBtn_Click);
            // 
            // twittBtn
            // 
            this.twittBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.twittBtn.Location = new System.Drawing.Point(497, 321);
            this.twittBtn.Name = "twittBtn";
            this.twittBtn.Size = new System.Drawing.Size(75, 23);
            this.twittBtn.TabIndex = 8;
            this.twittBtn.Text = "Twitter";
            this.twittBtn.UseVisualStyleBackColor = true;
            this.twittBtn.Visible = false;
            this.twittBtn.Click += new System.EventHandler(this.twittBtn_Click);
            // 
            // TwitterStatusLabel
            // 
            this.TwitterStatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TwitterStatusLabel.AutoSize = true;
            this.TwitterStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.TwitterStatusLabel.ForeColor = System.Drawing.Color.DarkRed;
            this.TwitterStatusLabel.Location = new System.Drawing.Point(482, 338);
            this.TwitterStatusLabel.Name = "TwitterStatusLabel";
            this.TwitterStatusLabel.Size = new System.Drawing.Size(44, 13);
            this.TwitterStatusLabel.TabIndex = 9;
            this.TwitterStatusLabel.Text = "Offline";
            this.TwitterStatusLabel.Visible = false;
            // 
            // BeepButton
            // 
            this.BeepButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BeepButton.Location = new System.Drawing.Point(746, 321);
            this.BeepButton.Name = "BeepButton";
            this.BeepButton.Size = new System.Drawing.Size(75, 23);
            this.BeepButton.TabIndex = 10;
            this.BeepButton.Text = "Sync";
            this.BeepButton.UseVisualStyleBackColor = true;
            this.BeepButton.Click += new System.EventHandler(this.BeepButton_Click);
            // 
            // ReviveButton
            // 
            this.ReviveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ReviveButton.Location = new System.Drawing.Point(665, 321);
            this.ReviveButton.Name = "ReviveButton";
            this.ReviveButton.Size = new System.Drawing.Size(75, 23);
            this.ReviveButton.TabIndex = 11;
            this.ReviveButton.Text = "Revive";
            this.ReviveButton.UseVisualStyleBackColor = true;
            this.ReviveButton.Click += new System.EventHandler(this.ReviveButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(916, 467);
            this.Controls.Add(this.ReviveButton);
            this.Controls.Add(this.BeepButton);
            this.Controls.Add(this.MainStatusStrip);
            this.Controls.Add(this.TwitterStatusLabel);
            this.Controls.Add(this.twittBtn);
            this.Controls.Add(this.PanicBtn);
            this.Controls.Add(this.StartBtn);
            this.Controls.Add(this.StatusBox);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.videoPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Social Camera";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainForm_Paint);
            this.Move += new System.EventHandler(this.MainForm_Resize);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.videoPanel.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.currentDayCtrl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumberOfDaysUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupNumberUD)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.emailCheck.ResumeLayout(false);
            this.emailCheck.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SnapPictureBox)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.RTTabPage.ResumeLayout(false);
            this.RTTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RTPB)).EndInit();
            this.StatusBox.ResumeLayout(false);
            this.StatusBox.PerformLayout();
            this.MainStatusStrip.ResumeLayout(false);
            this.MainStatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel videoPanel;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker StartTimeDTP;
        private System.Windows.Forms.NumericUpDown GroupNumberUD;
        private System.Windows.Forms.TextBox GroupTypeTB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CameraCB;
        private System.Windows.Forms.Button PathBtn;
        private System.Windows.Forms.TextBox PathTB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox StatusBox;
        private System.Windows.Forms.StatusStrip MainStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel StatusBar;
        private System.Windows.Forms.Button StartBtn;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Panel RecPanel;
        private System.Windows.Forms.TextBox RemarksTB;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Status1;
        private System.Windows.Forms.Button propertiesButton;
        private System.Windows.Forms.TabPage emailCheck;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox smtpPort;
        private System.Windows.Forms.TextBox smtpServer;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox useSSL;
        private System.Windows.Forms.TextBox emailTo;
        private System.Windows.Forms.TextBox emailFrom;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox reportOnError;
        private System.Windows.Forms.Button emailSaveSettings;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.CheckBox transferFilesCB;
        private System.Windows.Forms.TextBox remotePathTB;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown currentDayCtrl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown NumberOfDaysUD;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button refreshLogBtn;
        private System.Windows.Forms.TextBox logTB;
        private System.Windows.Forms.TextBox usernameTB;
        private System.Windows.Forms.TextBox serverTB;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox passwordTB;
        private System.Windows.Forms.Button saveSSHBtn;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button CaptureFrame;
        private System.Windows.Forms.PictureBox SnapPictureBox;
        private System.Windows.Forms.Button PanicBtn;
        private System.Windows.Forms.Button twittBtn;
        private System.Windows.Forms.Label TwitterStatusLabel;
        private System.Windows.Forms.DateTimePicker EndTimeDTP;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox GroupTypeSelectCB;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button typeAddBtn;
        private System.Windows.Forms.Button typeRemoveBtn;
        private System.Windows.Forms.ListBox typesListBox;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Button BeepButton;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private KinectToolbox.KinectControl KinectControl;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TabPage RTTabPage;
        private System.Windows.Forms.PictureBox RTPB;
        private System.Windows.Forms.CheckBox RTCB;
        private System.Windows.Forms.ToolStripStatusLabel DroppedFrameStatusLabel;
        private System.Windows.Forms.Button ReviveButton;

    }
}

