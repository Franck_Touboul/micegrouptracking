﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

namespace MyBugTrapper
{
    static class MyBugTrapper
    {
        private static Mutex bugTrackingMutex = new Mutex(false, "SocialCameraBugTracking");

        public static void SendEmail(String account, String password, String from, String to, String subject, String body)
        {
            try
            {
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                System.Net.NetworkCredential cred = new System.Net.NetworkCredential(account, password);

                mail.To.Add(to);
                mail.Subject = subject;

                mail.From = new System.Net.Mail.MailAddress(from);
                mail.IsBodyHtml = true;
                mail.Body = body;

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.gmail.com");
                smtp.UseDefaultCredentials = false;
                smtp.EnableSsl = true;
                smtp.Credentials = cred;
                smtp.Port = 587;
                smtp.Send(mail);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
            //System.Threading.Thread.Sleep(1000);            
            //MessageBox.Show("hello");
            bugTrackingMutex.WaitOne();
            MessageBox.Show("Error!!!!");

        }
    }
}
