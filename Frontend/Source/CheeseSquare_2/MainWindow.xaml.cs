﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Windows.Threading;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using System.Timers;
//
using Xceed.Wpf.Toolkit;

// DirectShow
using GMFBridgeLib;
using DirectShowLib;
using mf = MediaFoundation;


namespace CheeseSquare
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //
        private const int WS_CHILD = 0x40000000;	// attributes for video window
        private const int WS_CLIPCHILDREN = 0x02000000;
        public const int WM_GRAPHNOTIFY = 0x8000 + 1;
        private const int WM_SYSCOMMAND = 0x0112;
        private const int SC_SCREENSAVE = 0xF140;

        // Shceduled recordings
        DateTime ScheduledDay = DateTime.Now.Date;
        TimeSpan RecordingFrequency = TimeSpan.FromDays(1);
        List<System.Windows.Forms.Timer> TimerList = null;

        List<DateTime> DueDates = new List<DateTime>();

        List<RecordingData> Recordings = new List<RecordingData>();


        // Status
        private int CurrentDay = 1;
        private Boolean IsScheduled = false;
        private Boolean IsRecording = false;

        // thread safety
        private static object VideoLock = new object();

        private System.Windows.Forms.Panel VideoPanel;
        //
        private IGraphBuilder Graph = null;
        private ICaptureGraphBuilder2 Builder = null;
        private IBaseFilter Source = null;
        private IBaseFilter AVIMux = null;
        private IFileSinkFilter FileWriter;
        private DsROTEntry ROT = null;

        // VMR
        private IBaseFilter VMR = null;
        private enum VMRTypes { EVR, VMR9, VMR7 };
        private VMRTypes VMRType = VMRTypes.EVR;
        private mf.EVR.IMFVideoDisplayControl EVRDisplay = null;
        private static object EVRLock = new object();

        // Aux
        private IMediaControl Control = null;
        private IMediaEventEx MediaEventEx = null;
        private string CaptureFilename;
        private IPin PreviewPin;

        // Bridge
        Boolean UseBridge = false;
        private GMFBridgeController Bridge;
        private IBaseFilter PreviewSink;
        private IGraphBuilder PreviewGraph;
        private IBaseFilter StreamSource;
        private ICaptureGraphBuilder2 PreviewCaptureGraph;
        private IMediaControl PreviewControl;

        // Grabber
        public bool EnableGrabber = false;
        private IBaseFilter GrabberFilter;
        private ISampleGrabber Grabber;
        private MyVideoGrabber BufferGrabber;
        public delegate void NewFrameHandler(object sender, NewFrameEventArgs e);
        public event NewFrameHandler OnNewFrame;

        private MyDevice selectedDevice;
        private MyDevice currentDevice;
 

        System.Timers.Timer MaintenanceTimer;

        private string uniqueName = Utilities.GetUniqueKey(5);

        private FileStream logFile = new FileStream("SocialCamera.log", FileMode.Append, FileAccess.Write, FileShare.ReadWrite);

        // Recovery filename (incase of an error)
        string RecoverFromFilename = "";

        System.Windows.Forms.FolderBrowserDialog FolderBrowser = new System.Windows.Forms.FolderBrowserDialog();

        public MainWindow()
        {
            InitializeComponent();
            VideoPanel = VidHost.Child as System.Windows.Forms.Panel;
            string[] args = Environment.GetCommandLineArgs();
            if (args.Count() > 1)
                RecoverFromFilename = args[1];

            Title = "CheeseSquare (Version " + Properties.Settings.Default.Version + ")";
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.Path == "")
                PathTB.Text = System.IO.Directory.GetCurrentDirectory();
            else
                PathTB.Text = Properties.Settings.Default.Path;

            NumberOfDaysUD.Value = Properties.Settings.Default.NumberOfDays;
            StartTimeDTP.Value = Properties.Settings.Default.StartTime;
            EndTimeDTP.Value = Properties.Settings.Default.EndTime;

            PopulateGroupTypes();
            PopulateInputDevices();

            if (RecoverFromFilename != "")
                Recover(RecoverFromFilename);

            MaintenanceTimer = new System.Timers.Timer(500);
            MaintenanceTimer.Elapsed += new System.Timers.ElapsedEventHandler(OnMaintenanceEvent);
            MaintenanceTimer.Enabled = true;
            MaintenanceTimer.Start();

            //HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);
            //source.AddHook(new HwndSourceHook(WndProc));

            Microsoft.Win32.SystemEvents.SessionSwitch += new Microsoft.Win32.SessionSwitchEventHandler(SessionSwitchEvent);

        }

        public void SessionSwitchEvent(object sender, Microsoft.Win32.SessionSwitchEventArgs e)
        {
            //if (e.Reason == Microsoft.Win32.SessionSwitchReason.SessionUnlock)
              //  Window_ContentRendered(sender, null);
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            lock (EVRLock)
            {
                if (EVRDisplay != null)
                {
                    EVRDisplay.RepaintVideo();
                    LoggedMessage("RepaintVideo");
                }
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            logFile.Close();
            logFile = null;
            base.OnClosed(e);
        }

        public void Error(string title, string msg)
        {
            System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackFrame(1, true);

            string method = stackFrame.GetMethod().ToString();
            int line = stackFrame.GetFileLineNumber();
            System.Windows.MessageBox.Show(Environment.NewLine + msg + Environment.NewLine + Environment.NewLine + "[" + method + "; " + line + "]", title, System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
        }


        public void TestForError(string str, int hr)
        {
            try
            {
                DsError.ThrowExceptionForHR(hr);
            }
            catch (Exception e)
            {
                System.Diagnostics.StackFrame stackFrame = new System.Diagnostics.StackFrame(1, true);

                string method = stackFrame.GetMethod().ToString();
                int line = stackFrame.GetFileLineNumber();
                System.Windows.MessageBox.Show(Environment.NewLine + e.Message + Environment.NewLine + Environment.NewLine + "[" + method + "; " + line + "]", str, System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                throw (e);
            }
        }

        private void LoggedMessage(String msg)
        {
#if DEBUG
            StatusTB.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                string status = StatusTB.Text + "D " + msg + System.Environment.NewLine;
                StatusTB.Text = status;
                StatusTB.ScrollToEnd();
            }));
#endif
            if (logFile != null)
            {
                byte[] info = new UTF8Encoding(true).GetBytes("(" + uniqueName + ") " + selectedDevice + " (" + DateTime.Now.ToString("HH:mm:ss.ff dd/MM/yyyy") + "): D " + msg + System.Environment.NewLine);
                logFile.Write(info, 0, info.Length);
            }
        }

        private void Message(String msg)
        {
            StatusTB.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                string status = StatusTB.Text + msg + System.Environment.NewLine;
                StatusTB.Text = status;
                StatusTB.ScrollToEnd();
            }));
            if (logFile != null)
            {
                byte[] info = new UTF8Encoding(true).GetBytes("(" + uniqueName + ") " + selectedDevice + " (" + DateTime.Now.ToString("HH:mm:ss.ff dd/MM/yyyy") + "): - " + msg + System.Environment.NewLine);
                logFile.Write(info, 0, info.Length);
            }
        }

        private void PopulateGroupTypes()
        {
            GroupTypeSelectCB.Items.Clear();
            GroupsLB.Items.Clear();
            foreach (string s in Properties.Settings.Default.GroupTypes)
            {
                Regex type = new Regex(@"\s*\((?<type>\w+)\)");
                GroupTypeSelectCB.Items.Add(s);
                if (s == Properties.Settings.Default.DefaultGroupType)
                    GroupTypeSelectCB.SelectedItem = s;
                GroupsLB.Items.Add(s);
            }
        }

        private void PopulateInputDevices()
        {
            //enumerate Video Input filters
            foreach (MyDevice ds in MyDevice.GetDevices())
            {
                CameraCB.Items.Add(ds);
                if (ds.Path.Equals(Properties.Settings.Default.Camera))
                {
                    CameraCB.Text = ds.Name;
                    selectedDevice = ds;
                }
            }
        }

        public void Recover(string filename)
        {
            try
            {
                XDocument doc = XDocument.Load(filename);
                var data = from item in doc.Descendants("Experiment")
                           select new
                           {
                               Scheduled = item.Element("Scheduled").Value,
                               ScheduledDay = item.Element("ScheduledDay").Value,
                               Device = item.Element("Device").Value,
                               GroupType = item.Element("GroupType").Value,
                               GroupNumber = item.Element("GroupNumber").Value,
                               StartTime = item.Element("StartTime").Value,
                               EndTime = item.Element("EndTime").Value,
                               CurrentDay = item.Element("CurrentDay").Value,
                               TotalNumberOfDays = item.Element("TotalNumberOfDays").Value,
                               Path = item.Element("Path").Value
                           };

                foreach (var parameters in data)
                {
                    ScheduledDay = DateTime.Parse(parameters.ScheduledDay);
                    CameraCB.SelectedItem = CameraCB.FindName(parameters.Device);
                    GroupTypeSelectCB.Text = parameters.GroupType;
                    GroupNumberUD.Value = int.Parse(parameters.GroupNumber);
                    StartTimeDTP.Value = DateTime.Parse(parameters.StartTime);
                    EndTimeDTP.Value = DateTime.Parse(parameters.EndTime);
                    CurrentDayUD.Value = int.Parse(parameters.CurrentDay);
                    NumberOfDaysUD.Value = int.Parse(parameters.TotalNumberOfDays);
                    PathTB.Text = parameters.Path;
                    if (bool.Parse(parameters.Scheduled))
                        StartBTN_Click(null, null);
                    break;
                }
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show("Unable to recover previous session: " + e.Message);
            }
        }

        private void OnMaintenanceEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            /*if (DroppedFrames != null)
            {
                lock (VideoLock)
                {
                    if (DroppedFrames == null)
                        return;
                    StatusTB.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        int nDropped;
                        DroppedFrames.GetNumDropped(out nDropped);
                        InfoTB.Text = String.Format("Dropped frames: {0}", nDropped);
                    }));
                }
            }*/
        }


        private void CreateGraph()
        {
            CreatePreviewGraph();
            StartPreview();
        }

        private void InitializeGraph()
        {
            currentDevice = null;
            CloseInterfaces();
            int hr;
            Graph = (IGraphBuilder)new FilterGraph();
            Control = (IMediaControl)Graph;

            Builder = (ICaptureGraphBuilder2)new CaptureGraphBuilder2();
            LoggedMessage("  controls set");

            hr = Builder.SetFiltergraph(Graph);
            TestForError("Error setting preview graph", hr);

            Source = CreateInputFilter(FilterCategory.VideoInputDevice, selectedDevice);
            currentDevice = selectedDevice;
            hr = Graph.AddFilter(Source, "ESP");
            TestForError("Error adding source filter", hr);

            LoggedMessage("  filter set");


        }

        private void CreatePreviewGraph()
        {
            int hr;
            try
            {
                if (Graph == null || !selectedDevice.Equals(currentDevice))
                    InitializeGraph();
                else
                    TearDownGraph();

                LoggedMessage("creating graph...");

                AddPreviewSubGraph();

                MediaEventEx = (IMediaEventEx)Graph;
                //hr = MediaEventEx.SetNotifyWindow(new WindowInteropHelper(this).Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                //TestForError("Error setting notification window", hr);

                LoggedMessage("  graph done");

            }
            catch (Exception ex)
            {
                CloseInterfaces();
                LoggedMessage("Unable to open capture device '" + selectedDevice + "': " + ex.Message);
            }
        }

        public void HandleGraphEvent()
        {
            /*int hr = 0;
            EventCode evCode;
            IntPtr evParam1, evParam2;

            if (MediaEventEx == null)
                return;

            while (MediaEventEx.GetEvent(out evCode, out evParam1, out evParam2, 0) == 0)
            {

                LoggedMessage("event: " + evCode.ToString() + " with: " + evParam1.ToString() + ", " + evParam2.ToString());
                if (evCode == EventCode.ErrorAbort || evCode == EventCode.ErrorAbortEx) // try to recover
                {
                    RestoreRecording();
                } 
                else if (evCode == DirectShowLib.EventCode.OleEvent)
                {
                    LoggedMessage("params: '" + Marshal.PtrToStringBSTR((IntPtr)evParam1) + ", '" + Marshal.PtrToStringBSTR((IntPtr)evParam2) + "'");
                }
                // Free event parameters to prevent memory leaks associated with
                // event parameter data.  While this application is not interested
                // in the received events, applications should always process them.
                hr = MediaEventEx.FreeEventParams(evCode, evParam1, evParam2);
                DsError.ThrowExceptionForHR(hr);

                // Insert event processing code here, if desired
            }*/
        }


        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case WM_GRAPHNOTIFY:
                    HandleGraphEvent();
                    break;
                case WM_SYSCOMMAND:
                    
                    {
                        int intValue = wParam.ToInt32() & 0xFFF0;
                        if (intValue == SC_SCREENSAVE)
                            LoggedMessage("ScreenSaver");
                    }

                    break;

            }
            return IntPtr.Zero;
        }
        public void CreateCaptureGraph(string filename)
        {
            int hr;

            try
            {
                Control.Stop();
                TearDownGraph();

                Guid guid = DirectShowLib.MediaSubType.Avi;
                hr = Builder.SetOutputFileName(guid, filename, out AVIMux, out FileWriter);
                TestForError("Error setting capture graph", hr);

                // Now tell the AVIMUX to write out AVI files that old apps can read properly.
                // If we don't, most apps won't be able to tell where the keyframes are,
                // slowing down editing considerably
                // Doing this will cause one seek (over the area the index will go) when
                // you capture past 1 Gig, but that's no big deal.
                // NOTE: This is on by default, so it's not necessary to turn it on

                IConfigAviMux configAviMux = AVIMux as IConfigAviMux;
                if (configAviMux != null)
                    configAviMux.SetOutputCompatibilityIndex(false);

                hr = Builder.RenderStream(PinCategory.Capture, MediaType.Video, Source, null, AVIMux);
                TestForError("Encountered stream error", hr);

                AddPreviewSubGraph();

                Control = (IMediaControl)Graph;
                MediaEventEx = (IMediaEventEx)Graph;
                hr = MediaEventEx.SetNotifyWindow(new WindowInteropHelper(this).Handle, WM_GRAPHNOTIFY, IntPtr.Zero);
                TestForError("Error setting notification window", hr);
                LoggedMessage("  graph done");

            }
            catch (Exception e)
            {
                Error("Error", e.Message);
            }
        }

        private IBaseFilter CreateInputFilter(Guid category, MyDevice device)
        {
            try
            {
                LoggedMessage("creating input filter...");
                object source = null;
                Guid iid = typeof(IBaseFilter).GUID;
                {
                    LoggedMessage("  biniding filter");
                    LoggedMessage("  device set: " + device.Name);
                    device.Moniker.BindToObject(null, null, ref iid, out source);
                }
                LoggedMessage("  returning filter");
                return (IBaseFilter)source;
            }
            catch (Exception)
            {
            }
            return null;
        }

        private void DisplayPropertyPage(IBaseFilter dev)
        {
            //Get the ISpecifyPropertyPages for the filter
            ISpecifyPropertyPages pProp = dev as ISpecifyPropertyPages;
            int hr = 0;

            if (pProp == null)
            {
                //If the filter doesn't implement ISpecifyPropertyPages, try displaying IAMVfwCompressDialogs instead!
                IAMVfwCompressDialogs compressDialog = dev as IAMVfwCompressDialogs;
                if (compressDialog != null)
                {

                    hr = compressDialog.ShowDialog(VfwCompressDialogs.Config, IntPtr.Zero);
                    DsError.ThrowExceptionForHR(hr);
                }
                return;
            }

            //Get the name of the filter from the FilterInfo struct
            FilterInfo filterInfo;
            hr = dev.QueryFilterInfo(out filterInfo);
            DsError.ThrowExceptionForHR(hr);

            // Get the propertypages from the property bag
            DsCAUUID caGUID;
            hr = pProp.GetPages(out caGUID);
            DsError.ThrowExceptionForHR(hr);

            // Create and display the OlePropertyFrame
            object oDevice = (object)dev;
            hr = DirectShowTools.OleCreatePropertyFrame(new WindowInteropHelper(this).Handle, 0, 0, filterInfo.achName, 1, ref oDevice, caGUID.cElems, caGUID.pElems, 0, 0, IntPtr.Zero);
            DsError.ThrowExceptionForHR(hr);

            // Release COM objects
            Marshal.FreeCoTaskMem(caGUID.pElems);
            Marshal.ReleaseComObject(pProp);
            if (filterInfo.pGraph != null)
            {
                Marshal.ReleaseComObject(filterInfo.pGraph);
            }
        }

        public void CloseInterfaces()
        {
            StopGraph();
            if (Graph != null)
            {
                Marshal.ReleaseComObject(Graph);
                Graph = null;
            }
            if (Builder != null)
            {
                Marshal.ReleaseComObject(Builder);
                Builder = null;
            }
            if (AVIMux != null)
            {
                Marshal.ReleaseComObject(AVIMux);
                AVIMux = null;
            }
            if (FileWriter != null)
            {
                Marshal.ReleaseComObject(FileWriter);
                FileWriter = null;
            }
            if (Source != null)
            {
                Marshal.ReleaseComObject(Source);
                Source = null;
            }
            ROT = null;
            Control = null;
        }

        private void TearDownGraph()
        {
            StopGraph();
            StopPreview();

            if (Graph != null && Source != null)
                DirectShowTools.RemoveDownstream(ref Graph, Source);
            VMR = null;
            FileWriter = null;
        }

        public void StopGraph()
        {
            if (Control != null)
                Control.Stop();
        }

        public void StopPreview()
        {
            if (PreviewControl != null)
                PreviewControl.Stop();
            VMR = null;
            PreviewControl = null;
            PreviewGraph = null;
            StreamSource = null;
            if (Bridge != null)
            {
                Bridge.BridgeGraphs(null, null);
            }
        }

        public void StartPreview()
        {
            if (UseBridge && Bridge == null)
                return;

            int hr;
            try
            {
                if (UseBridge)
                {
                    PreviewGraph = (IGraphBuilder)new FilterGraph();
                    StreamSource = Bridge.InsertSourceFilter(PreviewSink, PreviewGraph) as IBaseFilter;

                    PreviewCaptureGraph = (ICaptureGraphBuilder2)new CaptureGraphBuilder2();
                    hr = PreviewCaptureGraph.SetFiltergraph(PreviewGraph);
                    TestForError("Error setting capture graph", hr);

                    LoggedMessage("  rendering preview stream");
                    if (VMRType == VMRTypes.EVR)
                        VMR = (IBaseFilter)new mf.EVR.EnhancedVideoRenderer();
                    else
                        VMR = (IBaseFilter)new VideoMixingRenderer9();
                    LoggedMessage("  setting video mixing");
                    hr = PreviewGraph.AddFilter(VMR, "Renderer");
                    TestForError("Error setting preview graph", hr);

                    ConfigureVideoRenderer();

                    hr = PreviewCaptureGraph.RenderStream(null, MediaType.Video, StreamSource, null, VMR);
                    TestForError("Source stream error", hr);

                    PreviewControl = (IMediaControl)PreviewGraph;
                    hr = PreviewControl.Run();
                    TestForError("Error starting color source graph", hr);

                    Bridge.BridgeGraphs(PreviewSink, StreamSource);
                }
                else
                {
                    ConfigureVideoRenderer();
                }

            }
            catch (Exception e)
            {
                Error("Error", e.Message);
            }
        }

        private void AddPreviewSubGraph()
        {
            int hr;
            // Bridge
            if (UseBridge)
            {
                Bridge = new GMFBridgeController();
                Bridge.AddStream(1, eFormatType.eMuxInputs, 1);
                PreviewSink = (IBaseFilter)Bridge.InsertSinkFilter(Graph);
                GrabberFilter = null;
                hr = Builder.RenderStream(PinCategory.Preview, MediaType.Video, Source, GrabberFilter, PreviewSink);
            }
            else
            {
                LoggedMessage("  rendering preview stream");
                if (VMRType == VMRTypes.EVR)
                    VMR = (IBaseFilter)new mf.EVR.EnhancedVideoRenderer();
                else
                    VMR = (IBaseFilter)new VideoMixingRenderer9();
                LoggedMessage("  setting video mixing");
                hr = Graph.AddFilter(VMR, "Renderer");
                TestForError("Error setting preview graph", hr);
                hr = Builder.RenderStream(PinCategory.Preview, MediaType.Video, Source, null, VMR);
                TestForError("Source stream error", hr);
            }


            // set preview framerate
            /*object StreamConfigObj;
            IAMStreamConfig StreamConfig;
            Builder.FindInterface(PinCategory.Preview, MediaType.Video, Source, typeof(IAMStreamConfig).GUID, out StreamConfigObj);
            StreamConfig = StreamConfigObj as IAMStreamConfig;
            AMMediaType mediaType;
            StreamConfig.GetFormat(out mediaType);

            if (mediaType.formatType == DirectShowLib.FormatType.VideoInfo || mediaType.formatType == DirectShowLib.FormatType.MpegVideo)
            {
                VideoInfoHeader infoHeader = (VideoInfoHeader)Marshal.PtrToStructure(mediaType.formatPtr, typeof(VideoInfoHeader));
                infoHeader.AvgTimePerFrame = (10000000 / 15);
                Marshal.StructureToPtr(infoHeader, mediaType.formatPtr, false);
                StreamConfig.SetFormat(mediaType);
            }*/
        }

        private void ConfigureVideoRenderer()
        {
            if (VMRType == VMRTypes.EVR)
            {
                mf.EVR.IMFVideoDisplayControl pDisplay;

                mf.IMFGetService pGetService = null;
                pGetService = (mf.IMFGetService)VMR;

                object vProcessor;
                pGetService.GetService(mf.MFServices.MR_VIDEO_MIXER_SERVICE, typeof(mf.EVR.IMFVideoProcessor).GUID, out vProcessor);
                try
                {
                    ((mf.EVR.IMFVideoProcessor)vProcessor).SetBackgroundColor(244 << 16 | 244 << 8 | 244);
                }
                finally
                {
                    Marshal.ReleaseComObject(vProcessor);
                }

                object serviceObj;
                pGetService.GetService(mf.MFServices.MR_VIDEO_RENDER_SERVICE, typeof(mf.EVR.IMFVideoDisplayControl).GUID, out serviceObj);
                try
                {
                    pDisplay = (mf.EVR.IMFVideoDisplayControl)serviceObj;
                }
                catch
                {
                    Marshal.ReleaseComObject(serviceObj);
                    throw;
                }
                try
                {
                    lock (EVRLock)
                    {
                        //pDisplay.Back(244 << 16 | 244 << 8 | 244);
                        //pDisplay.SetVideoWindow(new WindowInteropHelper(Application.Current.MainWindow).Handle);
                        //System.Windows.Forms.Panel vid = VidHost.Child as System.Windows.Forms.Panel;
                        pDisplay.SetVideoWindow(VideoPanel.Handle);
                        pDisplay.SetRenderingPrefs(mf.EVR.MFVideoRenderPrefs.AllowOutputThrottling | mf.EVR.MFVideoRenderPrefs.AllowBatching);
                        pDisplay.SetBorderColor(217 << 16 | 217 << 8 | 217);
//                        pDisplay.SetAspectRatioMode(mf.EVR.MFVideoAspectRatioMode.PreservePicture);

                        // Set the display position to the entire window.
                        //mf.Misc.MFRect rc = new mf.Misc.MFRect((int)VideoPanel.Margin.Left, (int)VideoPanel.Margin.Top, (int)VideoPanel.Margin.Right, (int)VideoPanel.Margin.Bottom);
                        //pDisplay.SetVideoPosition(null, rc);
                        EVRDisplay = pDisplay;
                    }
                }
                finally
                {
                    Marshal.ReleaseComObject(pDisplay);
                }
            }
            else
            {

                int hr = 0;
                IVMRFilterConfig9 filterConfig = VMR as IVMRFilterConfig9;

                // Configure VMR-9 in Windowsless mode
                hr = filterConfig.SetRenderingMode(VMR9Mode.Windowless);
                TestForError("Error!", hr);

                // With 2 input stream
                hr = filterConfig.SetNumberOfStreams(1);
                TestForError("Error!", hr);

                IVMRWindowlessControl9 windowlessControl = VMR as IVMRWindowlessControl9;
                if (windowlessControl != null)
                {
                    // Keep the aspect-ratio OK
                    hr = windowlessControl.SetAspectRatioMode(VMR9AspectRatioMode.LetterBox);
                    TestForError("Error!", hr);

                    hr = windowlessControl.SetBorderColor(244 << 16 | 244 << 8 | 244);
                    TestForError("Error!", hr);

                    IntPtr handle = VideoPanel.Handle;
                    //IntPtr handle = new WindowInteropHelper(Application.Current.MainWindow).Handle;
                    hr = windowlessControl.SetVideoClippingWindow(handle);
                    TestForError("Error!", hr);
                }
                // Init the VMR-9 with default size values
            }
            ResizeVideoWindow();
        }

        private void ResizeVideoWindow()
        {
            if (VMR != null)
            {
                int hr = 0;
                if (VMRType == VMRTypes.VMR9)
                {
                    // The main form is hosting the VMR-9
                    IVMRWindowlessControl9 windowlessControl = VMR as IVMRWindowlessControl9;

                    if (windowlessControl != null)
                    {
                        //System.Windows.Point offset = TransformToAncestor(Application.Current.MainWindow).Transform(new System.Windows.Point(VidHost.Margin.Left, VidHost.Margin.Top));
                        //hr = windowlessControl.SetVideoPosition(null, new DsRect((int)offset.X + 1, (int)offset.Y + 1, (int)(VidHost.ActualWidth + offset.X - 1), (int)(VidHost.ActualHeight + offset.Y - 1)));
                        hr = windowlessControl.SetVideoPosition(null, new DsRect(0, 0, (int)(VidHost.ActualWidth), (int)(VidHost.ActualHeight)));
                    }
                }
                else if (VMRType == VMRTypes.EVR)
                {
                    lock (EVRLock)
                    {
                        //System.Windows.Point offset = TransformToAncestor(Application.Current.MainWindow).Transform(new System.Windows.Point(VidHost.Margin.Left, VidHost.Margin.Top));
                        //mf.Misc.MFRect src = new mf.Misc.MFRect((int)offset.X + 1, (int)offset.Y + 1, (int)(VidHost.ActualWidth + offset.X - 1), (int)(VidHost.ActualHeight + offset.Y - 1));
                        mf.Misc.MFRect src = new mf.Misc.MFRect(0, 0, (int)(VidHost.ActualWidth), (int)(VidHost.ActualHeight));
                        mf.Misc.MFRect dst = new mf.Misc.MFRect();
                        EVRDisplay.GetVideoPosition(null, dst);

                        if (dst != src)
                            EVRDisplay.SetVideoPosition(null, src);
                    }
                }
            }
        }

        private string GetCurrentGroupTypeName()
        {
            Regex type = new Regex(@"\((?<type>\w+)\)");
            return type.Match(GroupTypeSelectCB.Text).Groups["type"].Value;
        }

        private void MakeNewCaptureFile()
        {
            Match indexMatch = Regex.Match(currentDevice.Name, "[0-9]*$");
            int cameraIndex = 0;
            if (indexMatch.Length > 0)
                cameraIndex = int.Parse(indexMatch.Value) - 1;
            string sBaseName = String.Format(GetCurrentGroupTypeName() + ".exp{0:D4}.day{1:D2}.cam{2:D2}",
                System.Decimal.ToInt32(GroupNumberUD.Value.Value),
                CurrentDay,
                cameraIndex + 1);

            // Find an unused file name
            int indexFile = 0;
            string sFileName = string.Format(@"{0}\{1}.avi", FolderBrowser.SelectedPath, sBaseName);
            while (File.Exists(sFileName))
            {
                sFileName = string.Format(@"{0}\{1}.{2}.avi", FolderBrowser.SelectedPath, sBaseName, ++indexFile);
            }
            CaptureFilename = sFileName;
        }

        private void WriteMetaData()
        {
            XDocument doc = new XDocument(
                     new XDeclaration("1.0", "utf-8", "yes"),
                     new XElement("Experiment",
                         new XElement("Start", DateTime.Now),
                         new XElement("Remarks", RemarksTB.Text)
                        ));
            doc.Save(CaptureFilename + ".xml");
        }

        /*private void TimerStopCapture(Object myObject, EventArgs myEventArgs)
        {
            System.Windows.Forms.Timer timer = myObject as System.Windows.Forms.Timer;
            if (timer != null)
                timer.Stop();
            StopRecording();

            foreach (DateTime nextEvent in DueDates)
            {
                if (nextEvent.CompareTo(DateTime.Now) >= 0)
                {
                    LoggedMessage("next event is scheduled to " + nextEvent.ToString());
                    return;
                }
            }
            LoggedMessage("no more scheduled events");
            IsScheduled = false;
            StartBTN.Content = "Start";
            ToggleOptionsControl(true);
        }

        private void TimerStartCapture(Object myObject, EventArgs myEventArgs)
        {
            ((System.Windows.Forms.Timer)myObject).Stop();
            CurrentDayUD.Value = DateTime.Now.Date.Subtract(ScheduledDay).Days + 1;
            StartRecording();
        }*/

        void SetDay(int day)
        {
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                CurrentDayUD.Value = day;
            }));
        }

        void RestoreRecording()
        {
            for (int i=0; i<Recordings.Count; ++i)
            {
                if (Recordings[i].type == RecordingData.EventType.Start && 
                    Recordings[i].dueDate < DateTime.Now &&
                    Recordings[i + 1].dueDate > DateTime.Now)
                {
                    Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        CurrentDayUD.Value = Recordings[i].day;
                        StartRecording();
                    }));
                    return;
                }
            }
            EndRecording();
        }

        void RunCommand(string cmd)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/C " + cmd;
            process.StartInfo = startInfo;
            process.Start();
        }

        void StartRecording(object sender, ElapsedEventArgs eea)
        {
            int index = Recordings.FindIndex(delegate(RecordingData r) { return r.timer == (Timer)sender; });
            int day = Recordings[index].day;
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                CurrentDayUD.Value = day;
                StartRecording();
            }));

            try
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    if (RecStartCB.IsChecked.Value)
                        RunCommand(RecStartTB.Text);
                }));
            }
            catch (Exception e)
            {
                LoggedMessage("Failed command: " + e.Message);
            }
        }

        void EndRecording()
        {
            StopRecording();
            foreach (DateTime nextEvent in DueDates)
            {
                if (nextEvent.CompareTo(DateTime.Now) >= 0)
                {
                    Message("next event is scheduled to " + nextEvent.ToString());
                    return;
                }
            }
            LoggedMessage("no more scheduled events");
            IsScheduled = false;
            StartBTN.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                StartBTN.Content = "Start";
                ToggleOptionsControl(true);
            }));
            SetDay(1);

        }

        private void ScheduleCapture()
        {
            int day = CurrentDayUD.Value.Value;
            int nDays = NumberOfDaysUD.Value.Value;
            int index = 0;
            DateTime? futureEvent = null;
            bool isFuture = false;

            for (; day <= nDays; ++day)
            {
                DateTime startDate = DateTime.Today.AddDays(index).Add(StartTimeDTP.Value.Value.TimeOfDay);
                DateTime endDate   = DateTime.Today.AddDays(index).Add(EndTimeDTP  .Value.Value.TimeOfDay);
                DueDates.Add(startDate);
                DueDates.Add(endDate);

                Timer startRec = new Timer();
                double startInterval = startDate.Subtract(DateTime.Now).TotalMilliseconds;
                startRec.AutoReset = false;
                startRec.Elapsed += new ElapsedEventHandler(StartRecording); //delegate { StartRecording(day); };

                Timer endRec = new Timer();
                double endInterval = endDate.Subtract(DateTime.Now).TotalMilliseconds;
                endRec.AutoReset = false;
                endRec.Elapsed += delegate { EndRecording(); };

                if ((isFuture || index == 0 && startInterval > 1) && futureEvent == null)
                    futureEvent = startDate;

                if (endInterval > 1)
                {
                    startRec.Interval = startInterval > 1 ? startInterval : 1;
                    endRec.Interval = endInterval;
                    Recordings.Add(new RecordingData(RecordingData.EventType.Start, startRec, startDate, day));
                    Recordings.Add(new RecordingData(RecordingData.EventType.End, endRec, endDate, day));
                    startRec.Enabled = true;
                    endRec.Enabled = true;
                }
                else 
                    isFuture = true;
                ++index;
            }
            if (futureEvent != null)
                Message("next event is scheduled to " + futureEvent.Value.ToString());
            StartBTN.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                StartBTN.Content = "Cancel";
                ToggleOptionsControl(false);
            }));
            IsScheduled = true;


        }

        private void CancelScheduledCapture()
        {
            StopRecording();
            foreach (RecordingData r in Recordings)
                r.timer.Stop();
            LoggedMessage("capturing is canceled!");
            Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                StartBTN.Content = "Start";
                ToggleOptionsControl(true);
            }));
            IsScheduled = false;
        }

        private void ToggleOptionsControl(bool enable)
        {
            //tabControl.Enabled = enable;
            foreach (Control item in MainTC.Items)
            {
                item.IsEnabled = enable;
            }
            Utilities.IsEnabledOnTree(MainTC, enable);
        }

        private void StartRecording()
        {
            try
            {
                lock (VideoLock)
                {

                    MakeNewCaptureFile();
                    WriteMetaData();
                    LoggedMessage("saving to " + CaptureFilename);
                        RecPanel.Visibility = System.Windows.Visibility.Visible;
                        CreateCaptureGraph(CaptureFilename);
                        if (Graph != null)
                        {
                            LoggedMessage("starting graph");
                            Control.Run();
                            StartPreview();
                        }
                }
            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show("Unable to start recording: " + e.Message);
            }
        }

        private void StopRecording()
        {
            try
            {
                lock (VideoLock)
                {
                    Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                    RecPanel.Visibility = System.Windows.Visibility.Hidden;
                        CreatePreviewGraph();
                        if (Graph != null)
                        {
                            LoggedMessage("starting graph");
                            int hr = Control.Run();
                            StartPreview();
                        }
                    }));

                    try
                    {
                        Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                        {
                            if (RecStopCB.IsChecked.Value)
                                RunCommand(RecStopTB.Text);
                        }));
                    }
                    catch (Exception e)
                    {
                        LoggedMessage("Failed command: " + e.Message);
                    }

                }

            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show("Unable to stop recording: " + e.Message);
            }
        }

        private void CameraCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Create the filter for the selected video input device
            try
            {
                lock (VideoLock)
                {
                    selectedDevice = (MyDevice)CameraCB.SelectedItem;
                    CreateGraph();
                    if (Graph != null)
                    {
                        LoggedMessage("starting graph");
                        int hr = Control.Run();
#if DEBUG
                        FilterState fs;
                        hr = Control.GetState(1000, out fs);
                        LoggedMessage("  graph status: " + fs.ToString() + "(" + hr.ToString() + ")");
                        ROT = new DsROTEntry(Graph);

#endif
                    }
                    if (selectedDevice != null)
                    {
                        Properties.Settings.Default.Camera = selectedDevice.Path;
                        Properties.Settings.Default.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggedMessage("Unable to open capture device '" + selectedDevice + "': " + ex.Message);
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ResizeVideoWindow();
        }

        private void StartBTN_Click(object sender, RoutedEventArgs e)
        {
            if ((string)StartBTN.Content == "Start")
            {
                ScheduleCapture();
                StartBTN.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                {
                    StartBTN.Background = new SolidColorBrush(Color.FromRgb(251, 185, 169));
                    StartBTN.Foreground = new SolidColorBrush(Color.FromRgb(153, 63, 37));
                }));
            }
            else
            {
                CancelScheduledCapture();
                StartBTN.Background = new SolidColorBrush(Color.FromRgb(215, 227, 163));
                StartBTN.Foreground = new SolidColorBrush(Color.FromRgb(92, 106, 47));
            }
        }

        private void PathBTN_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowser.SelectedPath = PathTB.Text;
            System.Windows.Forms.DialogResult result = FolderBrowser.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
                PathTB.Text = FolderBrowser.SelectedPath;
        }

        private void PathTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            FolderBrowser.SelectedPath = PathTB.Text;
            Properties.Settings.Default.Path = PathTB.Text;
            Properties.Settings.Default.Save();
        }

        private void DevicePropertiesBTN_Click(object sender, RoutedEventArgs e)
        {
            if (Source != null)
                DisplayPropertyPage(Source);
        }

        private void CurrentDayUD_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            CurrentDay = Decimal.ToInt32(CurrentDayUD.Value.Value);
        }

        private void StartTimeDTP_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Properties.Settings.Default.StartTime = StartTimeDTP.Value.Value;
            Properties.Settings.Default.Save();
        }

        private void EndTimeDTP_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Properties.Settings.Default.EndTime = EndTimeDTP.Value.Value;
            Properties.Settings.Default.Save();
        }

        private void NumberOfDaysUD_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            Properties.Settings.Default.NumberOfDays = Decimal.ToInt32(NumberOfDaysUD.Value.Value);
            Properties.Settings.Default.Save();
        }

        private void GroupTypeSelectCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Properties.Settings.Default.DefaultGroupType = (string)GroupTypeSelectCB.SelectedItem;
            Properties.Settings.Default.Save();
        }

        private void GroupRemoveBTN_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.GroupTypes.Remove(GroupsLB.SelectedItem.ToString());
            Properties.Settings.Default.Save();
            PopulateGroupTypes();
        }

        private void GroupAddBTN_Click(object sender, RoutedEventArgs e)
        {
            if (NewGroupNameTB.Text.Length > 0 && NewGroupDescTB.Text.Length > 0)
            {
                Properties.Settings.Default.GroupTypes.Add(NewGroupDescTB.Text + " (" + NewGroupNameTB.Text + ")");
                Properties.Settings.Default.Save();
                PopulateGroupTypes();
                NewGroupNameTB.Text = "";
                NewGroupDescTB.Text = "";
            }
        }

        private void NewGroupNameTB_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void NewGroupNameTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex r = new Regex("^[0-9]+");
            if (r.IsMatch(NewGroupNameTB.Text))
            {
                NewGroupNameTB.Text = r.Replace(NewGroupNameTB.Text, "");
            }
        }

        private void NewGroupDescTB_KeyDown(object sender, KeyEventArgs e)
        {
            char ch = Utilities.GetCharFromKey(e.Key);
            if (ch == '(' || ch == ')')
                e.Handled = true;
        }

        private void NewGroupDescTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex r = new Regex(@"[\(\)]");
            if (r.IsMatch(NewGroupDescTB.Text))
            {
                NewGroupDescTB.Text = r.Replace(NewGroupDescTB.Text, "");
            }
        }

        private void RecStartCB_Checked(object sender, RoutedEventArgs e)
        {
            if (RecStartCB.IsChecked.Value)
                RecStartTB.IsEnabled = true;
            else
                RecStartTB.IsEnabled = false;
        }

        private void RecStopCB_Checked(object sender, RoutedEventArgs e)
        {
            if (RecStopCB.IsChecked.Value)
                RecStopTB.IsEnabled = true;
            else
                RecStopTB.IsEnabled = false;
        }

        private void NewGroupNameTB_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            char ch = Utilities.GetCharFromKey(e.Key);
            if (Char.IsNumber(ch))
            {
                if (NewGroupNameTB.Text.Length == 0)
                    e.Handled = true;
            }
            else if (!(Char.IsLetterOrDigit(ch) || char.IsControl(ch)))
                e.Handled = true;

        }




    }



    public class NewFrameEventArgs : EventArgs
    {
        public MyVideoGrabber Grabber { get; private set; }

        public NewFrameEventArgs(MyVideoGrabber grabber)
        {
            Grabber = grabber;
        }
    }

    public class RecordingData
    {
        public RecordingData(EventType type_, Timer timer_, DateTime dueDate_, int day_)
        {
            type = type_;
            timer = timer_;
            dueDate = dueDate_;
            day = day_;
        }
        public enum EventType { Start, End };
        public Timer timer;
        public DateTime dueDate;
        public int day;
        public EventType type;
    }
}
