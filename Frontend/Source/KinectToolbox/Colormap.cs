﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KinectToolbox
{
    class Colormap
    {
        static public byte[,] map = new byte[,]{ { 0, 0, 144}, 
        { 0, 0, 160}, 
        { 0, 0, 176}, 
        { 0, 0, 192}, 
        { 0, 0, 208}, 
        { 0, 0, 224}, 
        { 0, 0, 240}, 
        { 0, 0, 255}, 
        { 0, 16, 255}, 
        { 0, 32, 255}, 
        { 0, 48, 255}, 
        { 0, 64, 255}, 
        { 0, 80, 255}, 
        { 0, 96, 255}, 
        { 0, 112, 255}, 
        { 0, 128, 255}, 
        { 0, 144, 255}, 
        { 0, 160, 255}, 
        { 0, 176, 255}, 
        { 0, 192, 255}, 
        { 0, 208, 255}, 
        { 0, 224, 255}, 
        { 0, 240, 255}, 
        { 0, 255, 255}, 
        { 16, 255, 240}, 
        { 32, 255, 224}, 
        { 48, 255, 208}, 
        { 64, 255, 192}, 
        { 80, 255, 176}, 
        { 96, 255, 160}, 
        { 112, 255, 144}, 
        { 128, 255, 128}, 
        { 144, 255, 112}, 
        { 160, 255, 96}, 
        { 176, 255, 80}, 
        { 192, 255, 64}, 
        { 208, 255, 48}, 
        { 224, 255, 32}, 
        { 240, 255, 16}, 
        { 255, 255, 0}, 
        { 255, 240, 0}, 
        { 255, 224, 0}, 
        { 255, 208, 0}, 
        { 255, 192, 0}, 
        { 255, 176, 0}, 
        { 255, 160, 0}, 
        { 255, 144, 0}, 
        { 255, 128, 0}, 
        { 255, 112, 0}, 
        { 255, 96, 0}, 
        { 255, 80, 0}, 
        { 255, 64, 0}, 
        { 255, 48, 0}, 
        { 255, 32, 0}, 
        { 255, 16, 0}, 
        { 255, 0, 0}, 
        { 240, 0, 0}, 
        { 224, 0, 0}, 
        { 208, 0, 0}, 
        { 192, 0, 0}, 
        { 176, 0, 0}, 
        { 160, 0, 0}, 
        { 144, 0, 0}, 
        { 128, 0, 0}};
    }
}
