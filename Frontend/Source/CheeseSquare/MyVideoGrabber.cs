﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Runtime.InteropServices;
using System.Drawing;
using DirectShowLib;

namespace CheeseSquare
{
    public interface IMyVideoGrabber
    {
        void HandleNewVideoFrame();
    }

    public class MyVideoGrabber : ISampleGrabberCB
    {
        VideoInfoHeader InfoHeader;
        private byte[] FrameBuffer;
        static private Mutex mut = new Mutex();
        private bool Waiting = false;

        public Bitmap image = null;
        private IMyVideoGrabber Handle = null;

        public MyVideoGrabber(IMyVideoGrabber handle, VideoInfoHeader infoHeader)
        {
            InfoHeader = infoHeader;
            Handle = handle;
        }


        int ISampleGrabberCB.SampleCB(double SampleTime, IMediaSample pSample)
        {
            return 0;
        }

        Semaphore pool = new Semaphore(0, 1);

        public Bitmap GetFrame()
        {
            Waiting = true;
            pool.WaitOne();
            return image;
        }

        int ISampleGrabberCB.BufferCB(double SampleTime, IntPtr pBuffer, int BufferLen)
        {
            mut.WaitOne();
            try
            {
                if (!Waiting)
                    return 0;
                /*mut.WaitOne();
                if (!Waiting)
                    return 0;*/
                if (FrameBuffer == null || BufferLen > FrameBuffer.Length)
                    FrameBuffer = new byte[BufferLen];
                Marshal.Copy(pBuffer, FrameBuffer, 0, BufferLen);
                GCHandle handle = GCHandle.Alloc(FrameBuffer, GCHandleType.Pinned);
                int scan0 = (int)handle.AddrOfPinnedObject();
                int stride = (InfoHeader.BmiHeader.BitCount / 8) * InfoHeader.BmiHeader.Width;
                //scan0 += (InfoHeader.BmiHeader.Height - 1) * stride;
                image = new Bitmap(InfoHeader.BmiHeader.Width, InfoHeader.BmiHeader.Height > 0 ? InfoHeader.BmiHeader.Height : -InfoHeader.BmiHeader.Height, stride, System.Drawing.Imaging.PixelFormat.Format24bppRgb, (IntPtr)scan0);
                image.RotateFlip(RotateFlipType.RotateNoneFlipY);
                handle.Free();
                //VideoControl.SetFrame(bmp);
                Waiting = false;
                pool.Release();
                //NewFrameEventArgs args = new NewFrameEventArgs(this);
                Handle.HandleNewVideoFrame();
            }
            finally
            {
                mut.ReleaseMutex();
            }
            return 0;
        }

    }
}
