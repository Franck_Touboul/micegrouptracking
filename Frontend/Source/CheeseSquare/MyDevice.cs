﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using DirectShowLib;

namespace CheeseSquare
{
    class MyDevice
    {
        private DsDevice DirectShowDevice;

        public string Name   // the Name property
        {
            get
            {
                return DirectShowDevice.Name;
            }
        }

        public string Path   // the Name property
        {
            get
            {
                return DirectShowDevice.DevicePath;
            }
        }


        public UCOMIMoniker Moniker   // the Name property
        {
            get
            {
                return (UCOMIMoniker)DirectShowDevice.Mon;
            }
        }

        public MyDevice(DsDevice ds)
        {
            DirectShowDevice = ds;
        }

        public static implicit operator MyDevice(DsDevice ds)
        {
            return new MyDevice(ds);
        }

        public static MyDevice[] GetDevices()
        {
            DsDevice[] dsDevices = DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice);
            MyDevice[] myDevices = new MyDevice[dsDevices.Length];
            int index = 0;
            foreach (DsDevice ds in dsDevices)
            {
                myDevices[index] = ds;
                ++index;
            }
            return myDevices;
        }

        public override string ToString()
        {
            return DirectShowDevice.Name;
        }
    }
}
