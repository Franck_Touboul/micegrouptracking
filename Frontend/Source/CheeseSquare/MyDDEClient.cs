﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NDde.Client;

namespace CheeseSquare
{
    public sealed class MyDDEClient
    {
        private DdeClient Client = null;
        public void Connect(string appname, string topic)
        {
            if (Client == null || Client.Topic != topic || Client.Service != appname)
            {
                try
                {
                    MainWindow.LoggedMessage("DDE connecting to " + appname + ", topic is " + topic);
                    Client = new DdeClient(appname, topic);
                    // Subscribe to the Disconnected event.  This event will notify the application when a conversation has been terminated.
                    Client.Disconnected += OnDisconnected;
                    // Connect to the server.  It must be running or an exception will be thrown.
                    Client.Connect();
                }
                catch (Exception e)
                {
                    MainWindow.LoggedMessage("DDE connect failed " + e.ToString());
                    Client = null;
                }
            }
        }

        public void Connect(string str)
        {
            string[] parts = str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                if (parts.Length == 3)
                    Connect(parts[0], parts[1]);
                else
                    MainWindow.LoggedMessage("Failed to connect to DDE '" + str + "'");
            }
            catch (Exception e)
            {
                MainWindow.LoggedMessage("Failed to connect to DDE '" + str + "'; " + e.Message);
            }

        }

        public void Run(string str)
        {
            string[] parts = str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                if (parts.Length == 3)
                    Run(parts[0], parts[1], parts[2]);
                else
                    MainWindow.LoggedMessage("Failed to run logged command '" + str + "'");
            }
            catch (Exception e)
            {
                MainWindow.LoggedMessage("Failed to run logged command '" + str + "'; " + e.Message);
            }
        }


        public void Run(string appname, string topic, string command)
        {
            Connect(appname, topic);

            // Wait for the user to press ENTER before proceding.
            Console.WriteLine("The Server sample must be running before the client can connect.");
            try
            {
                // Synchronous Execute Operation
                Client.Execute(command, 60000);

                // Asynchronous Execute Operation
                //client.BeginExecute(command, OnExecuteComplete, client);
            }
            catch (Exception e)
            {
                MainWindow.LoggedMessage("DDE execute failed " + e.ToString());
            }
        }

        private void OnExecuteComplete(IAsyncResult ar)
        {
            try
            {
                DdeClient client = (DdeClient)ar.AsyncState;
                client.EndExecute(ar);
                MainWindow.LoggedMessage("DDE execute complete");
            }
            catch (Exception e)
            {
                MainWindow.LoggedMessage("DDE execute failed: " + e.Message);
            }
        }

        private void OnPokeComplete(IAsyncResult ar)
        {
            try
            {
                DdeClient client = (DdeClient)ar.AsyncState;
                client.EndPoke(ar);
                Console.WriteLine("OnPokeComplete");
            }
            catch (Exception e)
            {
                Console.WriteLine("OnPokeComplete: " + e.Message);
            }
        }

        private void OnRequestComplete(IAsyncResult ar)
        {
            try
            {
                DdeClient client = (DdeClient)ar.AsyncState;
                byte[] data = client.EndRequest(ar);
                Console.WriteLine("OnRequestComplete: " + Encoding.ASCII.GetString(data));
            }
            catch (Exception e)
            {
                Console.WriteLine("OnRequestComplete: " + e.Message);
            }
        }

        private void OnStartAdviseComplete(IAsyncResult ar)
        {
            try
            {
                DdeClient client = (DdeClient)ar.AsyncState;
                client.EndStartAdvise(ar);
                Console.WriteLine("OnStartAdviseComplete");
            }
            catch (Exception e)
            {
                Console.WriteLine("OnStartAdviseComplete: " + e.Message);
            }
        }

        private void OnStopAdviseComplete(IAsyncResult ar)
        {
            try
            {
                DdeClient client = (DdeClient)ar.AsyncState;
                client.EndStopAdvise(ar);
                Console.WriteLine("OnStopAdviseComplete");
            }
            catch (Exception e)
            {
                Console.WriteLine("OnStopAdviseComplete: " + e.Message);
            }
        }

        private void OnAdvise(object sender, DdeAdviseEventArgs args)
        {
            Console.WriteLine("OnAdvise: " + args.Text);
        }

        private void OnDisconnected(object sender, DdeDisconnectedEventArgs args)
        {
            MainWindow.LoggedMessage(
                "OnDisconnected: " +
                "IsServerInitiated=" + args.IsServerInitiated.ToString() + " " +
                "IsDisposed=" + args.IsDisposed.ToString());
            Client = null;
        }

    } // class
}
