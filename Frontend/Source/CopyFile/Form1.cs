﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CopyFile
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static void CopyStreamToStream(
Stream source, Stream destination,
Action<Stream, Stream, Exception> completed)
        {
            byte[] buffer = new byte[0x1000];
            int read;
            try
            {
                while ((read = source.Read(buffer, 0, buffer.Length)) > 0)
                {
                    destination.Write(buffer, 0, read);
                }
                if (completed != null) completed(source, destination, null);
            }
            catch (Exception exc)
            {
                if (completed != null) completed(source, destination, exc);
            }
        }

    }
}
