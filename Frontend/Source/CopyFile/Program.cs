﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;

namespace CopyFile
{
    class Program
    {
        public static void CopyStreamToStream(
        Stream source, Stream destination,
        Action<Stream, Stream, Exception> completed)
        {
            byte[] buffer = new byte[0x1000];
            int read;
            try
            {
                while ((read = source.Read(buffer, 0, buffer.Length)) > 0)
                {
                    destination.Write(buffer, 0, read);
                }
                if (completed != null) completed(source, destination, null);
            }
            catch (Exception exc)
            {
                if (completed != null) completed(source, destination, exc);
            }
        }

        static void Main(string[] args)
        {
            if (args.Length < 2) {
                Console.WriteLine("syntax: <source file> <dest file>");
                return;
            }
            FileStream input = new FileStream(args[0],FileMode.Open);
            FileStream output = new FileStream(args[1], FileMode.Create); 
            CopyStreamToStream(input, output, (src,dst,exc) => {
                src.Close();
                dst.Close();
            });

            ThreadPool.QueueUserWorkItem(delegate
            {
                CopyStreamToStream(input, output, (src, dst, exc) =>
                {
                    src.Close();
                    dst.Close();
                });
            });
        }
    }
}
