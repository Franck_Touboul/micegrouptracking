﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace CopyFileDialog
{
    public partial class Form1 : Form
    {
        TextBox currSource;
        TextBox currDest;
        TextBox currFilesize;
        TextBox currMessage;
        ProgressBar currProgBar;

        public void CopyStreamToStream(
        Stream source, Stream destination,
        Action<Stream, Stream, Exception> completed)
        {
            byte[] buffer = new byte[0x1000];
            int read;
            try
            {
                while ((read = source.Read(buffer, 0, buffer.Length)) > 0)
                {
                    destination.Write(buffer, 0, read);
                    progressBar.Value = (int)(((double)source.Position / source.Length) * 100);
                    this.Text = "Copying... (" + progressBar.Value.ToString() + "% completed)";
                }
                if (completed != null) completed(source, destination, null);
            }
            catch (Exception exc)
            {
                if (completed != null) completed(source, destination, exc);
            }
        }

        private string GetFileSize(double byteCount)
        {
            string size = "0 Bytes";
            if (byteCount >= 1073741824.0)
                size = String.Format("{0:##.##}", byteCount / 1073741824.0) + " GB";
            else if (byteCount >= 1048576.0)
                size = String.Format("{0:##.##}", byteCount / 1048576.0) + " MB";
            else if (byteCount >= 1024.0)
                size = String.Format("{0:##.##}", byteCount / 1024.0) + " KB";
            else if (byteCount > 0 && byteCount < 1024.0)
                size = byteCount.ToString() + " Bytes";

            return size;
        }

        TextBox getTextField()
        {
            TextBox box = new TextBox();
            box.BorderStyle = BorderStyle.None;
            box.Dock = DockStyle.Fill;
            box.ReadOnly = true;
            box.BackColor = Color.White;
            return box;
        }

        public void Copy(string inputFile, string outputFile)
        {
            try
            {
                currSource = getTextField(); 
                currSource.Text = inputFile;

                currDest = getTextField(); 
                currDest.Text = outputFile;

                currProgBar = new ProgressBar();
                currProgBar.Height = 16;
                currProgBar.Dock = DockStyle.Fill;

                currMessage = getTextField();
                currMessage.Text = "";

                currFilesize = getTextField(); 

                histroyTable.RowStyles[histroyTable.RowCount-1].SizeType = SizeType.Absolute;
                histroyTable.RowStyles[histroyTable.RowCount-1].Height = 20;
                histroyTable.RowStyles.Add(new RowStyle(SizeType.Absolute, 20));
                histroyTable.RowCount++;
                histroyTable.Controls.Add(currSource, 0, histroyTable.RowCount-2);
                histroyTable.Controls.Add(currDest, 1, histroyTable.RowCount-2);
                histroyTable.Controls.Add(currFilesize, 2, histroyTable.RowCount - 2);
                histroyTable.Controls.Add(currMessage, 4, histroyTable.RowCount - 2);
                histroyTable.Controls.Add(currProgBar, 3, histroyTable.RowCount - 2);

                FileStream input = new FileStream(inputFile, FileMode.Open);
                FileStream output = new FileStream(outputFile, FileMode.Create);

                currFilesize.Text = GetFileSize(input.Length);

                sourceLbl.Text = inputFile;
                destLbl.Text = outputFile;
                CopyStreamToStream(input, output, (src, dst, exc) =>
                {
                    src.Close();
                    dst.Close();
                    if (exc != null)
                    {
                        //this.Text = "Copying Failed!";
                        messageLbl.Text = exc.Message;
                    }
                });

                //histroyTable.ColumnCount = 4;
                //histroyTable.RowCount = 1;
                //histroyTable.GrowStyle = TableLayoutPanelGrowStyle.AddRows;

                /*ThreadPool.QueueUserWorkItem(delegate
                {
                    CopyStreamToStream(input, output, (src, dst, exc) =>
                    {
                        src.Close();
                        dst.Close();
                    });
                });*/
            }
            catch (Exception exc)
            {
                //this.Text = "Copying Failed!";
                messageLbl.Text = exc.Message;
                progressBar.Visible = false;
                currMessage.Visible = true;
                currMessage.Text = "Failed: " + exc.Message;
            }
        }

        public Form1(string inputFile, string outputFile)
        {
            InitializeComponent();


            histroyTable.BackColor = Color.White;
            histroyTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;

            Label source = new Label();
            source.Text = "From"; 
            source.TextAlign = ContentAlignment.MiddleLeft;
            source.Dock = DockStyle.Fill;
            //source.fo = new Font(Font.SystemFontName, 16, FontStyle.Bold);

            Label dest = new Label(); 
            dest.Text = "To"; 
            dest.TextAlign = ContentAlignment.MiddleLeft;
            //dest.Font.Style = FontStyle.Bold;
            dest.Dock = DockStyle.Fill;

            Label prog = new Label();
            prog.Text = "Progress";
            prog.TextAlign = ContentAlignment.MiddleLeft;
            //dest.Font.Style = FontStyle.Bold;
            prog.Dock = DockStyle.Fill;

            Label size = new Label();
            size.Text = "Size";
            size.TextAlign = ContentAlignment.MiddleLeft;
            //dest.Font.Style = FontStyle.Bold;
            size.Dock = DockStyle.Fill;

            histroyTable.Controls.Add(source, 0, 0);
            histroyTable.Controls.Add(dest, 1, 0);
            histroyTable.Controls.Add(size, 2, 0);
            histroyTable.Controls.Add(prog, 3, 0);

            Copy(inputFile, outputFile);
            Copy(inputFile, outputFile);
        }

        private void histroyTable_Paint(object sender, PaintEventArgs e)
        {

        }

    }
}
